<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_ValoracionStock" pageWidth="1000" pageHeight="820" columnWidth="960" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DATE_TO" class="java.util.Date"/>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["http://192.168.0.216/openbravo/web"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_ORG" class="java.lang.String"/>
	<queryString>
		<![CDATA[select a.Categoria,a.Nombre, a.Descripcion, a.Costo,
       sum(a.Stock) as Stock, sum(a.Costo * a.Stock) as subtotal,
       (select ad_org_id
          from ad_org s
         where s.ad_client_id IN ($P!{USER_CLIENT})
           and s.c_calendar_id is not null
         limit 1) as organizationid
from (
SELECT t.AD_ORG_ID AS organizationid,
       p.name AS Nombre,
       cat.name as Categoria,
       coalesce(p.description, p.description, '-') AS Descripcion,
       coalesce((select c.cost from m_costing c
              where p.m_product_id = c.m_product_id AND
                    c.datefrom < ($P{DATE_TO}) AND
                    c.dateto >= ($P{DATE_TO}) limit 1), '0'
                                             ) AS Costo,
       sum(t.movementqty) AS Stock
FROM m_product p, m_transaction t, m_product_category cat
WHERE p.ad_client_id IN ($P!{USER_CLIENT})
  AND p.isactive='Y'
  AND t.m_product_id = p.m_product_id
  AND cat.m_product_category_id = p.m_product_category_id
  AND t.m_locator_id IN (select m_locator_id from m_locator where m_warehouse_id in (select m_warehouse_id from m_warehouse where isactive='Y'))
  AND t.movementdate <= ($P{DATE_TO})
GROUP BY 1, 2, 3, 4, 5
ORDER BY 2
) a
group by 1,2,3,4
ORDER BY 2]]>
	</queryString>
	<field name="nombre" class="java.lang.String"/>
	<field name="descripcion" class="java.lang.String"/>
	<field name="costo" class="java.math.BigDecimal"/>
	<field name="stock" class="java.math.BigDecimal"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="organizationid" class="java.lang.String"/>
	<field name="categoria" class="java.lang.String"/>
	<variable name="total_general" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="0" width="594" height="40"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="24" isUnderline="false"/>
				</textElement>
				<text><![CDATA[Reporte Valoración de Inventario - Total]]></text>
			</staticText>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="687" y="0" width="122" height="40"/>
				<imageExpression class="java.net.URL"><![CDATA[new java.net.URL($P{BASE_WEB}+"/../utility/ShowImageLogo?logo=yourcompanydoc&orgId="+ $F{organizationid})]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="50">
			<staticText>
				<reportElement style="Column header" x="0" y="33" width="102" height="17"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Código]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="543" y="33" width="200" height="17"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Descripción]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="743" y="33" width="70" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Costo]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="813" y="33" width="70" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Stock]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="883" y="33" width="76" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Subtotal]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" mode="Transparent" x="293" y="33" width="250" height="17" forecolor="#666666" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<text><![CDATA[Nombre]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="102" y="33" width="191" height="17"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Categoría]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField>
				<reportElement style="Detail" x="0" y="0" width="102" height="15"/>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="543" y="0" width="200" height="15"/>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{descripcion}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0000">
				<reportElement style="Detail" x="743" y="0" width="70" height="15"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{costo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="813" y="0" width="70" height="15"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{stock}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="883" y="0" width="76" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{subtotal}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="293" y="0" width="250" height="15"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="102" y="0" width="191" height="15"/>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{categoria}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="17" splitType="Stretch">
			<staticText>
				<reportElement style="Column header" x="743" y="0" width="140" height="17"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Total :]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="883" y="0" width="76" height="17"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total_general}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="839" y="0" width="80" height="20"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Página "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="919" y="0" width="40" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy">
				<reportElement style="Column header" x="0" y="0" width="102" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
