package com.atrums.felectronica.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECFE_Valor_Descuento extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      // Obtencion de los valores
      String stremPorcentaje = vars.getStringParameter("inpemAtecfeDescuento");
      String srtemValorDescuento = vars.getStringParameter("inpemNeoValordescuento");
      String strPriceActual = vars.getNumericParameter("inppriceactual");
      String strQtyInvoice = vars.getNumericParameter("inpqtyinvoiced");
      try {
        // Factura Cliente
        if (!vars.getStringParameter("inpcInvoiceId").isEmpty())
          printPage(response, vars, stremPorcentaje, srtemValorDescuento, strPriceActual,
              strQtyInvoice);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String stremPorcentaje, String srtemValorDescuento, String strPriceActual,
      String strQtyInvoice) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");
    String Mensaje1;
    String Mensaje2;
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/felectronica/ad_callouts/CallOut").createXmlDocument();

    // Calculo del valor ingresado para determinar el porcentaje

    
    
    BigDecimal valorDescuento;
    BigDecimal cantidad;
    BigDecimal precioActual;
    BigDecimal valorPorcentaje;
    BigDecimal valorPorcentajeNum;
    BigDecimal valorPorcentajeDen;
    try {
      valorDescuento = BigDecimal.valueOf(Double.valueOf(srtemValorDescuento).doubleValue());
      cantidad = BigDecimal.valueOf(Double.valueOf(strQtyInvoice).doubleValue());
      precioActual = BigDecimal.valueOf(Double.valueOf(strPriceActual).doubleValue());
      valorPorcentaje=new BigDecimal(0);
      int resp=valorDescuento.compareTo(new BigDecimal(0));
      if (resp != 0) {
    	  valorPorcentajeNum = ((valorDescuento.multiply(new BigDecimal(100))));
          valorPorcentajeDen = cantidad.multiply(precioActual);
          // valorPorcentaje = (valorPorcentajeNum.divide(valorPorcentajeDen)).setScale(2,
          // BigDecimal.ROUND_HALF_UP);
          valorPorcentaje = (valorPorcentajeNum.divide(valorPorcentajeDen, 2, RoundingMode.HALF_UP));
      }
      StringBuffer resultado = new StringBuffer();
      resultado.append("var calloutName='ATECFE_Valor_Descuento';\n\n");
      resultado.append("var respuesta = new Array(");
      resultado.append("new Array(\"inpemAtecfeDescuento\", \"" + valorPorcentaje + "\")");
      resultado.append(");");
      xmlDocument.setParameter("array", resultado.toString());
      xmlDocument.setParameter("frameName", "appFrame");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception e) {
      System.out.println(e.toString());
    }

  }
}
