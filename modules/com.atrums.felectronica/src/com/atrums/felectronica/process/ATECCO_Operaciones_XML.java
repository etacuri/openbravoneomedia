package com.atrums.felectronica.process;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.dom4j.Element;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.UtilSql;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.utils.FormatUtilities;

import com.atrums.felectronica.data.ATECFE_conf_firma;

public class ATECCO_Operaciones_XML {
  static final Logger log = Logger.getLogger(ATECCO_Operaciones_XML.class);
  private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  public ATECCO_Operaciones_XML() {
    super();
  }

  public boolean declararFacturaSRI(String strCinvoiceId, String varUser, OBError msg,
      Connection connection) {
    try {

      ATECFEGenerarXmlData[] axmlDet = this.methodSeleccionarDetalles(connection, strCinvoiceId);

      if (!(axmlDet.length > 0)) {
        msg.setType("Error");
        msg.setMessage("Las facturas necesitan tener productos o impuestos para procesar");
        msg.setTitle("@Error@");
      }

      Invoice invDato = OBDal.getInstance().get(Invoice.class, strCinvoiceId);

      ATECFEGenerarXmlData[] axmlDat = this.methodFacturaElect(connection,
          invDato.getTransactionDocument().getId());

      if (invDato.getInvoiceDate() == null) {
        msg.setType("Error");
        msg.setMessage("No tiene fecha de factura el documento");
        msg.setTitle("@Error@");
        return false;
      }

      if (!invDato.getDocumentNo().toString().matches("[0-9]*")) {
        msg.setType("Error");
        msg.setMessage("El N° del documento solo tienen que contener números");
        msg.setTitle("@Error@");
        return false;
      }

      if (axmlDat[0].dato1.equals("Y")) {
        if (verificarConfiguración(strCinvoiceId, "", "", msg, connection)) {
          if (this.generarFacturaXML(strCinvoiceId, connection, varUser, msg)) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      } else {
        Calendar cldFecha = new GregorianCalendar();
        SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
        String strFechaAut = "";

        if (invDato.getCoVencimientoAutSri() == null) {
          strFechaAut = sdfFormato.format(cldFecha.getTime());
        } else {
          strFechaAut = sdfFormato.format(invDato.getCoVencimientoAutSri());
        }

        /*
         * ATECFEGenerarXmlData.methodActualizarInvo(connection, null, null, null, "PD", "NE",
         * invDato.getCoNroAutSri(), "", strFechaAut, strCinvoiceId);
         */

        return true;
      }
    } catch (Exception ex) {
      // TODO Auto-generated catch block
      log.warn(ex.getMessage());
      msg.setType("Error");
      msg.setMessage(ex.getMessage());
      msg.setTitle("@Error@");
      return false;
    }
  }

  private boolean verificarConfiguración(String strCinvoiceId, String strCoRetencionCompra,
      String strMinout, OBError msg, Connection connection) throws ServletException, SQLException {

    ATECFEGenerarXmlData[] axmlConf = null;

    axmlConf = this.methodSeleccionarConfiguracion(connection, strCinvoiceId);

    if (axmlConf != null && axmlConf.length == 1) {
      if (axmlConf[0].dato1 == null || axmlConf[0].dato1.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay dirección del servidor de transacciones");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato2 == null || axmlConf[0].dato2.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay dirección del servidor de consultas");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato3 == null || axmlConf[0].dato3.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay un nombre de la base de datos del servidor de consultas");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato4 == null || axmlConf[0].dato4.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay un puerto de la base de datos del servidor de consultas");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato5 == null || axmlConf[0].dato5.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay un usuario de la base de datos del servidor de consultas");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato6 == null || axmlConf[0].dato6.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay una contraseña de la base de daros del servidor de consultas");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato7 == null || axmlConf[0].dato7.equals("")) {
        msg.setType("Error");
        msg.setMessage("No hay un CI/RUC del cliente de la factura");
        msg.setTitle("@Error@");
        return false;
      } else if (axmlConf[0].dato8 == null || axmlConf[0].dato8.equals("")) {
        msg.setType("Error");
        msg.setMessage(
            "No hay un email del cliente de la factura, para enviar la factura al cliente");
        msg.setTitle("@Error@");
        return false;
      } else if (validarEmail(axmlConf[0].dato8)) {
        msg.setType("Error");
        msg.setMessage("El email del cliente no es valido");
        msg.setTitle("@Error@");
        return false;
      } else {
        return true;
      }
    } else {
      msg.setType("Error");
      msg.setMessage(
          "No se ha encontrado una configuración del servidor de facturación electrónica");
      msg.setTitle("@Error@");
      return false;
    }
  }

  private boolean validarEmail(String strEmail) {
    Pattern pattern = Pattern.compile(PATTERN_EMAIL);
    Matcher matcher = pattern.matcher(strEmail);

    return !matcher.matches();
  }

  private ATECFEGenerarXmlData[] methodSeleccionarDetalles(Connection connection,
      String strCinvoiceId) throws SQLException {
    String sqlQuery = "select coalesce(p.value,'') as dato1, " + "coalesce(null,'') as dato2, "
        + "coalesce(p.description,coalesce(p.name,'')) as dato3,"
        + "round(coalesce(il.qtyinvoiced,0),2) as dato4, "
        + "round(coalesce(il.priceactual,0),2) as dato5, "
        + "round((coalesce(il.em_atecfe_descuento,0) / 100) * (il.priceactual * il.qtyinvoiced), 2) as dato6, "
        + "round(coalesce(il.linenetamt,0),2) as dato7, "
        + "coalesce(il.c_invoiceline_id,'') as dato8 " + "from c_invoiceline il "
        + "inner join m_product p on (il.m_product_id = p.m_product_id) "
        + "where il.c_invoice_id = '" + strCinvoiceId + "'; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodFacturaElect(Connection connection, String strDocumentId)
      throws SQLException {
    String sqlQuery = "select d.em_atecfe_fac_elec as dato1, "
        + "coalesce(dt.templatelocation,'') as dato2, "
        + "coalesce(dt.templatefilename,'') as dato3, " + "coalesce(dt.reportfilename,'') as dato4 "
        + "from c_doctype d"
        + "left join c_poc_doctype_template dt on (dt.c_doctype_id = d.c_doctype_id) "
        + "where d.c_doctype_id = '" + strDocumentId + "' " + "limit 1; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSeleccionarConfiguracion(Connection connection,
      String strCinvoiceId) throws SQLException {
    String sqlQuery = "select acs.direc_serv_transa as dato1, " + "acs.direc_serv_consul as dato2, "
        + "acs.nombre_bdd as dato3, " + "acs.puerto_bdd as dato4, " + "acs.usuario_bdd as dato5, "
        + "acs.password_bdd as dato6, " + "b.taxid as dato7, " + "u.email as dato8, "
        + "pc.smtpserverpassword as dato9 " + "from c_invoice i "
        + "inner join atecfe_conf_servidor acs ON (i.ad_client_id = acs.ad_client_id) "
        + "left join c_bpartner b ON (i.c_bpartner_id = b.c_bpartner_id) "
        + "left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y') "
        + "left join c_poc_configuration pc ON (i.ad_client_id = pc.ad_client_id) "
        + "where i.c_invoice_id = '" + strCinvoiceId + "'" + "limit 1; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSelDirMatriz(Connection connection) throws SQLException {
    String sqlQuery = "select coalesce(c.address1,coalesce(c.address2,'')) as dato1 "
        + "from ad_orginfo ad, ad_org og, c_location c " + "where ad.ad_org_id = og.ad_org_id"
        + " and og.issummary = 'Y' " + "and og.ad_orgtype_id = '1'"
        + " and c.c_location_id = ad.c_location_id " + "limit 1; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSeleccionarDirec(Connection connection, String strAdOrgId)
      throws SQLException {
    String sqlQuery = "select (coalesce(l.address1,coalesce(l.address2,'')) || ', ' || coalesce(l.city,'') || '-' || coalesce(c.name,'')) as dato1 "
        + "   from ad_orginfo oi "
        + "            inner join c_location l on (oi.c_location_id = l.c_location_id) "
        + "            inner join c_country c on (l.c_country_id = c.c_country_id) "
        + "   where oi.ad_org_id = '" + strAdOrgId + "'; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSeleccionarImpues(Connection connection,
      String strCInvoiceId) throws SQLException {
    String sqlQuery = "select coalesce(tc.em_atecfe_codtax,'') as dato1, "
        + "coalesce(t.em_atecfe_tartax,'') as dato2, "
        + "round(coalesce(it.taxbaseamt,0),2) as dato3, "
        + "round(coalesce(it.taxamt,0),2) as dato4, " + "round(coalesce(t.rate,0),2) as dato5 "
        + "from c_invoice i " + "inner join c_invoicetax it on (i.c_invoice_id = it.c_invoice_id) "
        + "inner join c_tax t on (it.c_tax_id = t.c_tax_id) "
        + "inner join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id) "
        + "where tc.em_atecfe_codtax is not null and i.c_invoice_id = '" + strCInvoiceId + "'; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSeleccionarAdicional(Connection connection,
      String strAdClientId) throws SQLException {
    String sqlQuery = "select coalesce(null,'3') as dato1, "
        + "coalesce(null,'Pagina web') as dato2, " + "coalesce(d.dummy,'') as dato3, "
        + "coalesce(null,'rucFirmante') as dato4, " + "coalesce(d.dummy,'') as dato5, "
        + "coalesce(null,'Factura') as dato6, " + "coalesce(d.dummy,'') as dato7 " + "from dual d "
        + "where d.dummy <> '" + strAdClientId + "'; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] methodSeleccionarFirma(Connection connection, String strUserId)
      throws SQLException {
    String sqlQuery = "select acf.atecfe_conf_firma_id as dato1 " + "from atecfe_conf_firma acf "
        + "join atecfe_conf_firma_lineas acfl on (acf.atecfe_conf_firma_id = acfl.atecfe_conf_firma_id) "
        + "where acf.isactive = 'Y' " + "and acfl.isactive = 'Y' " + "and acfl.ad_user_id = '"
        + strUserId + "'; ";

    return this.consultaSQL(sqlQuery, connection);
  }

  private ATECFEGenerarXmlData[] consultaSQL(String sqlQuery, Connection connection)
      throws SQLException {
    PreparedStatement ps = connection.prepareStatement(sqlQuery);
    ResultSet selectDatos = ps.executeQuery();

    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);

    while (selectDatos.next()) {
      ATECFEGenerarXmlData objectATECFEGenerarXmlData = new ATECFEGenerarXmlData();
      objectATECFEGenerarXmlData.dato1 = UtilSql.getValue(selectDatos, "dato1");
      objectATECFEGenerarXmlData.dato2 = UtilSql.getValue(selectDatos, "dato2");
      objectATECFEGenerarXmlData.dato3 = UtilSql.getValue(selectDatos, "dato3");
      objectATECFEGenerarXmlData.dato4 = UtilSql.getValue(selectDatos, "dato4");
      objectATECFEGenerarXmlData.dato5 = UtilSql.getValue(selectDatos, "dato5");
      objectATECFEGenerarXmlData.dato6 = UtilSql.getValue(selectDatos, "dato6");
      objectATECFEGenerarXmlData.dato7 = UtilSql.getValue(selectDatos, "dato7");
      objectATECFEGenerarXmlData.dato8 = UtilSql.getValue(selectDatos, "dato8");
      vector.addElement(objectATECFEGenerarXmlData);
    }

    selectDatos.close();
    ps.close();

    ATECFEGenerarXmlData[] axmlDet = new ATECFEGenerarXmlData[vector.size()];
    vector.copyInto(axmlDet);

    return axmlDet;
  }

  private boolean generarFacturaXML(String strCinvoiceId, Connection connection, String varUser,
      OBError msg) {
    return false;
  }

  public void addCamposAdic(Element elmNodo, Connection conn, String strAdClient)
      throws ServletException, UnsupportedEncodingException, SQLException {
    String strDato = "dato";

    ATECFEGenerarXmlData[] axmlInfAdici = this.methodSeleccionarAdicional(conn, strAdClient);

    if (axmlInfAdici != null && axmlInfAdici.length > 0) {
      final Element elminfAdc = elmNodo.addElement("infoAdicional");

      for (int i = 1; i <= Integer.parseInt(axmlInfAdici[0].dato1); i++) {

        String utfCampoAdicional = new String(
            axmlInfAdici[0].getField(strDato + (i * 2)).getBytes("ISO-8859-1"), "utf-8");

        String utfInfo = new String(
            axmlInfAdici[0].getField(strDato + ((i * 2) + 1)).getBytes("ISO-8859-1"), "utf-8");

        elminfAdc.addElement("campoAdicional")
            .addAttribute("nombre", this.normalizacionPalabras(utfCampoAdicional))
            .addText(this.normalizacionPalabras(utfInfo));
      }
    }
  }

  public File firmarDocumento(File flDoc, Connection conn, String strUse, OBError msg)
      throws Exception {
    File flDocumentoFirmado = null;

    /*
     * Realizar una consulta de la firmas electronicas que estan activas
     */
    ATECFEGenerarXmlData[] axmlConFir = this.methodSeleccionarFirma(conn, strUse);

    /*
     * Tomando la primera firma del documento para realizar la firma
     */
    if (axmlConFir != null && axmlConFir.length > 0) {
      ATECFE_conf_firma acfData = OBDal.getInstance().get(ATECFE_conf_firma.class,
          axmlConFir[0].dato1);

      String strPass = FormatUtilities.encryptDecrypt(acfData.getClaveContra(), false);
      String strDire = acfData.getDIRArchivoFirma();
      String strNArc = acfData.getNombreArchivo();

      /*
       * Enviando documento para realizar la firma
       */
      flDocumentoFirmado = ATECFE_XAdESBESSignature.firmar(flDoc,
          strDire + File.separator + strNArc, strPass);

      /*
       * Devolviendo documento firmado
       */
      return flDocumentoFirmado;
    } else {
      msg.setType("Error");
      msg.setMessage("El usuario no tiene permisos para firmar digitalmente el documento");
      msg.setTitle("@Error@");
      return null;
    }
  }

  private boolean generarFacturaXMLGre(String strMInout, Connection conn, String strUser,
      OBError msg) {
    return false;

  }

  private String normalizacionPalabras(String strPalabras) {

    String strPalabraAux = "";

    strPalabraAux = strPalabras.replaceAll("á", "a");
    strPalabraAux = strPalabraAux.replaceAll("é", "e");
    strPalabraAux = strPalabraAux.replaceAll("í", "i");
    strPalabraAux = strPalabraAux.replaceAll("ó", "o");
    strPalabraAux = strPalabraAux.replaceAll("ú", "u");
    return strPalabraAux;
  }
}
