package com.atrums.neomedia;

import org.openbravo.client.kernel.BaseTemplateComponent;
import org.openbravo.dal.core.OBContext;

public class GenFactViewComponent extends BaseTemplateComponent {
  public static final String GENFAC_VIEW_COMPONENT_ID = "GenFactView";

  public String getUserName() {
    return OBContext.getOBContext().getUser().getName();
  }
}
