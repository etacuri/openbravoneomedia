package com.atrums.neomedia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

@ApplicationScoped
@ComponentProvider.Qualifier(GenFactpubliComponentProvider.GENFACTPUBLI_VIEW_COMPONENT_TYPE)
public class GenFactpubliComponentProvider extends BaseComponentProvider {

  public static final String GENFACTPUBLI_VIEW_COMPONENT_TYPE = "OBEXAPP_AvanceViewType";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    // TODO Auto-generated method stub
    if (componentId.equals(GenFactpubliViewComponent.GENFACPUBLI_VIEW_COMPONENT_ID)) {
      final GenFactpubliViewComponent component = new GenFactpubliViewComponent();
      component.setId(GenFactpubliViewComponent.GENFACPUBLI_VIEW_COMPONENT_ID);
      component.setParameters(parameters);
      return component;
    }
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    globalResources.add(createStaticResource("web/com.atrums.neomedia/js/genfactpubli-toolbar-button.js",
        true));
   return globalResources;
  }

  @Override
  public List<String> getTestResources() {
    return Collections.emptyList();
  }
}
