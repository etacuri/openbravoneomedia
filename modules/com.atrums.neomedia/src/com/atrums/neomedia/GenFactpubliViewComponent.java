package com.atrums.neomedia;

import org.openbravo.client.kernel.BaseTemplateComponent;
import org.openbravo.dal.core.OBContext;

public class GenFactpubliViewComponent extends BaseTemplateComponent {
  public static final String GENFACPUBLI_VIEW_COMPONENT_ID = "GenFactpubliView";

  public String getUserName() {
    return OBContext.getOBContext().getUser().getName();
  }
}
