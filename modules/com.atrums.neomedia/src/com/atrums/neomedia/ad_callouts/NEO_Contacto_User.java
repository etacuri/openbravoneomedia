package com.atrums.neomedia.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Contacto_User extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      // Obtencion de los valores
      String strContactoId = vars.getStringParameter("inpcontactoId");
      try {
        // Seguimiento Comercial
        if (!vars.getStringParameter("inpneoSegComercialId").isEmpty())       	
          printPage(response, vars, strContactoId);
        
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strContactoId) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/neomedia/ad_callouts/CallOut").createXmlDocument();

    // Calculo del valor ingresado para determinar el porcentaje

    NEOUserData[] data=NEOUserData.select(this, strContactoId);
    try {
    	String email=null;
    	String telefono=null;
    	if (data != null && data.length > 0) {
    		email = data[0].email.equals("") ? "N/A" : data[0].email;
    		telefono = data[0].telefono.equals("") ? "N/A" : data[0].telefono;
    	}
    	
      StringBuffer resultado = new StringBuffer();
      resultado.append("var calloutName='NEO_Contacto_User';\n\n");
      resultado.append("var respuesta = new Array(");

      resultado.append("new Array(\"inptelefonoContacto\", \"" + telefono + "\"),");
      resultado.append("new Array(\"inpemailContacto\", \"" + email + "\")");
      
      resultado.append(");");
      xmlDocument.setParameter("array", resultado.toString());
      xmlDocument.setParameter("frameName", "appFrame");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception e) {
      System.out.println(e.toString());
    }

  }
}
