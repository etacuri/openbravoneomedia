package com.atrums.neomedia.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Val_Publicidad extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      // Obtencion de los valores
    	String strOrderId = vars.getStringParameter("inpcOrderId");
      String strSeguimientoId = vars.getStringParameter("inpemNeoSegComercialId");
      try {
        // Seguimiento Comercial
        if (!vars.getStringParameter("inpemNeoSegComercialId").isEmpty())       	
          printPage(response, vars, strSeguimientoId);
        
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strSeguimientoId) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/neomedia/ad_callouts/CallOut").createXmlDocument();

    // Calculo del valor ingresado para determinar el porcentaje

    NEOUserData[] data=NEOUserData.selectPublicidad(this, strSeguimientoId);
    try {
    	String publicidad=null;
    	if (data != null && data.length > 0) {
    		publicidad = data[0].name.equals("") ? "N/A" : data[0].name;
    	}
    	
      StringBuffer resultado = new StringBuffer();
      resultado.append("var calloutName='NEO_Val_Publicidad';\n\n");
      resultado.append("var respuesta = new Array(");

      resultado.append("new Array(\"inpemNeoEspublicidad\", \"" + publicidad + "\")");
      
      resultado.append(");");
      xmlDocument.setParameter("array", resultado.toString());
      xmlDocument.setParameter("frameName", "appFrame");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception e) {
      System.out.println(e.toString());
    }

  }
}
