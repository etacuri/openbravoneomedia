package com.atrums.neomedia.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Val_Fob extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;
  private static final BigDecimal ZERO = new BigDecimal(0.0);
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
    	 String strChanged = vars.getStringParameter("inpLastFieldChanged");
    	 
         if (log4j.isDebugEnabled())
                 log4j.debug("CHANGED: " + strChanged);
         
         
      // OBTENCION DE VALORES
         String srtCantidad = vars.getNumericParameter("inpneoCantidad");
         String srtUnitFob = vars.getNumericParameter("inpneoUnitFob");
    	
    	 
    	 
      try {
    	  
        // EJECUCION DE PROCESO DEL CALLOUT
        	//if ((!strLineprodId.isEmpty())||(strLineprodId!="")) {
        		printPage(response, vars, srtCantidad,srtUnitFob);	
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String srtCantidad, String srtUnitFob) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");
    
    String Mensaje="";

    
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/neomedia/ad_callouts/CallOut").createXmlDocument();
    
    

    // RESPUESTA AL CALLOUT PARA QUE MUESTRE MENSAJE
    try{
      BigDecimal TotalFob=BigDecimal.ZERO;;
      BigDecimal Cantidad;
      BigDecimal UnitFob;
      
      
     

      
      StringBuffer resultado = new StringBuffer();
      resultado.append("var calloutName='NEO_Val_Fob';\n\n");
      resultado.append("var respuesta = new Array(");
      
      
      Cantidad= (!Utility.isBigDecimal(srtCantidad) ? ZERO : new BigDecimal(srtCantidad));
      UnitFob= (!Utility.isBigDecimal(srtUnitFob) ? ZERO : new BigDecimal(srtUnitFob));

      
      
      //////COSTO TOTAL FOB

      TotalFob = (Cantidad.multiply(UnitFob));
      int comp1=TotalFob.compareTo(new BigDecimal(10000));
      if(comp1==1) {
    	  Mensaje="EL COSTO TOTAL FOB SUPERA LOS $10000";
      }
      
      /////PESO CALCULADO
         
      
      resultado.append("new Array('MESSAGE', \""
              + Utility.messageBD(this, Mensaje.toString(),
                  vars.getLanguage()) + "\")");
    
      resultado.append(");");
      
      xmlDocument.setParameter("array", resultado.toString());
      xmlDocument.setParameter("frameName", "appFrame");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception e) {
      System.out.println(e.toString());
    }

  }
}
