package com.atrums.neomedia.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Val_Peso extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;
  private static final BigDecimal ZERO = new BigDecimal(0.0);
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
    	 String strChanged = vars.getStringParameter("inpLastFieldChanged");
    	 
         if (log4j.isDebugEnabled())
                 log4j.debug("CHANGED: " + strChanged);
         
         
      // OBTENCION DE VALORES
         //String strLineprodId=vars.getStringParameter("inpneoLineprodId");
         String srtCantidad = vars.getNumericParameter("inpneoCantidad");
         String srtUnitFob = vars.getNumericParameter("inpneoUnitFob");
    	 String srtPesoCalculo = vars.getNumericParameter("inpneoPesoCalculo");
    	 String srtBrutoUnit = vars.getNumericParameter("inpneoPesobrutoUnit");
    	 String srtAncho = vars.getNumericParameter("inpneoAncho");
    	 String strAlto = vars.getNumericParameter("inpneoAlto");
    	 String strProfundidad = vars.getNumericParameter("inpneoProfundidad");
    	 
    	 
      try {
    	  
        // EJECUCION DE PROCESO DEL CALLOUT
        	//if ((!strLineprodId.isEmpty())||(strLineprodId!="")) {
        		printPage(response, vars, srtCantidad,srtUnitFob,srtPesoCalculo,srtBrutoUnit,srtAncho,strAlto,strProfundidad);	
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String srtCantidad, String srtUnitFob, String srtPesoCalculo,String srtBrutoUnit,String srtAncho, String srtAlto, String srtProfundidad) throws IOException, ServletException, ParseException {
    log4j.debug("Output: dataSheet");
    
    String Mensaje1="";
    String Mensaje2="";
    
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/neomedia/ad_callouts/CallOut").createXmlDocument();
    
    

    // RESPUESTA AL CALLOUT PARA QUE MUESTRE MENSAJE
    try{
      BigDecimal PesoCalculado=BigDecimal.ZERO;;
      BigDecimal TotalFob=BigDecimal.ZERO;;
      BigDecimal Cantidad;
      BigDecimal UnitFob;
      BigDecimal BrutoTotal=BigDecimal.ZERO;;
      BigDecimal TotalVol=BigDecimal.ZERO;;
      BigDecimal TotalVolumetrico=BigDecimal.ZERO;;
      BigDecimal Ancho;
      BigDecimal Alto;
      BigDecimal Profundidad;
      BigDecimal BrutoUnit;
      
     

      
      StringBuffer resultado = new StringBuffer();
      resultado.append("var calloutName='NEO_Val_Peso_Fob';\n\n");
      resultado.append("var respuesta = new Array(");
      
      
      Cantidad= (!Utility.isBigDecimal(srtCantidad) ? ZERO : new BigDecimal(srtCantidad));
      UnitFob= (!Utility.isBigDecimal(srtUnitFob) ? ZERO : new BigDecimal(srtUnitFob));
      BrutoUnit= (!Utility.isBigDecimal(srtBrutoUnit) ? ZERO : new BigDecimal(srtBrutoUnit));
      Ancho= (!Utility.isBigDecimal(srtAncho) ? ZERO : new BigDecimal(srtAncho));
      Alto= (!Utility.isBigDecimal(srtAlto) ? ZERO : new BigDecimal(srtAlto));
      Profundidad= (!Utility.isBigDecimal(srtProfundidad) ? ZERO : new BigDecimal(srtProfundidad));
      
      
      //////COSTO TOTAL FOB

      TotalFob = (Cantidad.multiply(UnitFob));
      int comp1=TotalFob.compareTo(new BigDecimal(10000));
      if(comp1==1) {
    	  Mensaje1="EL COSTO TOTAL FOB SUPERA LOS $10000";
      }
      
      /////PESO CALCULADO
      BrutoTotal = (Cantidad.multiply(BrutoUnit));
      
      TotalVol=(Ancho.multiply(Alto)).multiply(Profundidad);
      
      TotalVolumetrico = (TotalVol.divide(new BigDecimal(6000), 2, RoundingMode.HALF_UP));
      
      int comp2=BrutoTotal.compareTo(TotalVolumetrico);
      if ((comp2==1) || (comp2==0)){
    	  PesoCalculado=BrutoTotal;
      }
      if (comp2==-1) {
    	  PesoCalculado=TotalVolumetrico;
      }
      
 
      int resp=PesoCalculado.compareTo(new BigDecimal(50));
      if (resp == 1) {
    	  Mensaje2="EL PESO TOTAL CALCULADO SUPERA LOS 50 KG";
      }
      if (!Mensaje1.equals("")||!Mensaje2.equals("")) {
    	  resultado.append("new Array('MESSAGE', \""
                  + Utility.messageBD(this, Mensaje1.toString()+" "+Mensaje2.toString(),
                      vars.getLanguage()) + "\")");
        
          resultado.append(");");
      }     
      xmlDocument.setParameter("array", resultado.toString());
      xmlDocument.setParameter("frameName", "appFrame");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception e) {
      System.out.println(e.toString());
    }

  }
}
