package com.atrums.neomedia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

@ApplicationScoped
@ComponentProvider.Qualifier(GenFactComponentProvider.GENFACT_VIEW_COMPONENT_TYPE)
public class GenFactComponentProvider extends BaseComponentProvider {

  public static final String GENFACT_VIEW_COMPONENT_TYPE = "OBEXAPP_AvanceViewType";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    // TODO Auto-generated method stub
    if (componentId.equals(GenFactViewComponent.GENFAC_VIEW_COMPONENT_ID)) {
      final GenFactViewComponent component = new GenFactViewComponent();
      component.setId(GenFactViewComponent.GENFAC_VIEW_COMPONENT_ID);
      component.setParameters(parameters);
      return component;
    }
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    globalResources.add(createStaticResource("web/com.atrums.neomedia/js/genfact-toolbar-button.js",
        true));
   return globalResources;
  }

  @Override
  public List<String> getTestResources() {
    return Collections.emptyList();
  }
}
