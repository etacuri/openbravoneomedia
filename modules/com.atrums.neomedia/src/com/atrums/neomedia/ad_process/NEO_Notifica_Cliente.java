package com.atrums.neomedia.ad_process;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

import com.atrums.neomedia.data.neoCaso;

public class NEO_Notifica_Cliente implements Process {

	static Logger log4j = Logger.getLogger(NEO_Notifica_Cliente.class);
	
	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		
		final StringBuilder sb = new StringBuilder();
		sb.append("Resultados de envío de correos:<br/>");
		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();
		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
		varsAux.getOrg());
		
		try {
			
			NEOCorreos[] soportes=NEOCorreos.obtenerIdSoportes(conn);
			if((soportes==null)||(soportes.length==0)){
				sb.append("NO EXISTEN CASOS DE SOPORTE CERRADOS PARA NOTIFICAR<br/>");
			}else {
				NEO_Funciones_Aux opeaux= new NEO_Funciones_Aux();
				SimpleDateFormat fechaFormato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				for (NEOCorreos s : soportes) {
					
					neoCaso csop=OBDal.getInstance().get(neoCaso.class, s.dato1);
					
					/*
					 * CONTENIDO USADO EN CORREO
					 */
					 
					String cuerpo3="Región Sierra: 095 872 1627, whatsapp: https://wa.me/593984065809\n";
					cuerpo3=cuerpo3+"Región Costa: 099 406 5809, whatsapp: https://wa.me/593958721627\n";
					cuerpo3=cuerpo3+ "\n";
					
					String cuerpo4="También puede escribirnos a soporte@neomedia.ec y con gusto le atenderemos.\n";
					cuerpo4=cuerpo4+"Saludos,\n";
					cuerpo4=cuerpo4+ "\n";
					cuerpo4=cuerpo4+"NEOMEDIA S.A";
					
					/*
					 * 
					 */
					
					String strReceptor = csop.getContantocli().getEmail().toString();
					
					if(!strReceptor.isEmpty()) {
						
						 String asunto="CIERRE DE CASO DE SOPORTE N° " + csop.getDocumentNo().toString();
						 
						 String cuerpo="Estimado(s) "+ csop.getCBPCliente().getName().toString() + "\n";
						 
						 cuerpo=cuerpo+ "\n";
						 
						 cuerpo=cuerpo+"Nos dirigimos a usted(es) con el fin de informarle que el caso N° "+csop.getDocumentNo().toString()+", con respecto a: "+csop.getNombrecaso().toString()+" ha sido cerrado exitosamente.\n";
						 
						 cuerpo=cuerpo+ "\n";
						 
						 String cuerpo2="Fecha de Inicio: "+fechaFormato.format(csop.getInicio())+"\n";
						 
						 cuerpo2=cuerpo2+"Fecha de Cierre: "+fechaFormato.format(csop.getFinal())+"\n";
						 
						 cuerpo2=cuerpo2+ "\n";
						 
						 cuerpo2=cuerpo2+"Si su inconveniente persiste por favor contáctenos a través de nuestros números de contacto:\n";
						 
						 cuerpo2=cuerpo2+ "\n";
						 
						 if (opeaux.enviarCorreo(strReceptor, asunto,cuerpo+cuerpo2+cuerpo3+cuerpo4,
 		                          null, null)) {
							
							 if((NEOCorreos.actualizaSoporte(conn, csop.getId().toString()))==1) {
								 sb.append("SE ENVIO CORRECTAMENTE EL CORREO DEL CASO"+csop.getDocumentNo().toString());
								 log4j.info("SE ENVIO CORRECTAMENTE EL CORREO DEL CASO"+csop.getDocumentNo().toString());
							 }else {
								 log4j.info("ERROR AL ACTUALIZAR EL ESTADO DEL CASO"+csop.getDocumentNo().toString());
							 }
						 }
						 
						 
					}else {
						sb.append("NO TIENE CONFIGURADO EL CORREO DEL CONTACTO EN EL SOPORTE"+csop.getDocumentNo().toString());
						log4j.info("NO TIENE CONFIGURADO EL CORREO DEL CONTACTO EN EL SOPORTE"+csop.getDocumentNo().toString());
					}
					 
				} 
				OBDal.getInstance().commitAndClose();
			}
			
			log4j.info("PROCESO DE NOTIFICACION FINALIZADO CORRECTAMENTE");
			
			  final OBError msg = new OBError();
		      msg.setType("Success");
		      msg.setTitle("Resultados!");
		      msg.setMessage(sb.toString());
		      bundle.setResult(msg);
		} catch (Exception ex) {
			log4j.info("ERROR EN EL PROCESO DE ENVIO DE CORREO"+ex.getMessage().toString());
			final OBError msg = new OBError();
		      msg.setType("Error");
		      msg.setMessage(ex.getMessage());
		      msg.setTitle("Ocurrió un Error¡");
		      bundle.setResult(msg);
		      return;
		 } finally {
		      OBDal.getInstance().rollbackAndClose();
		 }
		
	}

}
