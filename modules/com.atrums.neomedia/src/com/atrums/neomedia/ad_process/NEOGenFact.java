package com.atrums.neomedia.ad_process;

import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.openbravo.database.ConnectionProvider;



class NEOGenFact {
  private static final Logger log = Logger.getLogger(NEOGenFact.class);

  private static ConnectionProvider connectionProvider;
  private static NEOGenerarFactura[] data = null;
  
  public NEOGenFact(ConnectionProvider connectionProvider, NEOGenerarFactura[] data) {
	    setConnectionProvider(connectionProvider);
	    setData(data);
	  }

	  public NEOGenFact(ConnectionProvider connectionProvider) {
	    setConnectionProvider(connectionProvider);
	  }
  

  public static NEOGenerarFactura[] generarCabecera(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.crearcabecera(getConnectionProvider(), cOrderId);
    return datos;
  }
  public static NEOGenerarFactura[] generarLineas(String cOrderId,String idInvoice, String numLinea) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.crearLineas(getConnectionProvider(), cOrderId,idInvoice,numLinea);
    return datos;
  }
  public static NEOGenerarFactura[] validarLinea(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.validaLinea(getConnectionProvider(), cOrderId);
    return datos;
  }
  public static NEOGenerarFactura[] obtenerNumDoc(String idInvoice) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneNumFact(getConnectionProvider(), idInvoice);
    return datos;
  }
  public static NEOGenerarFactura[] obtenerEsPublicidad(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneEsPublicidad(getConnectionProvider(), cOrderId);
    return datos;
  }
  public static NEOGenerarFactura[] obtenerEsFacturaParte(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneEsFacturaParte(getConnectionProvider(), cOrderId);
    return datos;
  }
  public static String obtieneIdInvoice(String bPartnerId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneIdInvoice(getConnectionProvider(), bPartnerId);
	  if (datos.length>0) {
		  return datos[0].invoiceId;
	  }else {
		  return "0";
	  }
    
  }
  public static NEOGenerarFactura[] obtenerCantFacturar(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneCantPublicidad(getConnectionProvider(), cOrderId);
    return datos;
  }
  public static NEOGenerarFactura[] obtieneIdTercero(String cOrderId) throws ServletException {
	  NEOGenerarFactura[] datos = null;
	  datos = NEOGenerarFactura.obtieneIdTercero(getConnectionProvider(), cOrderId);
    return datos;
  }
  
  public static ConnectionProvider getConnectionProvider() {
	    return connectionProvider;
	  }

  public static void setConnectionProvider(ConnectionProvider connectionProvider) {
	  NEOGenFact.connectionProvider = connectionProvider;
	  }
  
  public static NEOGenerarFactura[] getData() {
	    return data;
	  }

  public static void setData(NEOGenerarFactura[] data) {
	  NEOGenFact.data = data;
	  }
}
