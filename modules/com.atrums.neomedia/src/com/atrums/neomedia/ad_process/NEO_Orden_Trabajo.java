package com.atrums.neomedia.ad_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Orden_Trabajo extends HttpSecureAppServlet {
	
	
	  /**
	   * 
	   */
	  private static final long serialVersionUID = 1L;
	  

	@Override
	  public void init(ServletConfig config) {
	    super.init(config);
	    boolHist = false;
	  }
	  
	  
	  public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws IOException, ServletException {
		    VariablesSecureApp vars = new VariablesSecureApp(request);

		    if (vars.commandIn("DEFAULT")) {
		      String strProcessId = vars.getStringParameter("inpProcessId");
		      String strWindow = vars.getStringParameter("inpwindowId");
		      String strTab = vars.getStringParameter("inpTabId");
		      String strKey = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
		      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
		    } else if (vars.commandIn("SAVE")) {
		      String strWindow = vars.getStringParameter("inpWindowId");
		      String strOrder = vars.getStringParameter("inpcOrderId");
		      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
		      String strTab = vars.getStringParameter("inpTabId");

		      String strWindowPath = Utility.getTabURL(strTab, "R", true);
		      if (strWindowPath.equals("")) {
		        strWindowPath = strDefaultServlet;
		      }

		      OBError myError = processButton(vars, strKey, strOrder);
		      log4j.debug(myError.getMessage());
		      vars.setMessage(strTab, myError);
		      printPageClosePopUp(response, vars, strWindowPath);
		      
		    
		    }
		  }
	  
	  
	  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
		      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

		    Connection conn = null;

		    try {
		      XmlDocument xmlDocument = xmlEngine
		          .readXmlTemplate("com/atrums/neomedia/ad_process/NEO_Orden_Trabajo")
		          .createXmlDocument();
		      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
		      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		      xmlDocument.setParameter("theme", vars.getTheme());
		      xmlDocument.setParameter("key", strKey);
		      xmlDocument.setParameter("window", windowId);
		      xmlDocument.setParameter("tab", strTab);

		      response.setContentType("text/html; charset=UTF-8");
		      PrintWriter out = response.getWriter();
		      out.println(xmlDocument.print());
		      out.close();

		    } catch (Exception ex) {
		      // TODO: handle exception
		      throw new ServletException(ex);
		    } finally {
		      if (conn != null) {
		        try {
		          conn.close();
		        } catch (Exception e) {
		        }
		      }
		    }
		  }
	  
	  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder) {
		  
		  OBError myError = null;
		  Connection conn = OBDal.getInstance().getConnection();
		  OBContext.setAdminMode(true);
		  String mensajeop = null;
		  int auxresult = 0;
		  String auxerrormsg = null;
		  String numCotizaP;
		  String nombreTercero;
		  try {
			  Order order = OBDal.getInstance().get(Order.class, strOrder);
			  numCotizaP=order.getDocumentNo();
			  nombreTercero=order.getBusinessPartner().getName();
			  String sqlQuery = "";

		      String strPInstance = null;

		      PreparedStatement ps = null;
		      ResultSet rs = null;
		      
		      strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();

		      sqlQuery = "INSERT INTO ad_pinstance("
		          + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
		          + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
		          + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance
		          + "', 'FE2499A8A95145BEBCCF16890D356D8F', '" + strOrder + "', 'N', now(), "
		          + "'"+vars.getUser()+"', now(), null, null, '" + vars.getClient() + "', '"+vars.getOrg()+"', " + "'100', '100', 'Y');";

		      ps = conn.prepareStatement(sqlQuery);
		      int updateOrder = ps.executeUpdate();
		      
		      ps.close();
		      if(updateOrder == 1) {
		    	 
		    	  sqlQuery = "SELECT * FROM neo_genera_ordentrabajo('" + strPInstance + "')";
		    	  ps = conn.prepareStatement(sqlQuery);
		          ps.execute();
		          ps.close();
		    	  
		          
		          sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
		                  + strPInstance + "'";

		          ps = conn.prepareStatement(sqlQuery);
		          rs = ps.executeQuery();
		             
		              while (rs.next()) {
		                  if (rs.getString("result") != null) {
		                    auxresult = rs.getInt("result");
		                  }

		                  auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
		                }
		              
		              
		              if (auxerrormsg != null) {
		                  auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
		                } else {
		                  auxerrormsg = "@Ordenes de Trabajo Generadas Correctamente@";
		                }
		              mensajeop=auxerrormsg;
		              rs.close();
		              ps.close();
		              
		              
		              try {  

		        		  Boolean emailNoEnviado = true;
		            	  
		            	  NEO_Funciones_Aux opeaux= new NEO_Funciones_Aux();
		            	  
		            	  NEOCorreos[] neoCorreos=null;
		            	 
		            	  neoCorreos= NEOCorreos.obtenerCorreos(this);
		            	  
		            	  String numDocumento=null;
		            	  
		            	  for (NEOCorreos correos : neoCorreos) {
		            		  NEOCorreos[] creados=null;
		            		  creados=NEOCorreos.obtenerCreados(this, correos.dato1, strOrder);
		            		  
		            		  /*CORREOS*/
		            		  numDocumento="";
		    		          sqlQuery = "select documentno as dato1 " + 
		    		          		" 	from c_order " + 
		    		          		" 	where c_doctype_id='"+correos.dato1+"'" + 
		    		          		" 	and quotation_id='"+strOrder+"'";

		    		          ps = conn.prepareStatement(sqlQuery);
		    		          rs = ps.executeQuery();
		    		          
		    		          
		    		          while (rs.next()) {
		    		                  if (rs.getString("dato1") != null) {
		    		                	  numDocumento=rs.getString("dato1");
		    		                    System.out.println(rs.getString("dato1"));
		    		                  }
		    		                }
		    		           rs.close();
		    		           ps.close();

		            		  if(!numDocumento.isEmpty()) {
		            			  
		            			  String strReceptor = correos.dato2;
		            			  
		            			  NEOCorreos[] tipodoc=null;
		            			  tipodoc=NEOCorreos.obtenerNombreDocumento(this, correos.dato1);
		            			  
		            			  String asunto="NUEVA "+ tipodoc[0].dato1 +" " + numDocumento;
		            			  
		            		        if (opeaux.enviarCorreo(strReceptor, asunto,
		  		                          "Estimado(a), \n Se ha generado la "+ tipodoc[0].dato1 +" " + numDocumento+ " , del Tercero "+ nombreTercero +". Proveniente de la cotizacion numero: " + numCotizaP,
		  		                          null, null)) {
		            		                myError = new OBError();
		            		                myError.setType("Success");
		            		                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

		            		                String auxerrormsgTrad = "";

		            		                while ((auxerrormsg.indexOf("@") != -1)
		            		                    && (auxerrormsg.indexOf("@", auxerrormsg.indexOf("@") + 1) != -1)
		            		                    && emailNoEnviado) {
		            		                  int auxIni = auxerrormsg.indexOf("@");
		            		                  int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

		            		                  String auxParteInicio = auxerrormsg.substring(0, auxIni);
		            		                  String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);
		            		                  auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());
		            		                  auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;
		            		                  auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
		            		                }

		            		                auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

		            		                auxerrormsg = auxerrormsgTrad.equals("")
		            		                    ? ("Se envio el correo electronico a: " + strReceptor)
		            		                    : (auxerrormsgTrad + ", Se envio el correo electronico a: " + strReceptor);

		            		                emailNoEnviado = false;
		            		        	
		            		        	log4j.info("LOS CORREOS SE ENVIARON CORRECTAMENTE");
		            		        	
		  		                        }else {
		  		                        log4j.info("NO SE ENVIARON LOS CORREOS");
		  		                        }
		            		        
		            		        	auxerrormsg="ENVIADOS CORRECTAMENTE";
		  		                      } 
		            		  /*********/
		            		  }
		            	  OBDal.getInstance().commitAndClose();
		  		        } catch (Exception ex) {
		  		        	OBContext.restorePreviousMode();
		  		        	System.out.println(ex.getMessage().toString());
		  		        	auxerrormsg="ERROR AL INTENTAR ENVIAR LOS CORREOS";
		  		        }
		              
		              	myError = new OBError();
		                myError.setType("Success");
		                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
		                myError.setMessage(mensajeop+" / CORREOS: "+auxerrormsg ); 
		              
		      }			  
		      	
                
		  } catch (Exception e) {
		      e.printStackTrace();
		      log4j.warn("Rollback in transaction");
		      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
		      myError = new OBError();
		      myError.setType("Error");
		      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
		      myError.setMessage("Error "+ e.getMessage());
		    } finally {
		      OBContext.restorePreviousMode();
		      if (conn != null) {
		        try {               
		          conn.close();
		        } catch (Exception ex) {
		        }
		        
		      }
		    }

		  
		    return myError;
		 
		  
	  }
	  
	  
	  
	  private void EnviarCorreo(VariablesSecureApp vars,String strOrder, String numCotiza) {
		  
		  OBError myError = null;
		  String auxerrormsg = null;
		  Boolean emailNoEnviado = true;
		  try {
            	  
                     	
		  	} catch (Exception e) {
		  		log4j.info("ERROR AL INTENTAR ENVIAR LOS CORREOS"+e.getMessage());
		}
	}
}
