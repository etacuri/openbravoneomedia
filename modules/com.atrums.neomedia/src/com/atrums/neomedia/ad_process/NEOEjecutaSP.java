package com.atrums.neomedia.ad_process;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
 
public class NEOEjecutaSP implements FieldProvider {
  static Logger log4j = Logger.getLogger(NEOEjecutaSP.class);
  private String InitRecordNumber = "0";

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }


  public static String ejecutaProcedure(ConnectionProvider connectionProvider,
      String srtrProccedure, String strParametro, String strParametro1, String strParametro2,
      String strParametro3, String strParametro4, String strColumnName) throws ServletException {
    String strSql = "";
    strSql = strSql + " SELECT * FROM " + srtrProccedure.toUpperCase() + "('" + strParametro
        + "','" + strParametro1 + "'," + strParametro2 + "," + strParametro3 + ",'" + strParametro4 + "')";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    try {
      st = connectionProvider.getPreparedStatement(strSql);
      result = st.executeQuery();

      if (result.next()) {
        strReturn = UtilSql.getValue(result, strColumnName);
      }
      result.close();
    } catch (SQLException e) {
      log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@"
          + e.getMessage());
    } catch (Exception ex) {
      log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch (Exception ignore) {
        ignore.printStackTrace();
      }
    }
    return (strReturn);
  }

  @Override
  public String getField(String fieldName) {
    // TODO Auto-generated method stub
      return null;
    }
}