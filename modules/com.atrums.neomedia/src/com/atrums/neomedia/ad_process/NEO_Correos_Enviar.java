package com.atrums.neomedia.ad_process;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;

public class NEO_Correos_Enviar extends HttpSecureAppServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	  public void init(ServletConfig config) {
	    super.init(config);
	    boolHist = false;
	  }

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest req, HttpServletResponse resp,VariablesSecureApp vars,String strOrder, String numCotiza) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 OBError myError = null;
		    Connection connect = null;
		    Boolean emailNoEnviado = true;
		    OBContext.setAdminMode(true);
		    String auxerrormsg = null;
		  try {
			  connect = OBDal.getInstance().getConnection();
         	  NEO_Funciones_Aux opeaux= new NEO_Funciones_Aux();
         	  
         	  NEOCorreos[] neoCorreos=null;
         	 
         	  neoCorreos= NEOCorreos.obtenerCorreos(this);
         	  
         	  for (NEOCorreos correos : neoCorreos) {
         		  NEOCorreos[] creados=null;
         		  creados=NEOCorreos.obtenerCreados(this, correos.dato1, strOrder);
         		  
         		  if(creados.length > 0) {
         			  
         			  String strReceptor = correos.dato2;
         			  
         			  NEOCorreos[] tipodoc=null;
         			  tipodoc=NEOCorreos.obtenerNombreDocumento(this, correos.dato1);
         			  
         			  String asunto="Nueva "+ tipodoc[0].dato1 +" " + creados[0].dato1;
         			  
         		        if (opeaux.enviarCorreo(strReceptor, asunto,
		                          "Estimado(a), \n Se ha generado la "+ tipodoc[0].dato1 +" " + creados[0].dato1+ " de la cotizacion numero: " +numCotiza,
		                          null, null)) {

         		                myError = new OBError();
         		                myError.setType("Success");
         		                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

         		                String auxerrormsgTrad = "";

         		                while ((auxerrormsg.indexOf("@") != -1)
         		                    && (auxerrormsg.indexOf("@", auxerrormsg.indexOf("@") + 1) != -1)
         		                    && emailNoEnviado) {
         		                  int auxIni = auxerrormsg.indexOf("@");
         		                  int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

         		                  String auxParteInicio = auxerrormsg.substring(0, auxIni);
         		                  String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);
         		                  auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());
         		                  auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;
         		                  auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
         		                }

         		                auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

         		                auxerrormsg = auxerrormsgTrad.equals("")
         		                    ? ("Se envio el correo electronico a: " + strReceptor)
         		                    : (auxerrormsgTrad + ", Se envio el correo electronico a: " + strReceptor);

         		                emailNoEnviado = false;
         		        	
         		        	log4j.info("LOS CORREOS SE ENVIARON CORRECTAMENTE");
		                      
		                        }else {
		                        	log4j.info("NO SE ENVIARON LOS CORREOS");
		                        }

		                      
		                      } 
         		 OBDal.getInstance().commitAndClose();
         		  }
         	  
                  	
		} catch (Exception e) {
			log4j.info("ERROR AL INTENTAR ENVIAR LOS CORREOS"+e.getMessage());
		}finally {

		      OBContext.restorePreviousMode();
		      if (connect != null) {
		        try {               
		        	connect.close();
		        } catch (Exception ex) {
		        }
		        
		      }
		    
		}
		
	}
	

}
