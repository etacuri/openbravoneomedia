package com.atrums.neomedia.ad_process;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.ConnectionProviderImpl;
import org.openbravo.exception.PoolNotFoundException;

import com.atrums.nomina.util.UtilNomina;




public class CapaNeoIntermedia extends Thread {
	private static final Logger log = Logger.getLogger(CapaNeoIntermedia.class);
	  NEOGenFact partner = null;
	  public NEOGenerarFactura generarCabecera(String idCOrder) throws ServletException {
		    NEOGenerarFactura[] datos = partner.generarCabecera(idCOrder);
		    if (datos != null || datos.length == 1) {
		      return datos[0];
		    }else {
			return null;
		    }
		  }
	public NEOGenerarFactura generarLineas(String idCOrder,String idInvoice, String numLinea) throws ServletException {
		    NEOGenerarFactura[] datos = partner.generarLineas(idCOrder, idInvoice, numLinea);
		    return datos[0];
	}
	public NEOGenerarFactura validarLinea(String idCOrder) throws ServletException {
	    NEOGenerarFactura[] datos = partner.validarLinea(idCOrder);
	    return datos[0];
	}
	public NEOGenerarFactura obtenerNumDoc(String idInvoice) throws ServletException {
	    NEOGenerarFactura[] datos = partner.obtenerNumDoc(idInvoice);
	    return datos[0];
	}
	
	public NEOGenerarFactura obtenerEsPublicidad(String idCOrder) throws ServletException {
	    NEOGenerarFactura[] datos = partner.obtenerEsPublicidad(idCOrder);
	    return datos[0];
	}
	
	public NEOGenerarFactura obtenerEsFacturaParte(String idCOrder) throws ServletException {
	    NEOGenerarFactura[] datos = partner.obtenerEsFacturaParte(idCOrder);
	    return datos[0];
	}
	
	public NEOGenerarFactura obtenerCantFacturar(String idCOrder) throws ServletException {
	    NEOGenerarFactura[] datos = partner.obtenerCantFacturar(idCOrder);
	    return datos[0];
	}
	/*public NEOGenerarFactura obtieneIdInvoice(String idPartner) throws ServletException {
	 *   NEOGenerarFactura[] datos = partner.obtieneIdInvoice(idPartner);
	 *   return datos[0];
	}*/
	
	public String obtieneIdInvoice(String idPartner) throws ServletException {
	   String datos = partner.obtieneIdInvoice(idPartner);
	    if (!datos.equals("0")) {
	    	return datos;	
	    }else
	    {
	    	return "0";
	    }
	    	
	    
	}
	public NEOGenerarFactura obtieneIdTercero(String idCOrder) throws ServletException {
	    NEOGenerarFactura[] datos = partner.obtieneIdTercero(idCOrder);
	    return datos[0];
	}
	
	  public CapaNeoIntermedia() throws PoolNotFoundException {
		    String strDirectorio = UtilNomina.class.getResource("/").getPath();
		    Integer value = strDirectorio.indexOf("/src-core/build/classes/");
		    try {
		      if (value > 0) {
		        strDirectorio = strDirectorio.substring(1, value + 1);
		        strDirectorio = strDirectorio + "config/";
		      } else {
		        value = strDirectorio.indexOf("/build/classes/");
		        if (value > 0) {
		          strDirectorio = strDirectorio.substring(1, value + 1);
		          strDirectorio = strDirectorio + "config/";
		        } else {
		          value = strDirectorio.indexOf("/WEB-INF/classes/");
		          log.info("strDirectorio " + value);
		          if (value > 0) {
		            strDirectorio = strDirectorio.substring(1, value + 1);
		            strDirectorio = "/" + strDirectorio + "" + "WEB-INF/";
		            log.info(strDirectorio);
		          } else {
		            log.info("Problema en el Path " + value);
		            strDirectorio = "/var/lib/tomcat6/webapps/openbravo/WEB-INF/";
		          }
		        }
		      }
		    } catch (Exception e) {
		      log.error(e.toString() + " --------------" + strDirectorio);
		    }
		    final ConnectionProvider conn = new ConnectionProviderImpl(strDirectorio
		        + "/Openbravo.properties");
		    partner = new NEOGenFact(conn);
		  }
}
