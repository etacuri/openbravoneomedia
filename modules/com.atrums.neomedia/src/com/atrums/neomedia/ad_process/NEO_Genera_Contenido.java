package com.atrums.neomedia.ad_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.xmlEngine.XmlDocument;

public class NEO_Genera_Contenido extends HttpSecureAppServlet {
	
	
	  /**
	   * 
	   */
	  private static final long serialVersionUID = 1L;
	  

	@Override
	  public void init(ServletConfig config) {
	    super.init(config);
	    boolHist = false;
	  }
	  
	  
	  public void doPost(HttpServletRequest request, HttpServletResponse response)
		      throws IOException, ServletException {
		    VariablesSecureApp vars = new VariablesSecureApp(request);
		    
		    if (vars.commandIn("DEFAULT")) {
		      String strProcessId = vars.getStringParameter("inpProcessId");
		      String strWindow = vars.getStringParameter("inpwindowId");
		      String strTab = vars.getStringParameter("inpTabId");
		      String strKey = vars.getGlobalVariable("inpneoOrdenPrimerVId", strWindow + "|C_Orderline_ID");
		      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
		    } else if (vars.commandIn("SAVE")) {
		      String strWindow = vars.getStringParameter("inpWindowId");
		      String strOrderline = vars.getStringParameter("inpneoOrdenPrimerVId");
		      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|NEO_Orden_Primer_V_ID");
		      String strTab = vars.getStringParameter("inpTabId");
		      String strWindowPath = Utility.getTabURL(strTab, "R", true);
		      if (strWindowPath.equals("")) {
		        strWindowPath = strDefaultServlet;
		      }

		      OBError myError = processButton(vars, strKey, strOrderline);
		      log4j.debug(myError.getMessage());
		      vars.setMessage(strTab, myError);
		      printPageClosePopUp(response, vars, strWindowPath);
		      
		    
		    }
		  }
	  
	  
	  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
		      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

		    Connection conn = null;

		    try {
		      XmlDocument xmlDocument = xmlEngine
		          .readXmlTemplate("com/atrums/neomedia/ad_process/NEO_Genera_Contenido")
		          .createXmlDocument();
		      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
		      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		      xmlDocument.setParameter("theme", vars.getTheme());
		      xmlDocument.setParameter("key", strKey);
		      xmlDocument.setParameter("window", windowId);
		      xmlDocument.setParameter("tab", strTab);

		      response.setContentType("text/html; charset=UTF-8");
		      PrintWriter out = response.getWriter();
		      out.println(xmlDocument.print());
		      out.close();

		    } catch (Exception ex) {
		      // TODO: handle exception
		      throw new ServletException(ex);
		    } finally {
		      if (conn != null) {
		        try {
		          conn.close();
		        } catch (Exception e) {
		        }
		      }
		    }
		  }
	  
	  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrderline) {
		  
		  OBError myError = null;
		  Connection conn = OBDal.getInstance().getConnection();
		  OBContext.setAdminMode(true);
		  String mensajeop = null;
		  String auxerrormsg = null;
		  String idAsignado="";
		  String numDocumento="";
		  String clienteNombre="";
		  int auxresult = 0;
		  try {
			  OrderLine orderline = OBDal.getInstance().get(OrderLine.class, strOrderline);	
			  clienteNombre=orderline.getBusinessPartner().getName();
			  NEOCorreos[] neoIdAsignado=null;
         	 
			  neoIdAsignado= NEOCorreos.obtenerIdAsignado(this,strOrderline);
			  
			  idAsignado=neoIdAsignado[0].dato1;
			  
			  if(!idAsignado.isEmpty()) {
				  String sqlQuery = "";

			      String strPInstance = null;

			      PreparedStatement ps = null;
			      ResultSet rs = null;
			      
			      strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();

			      sqlQuery = "INSERT INTO ad_pinstance("
			          + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
			          + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
			          + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance
			          + "', '20F8F00E71C74BC092F7062DA3FC1DC7', '" + strOrderline + "', 'N', now(), "
			          + "'"+vars.getUser()+"', now(), null, null, '" + vars.getClient() + "', '"+vars.getOrg()+"', " + "'100', '100', 'Y');";

			      ps = conn.prepareStatement(sqlQuery);
			      int updateOrder = ps.executeUpdate();
			      
			      ps.close();
			      if(updateOrder == 1) {
			    	 
			    	  sqlQuery = "SELECT * FROM neo_gen_caso_contenido_ind('" + strPInstance + "')";
			    	  ps = conn.prepareStatement(sqlQuery);
			          ps.execute();
			          ps.close();
			    	  
			          
			          sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
			                  + strPInstance + "'";

			          ps = conn.prepareStatement(sqlQuery);
			          rs = ps.executeQuery();
			             
			              while (rs.next()) {
			                  if (rs.getString("result") != null) {
			                    auxresult = rs.getInt("result");
			                  }

			                  auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
			                }
			              
			              
			              if (auxerrormsg != null) {
			                  auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
			                } else {
			                  auxerrormsg = "@Casos Contenidos Generado Exitosamente@";
			                }
			              mensajeop=auxerrormsg;
			              rs.close();
			              ps.close();
			              
			              
			              try {
			            	  
			            	  
			            	  numDocumento="";
		    		          sqlQuery = "select documentno as dato1 " + 
		    		          		" 	from neo_monitoreo " + 
		    		          		" 	where ordentrabajolinea_id='"+strOrderline+"'" + 
		    		          		" 	and docstatus='AB'" +
		    		          		"	order by created desc limit 1";

		    		          ps = conn.prepareStatement(sqlQuery);
		    		          rs = ps.executeQuery();
		    		          
		    		          while (rs.next()) {
	    		                  if (rs.getString("dato1") != null) {
	    		                	  numDocumento=rs.getString("dato1");
	    		                  }
	    		                }
	    		           rs.close();
	    		           ps.close();


			        		  Boolean emailNoEnviado = true;
			            	  
			            	  NEO_Funciones_Aux opeaux= new NEO_Funciones_Aux();
			            	  
			            	  NEOCorreos[] neoCorreoTercero=null;
			            	 
			            	  neoCorreoTercero= NEOCorreos.obtenerCorreoTercero(this,idAsignado);
			            	  
			            	  String emailAsignado=neoCorreoTercero[0].dato1;

			            		  if(!emailAsignado.isEmpty()) {
			            			  
			            			  String strReceptor = emailAsignado;
			            			  
			            			  
			            			  String asunto="NUEVO CASO DE CONTENIDO N° "+numDocumento;
			            			  
			            		        if (opeaux.enviarCorreo(strReceptor, asunto,
			  		                          "Estimado(a), \n Se le ha asignado un nuevo caso de contenido N° "+numDocumento+", correspondiente al cliente: "+clienteNombre,
			  		                          null, null)) {
			            		        	OBDal.getInstance().commitAndClose();
			            		        	
			            		                myError = new OBError();
			            		                myError.setType("Success");
			            		                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

			            		                String auxerrormsgTrad = "";

			            		                while ((auxerrormsg.indexOf("@") != -1)
			            		                    && (auxerrormsg.indexOf("@", auxerrormsg.indexOf("@") + 1) != -1)
			            		                    && emailNoEnviado) {
			            		                  int auxIni = auxerrormsg.indexOf("@");
			            		                  int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

			            		                  String auxParteInicio = auxerrormsg.substring(0, auxIni);
			            		                  String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);
			            		                  auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());
			            		                  auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;
			            		                  auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
			            		                }

			            		                auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

			            		                auxerrormsg = auxerrormsgTrad.equals("")
			            		                    ? ("Se envio el correo electronico a: " + strReceptor)
			            		                    : (auxerrormsgTrad + ", Se envio el correo electronico a: " + strReceptor);

			            		                emailNoEnviado = false;
			            		        	
			            		        	log4j.info("LOS CORREOS SE ENVIARON CORRECTAMENTE");
			            		        	
			  		                        }else {
			  		                        log4j.info("NO SE ENVIARON LOS CORREOS");
			  		                        }
			            		        
			            		        	auxerrormsg="ENVIADOS CORRECTAMENTE";
			  		                      } 
			            		  
			            	  
			  		        } catch (Exception ex) {
			  		        	OBContext.restorePreviousMode();
			  		        	System.out.println(ex.getMessage().toString());
			  		        	auxerrormsg="ERROR AL INTENTAR ENVIAR LOS CORREOS";
			  		        }
			              
			              	myError = new OBError();
			                myError.setType("Success");
			                myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
			                myError.setMessage(mensajeop+" / CORREOS: "+auxerrormsg ); 
			              
			      }			  
				  
				  
			  }else {
					myError = new OBError();
	                myError.setType("Error");
	                myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
	                myError.setMessage("NO SE HA ASIGNADO EL RESPONSABLE EN LA LINEA SELECCIONADA"); 
				  
			  }	  
                
		  } catch (Exception e) {
		      e.printStackTrace();
		      log4j.warn("Rollback in transaction");
		      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
		      myError = new OBError();
		      myError.setType("Error");
		      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
		      myError.setMessage("Error "+ e.getMessage());
		    } finally {
		      OBContext.restorePreviousMode();
		      if (conn != null) {
		        try {               
		          conn.close();
		        } catch (Exception ex) {
		        }
		        
		      }
		    }

		  
		    return myError;
		 
		  
	  }
	  
}
