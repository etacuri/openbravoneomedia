package com.atrums.neomedia.ad_process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.openbravo.dal.core.OBContext;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.utils.FormatUtilities;

public class NEO_Funciones_Aux {
	  
	static Logger log4j = Logger.getLogger(NEO_Funciones_Aux.class);
	
	public File bytetofile(byte[] bytes) throws IOException {
	    File fldocumen = File.createTempFile("documento", ".xml", null);
	    fldocumen.deleteOnExit();
	    byte[] bytesde = Base64.decodeBase64(bytes);

	    FileOutputStream os = new FileOutputStream(fldocumen);

	    os.write(bytesde);
	    os.close();

	    return fldocumen;
	  }

	
	public boolean enviarCorreo(final String recipient, final String subject, final String body,
		      final String type, final List<File> attachments) {
		    try {
		      Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
		      final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

		      if (mailConfig == null) {
		        return false;
		      }

		      final String username = mailConfig.getSmtpServerAccount();
		      final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(),
		          false);
		      final String connSecurity = mailConfig.getSmtpConnectionSecurity();
		      final int port = mailConfig.getSmtpPort().intValue();
		      final String senderAddress = mailConfig.getSmtpServerSenderAddress();
		      final String host = mailConfig.getSmtpServer();
		      final boolean auth = mailConfig.isSMTPAuthentification();

		      EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
		          recipient, null, null, null, subject, body, type, null, null, null);
		      return true;
		    } catch (Exception e) {
		      // TODO: handle exception
		      return false;
		    }
		  }
}
