package com.atrums.neomedia.ad_process;
import javax.servlet.http.HttpServletRequest;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBErrorBuilder;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DbUtility;


public class NEOCrearProductoNuevo extends DalBaseProcess {

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		  ConnectionProvider conn = bundle.getConnection();
		  HttpServletRequest request = RequestContext.get().getRequest();
		  VariablesSecureApp vars = new VariablesSecureApp(request);
		  try {
			  String orderIdaux = (String) bundle.getParams().get("C_Order_ID");
		      Order objOrderaux = OBDal.getInstance().get(Order.class, orderIdaux);
		      for (OrderLine ordLineaux : objOrderaux.getOrderLineList()) {
			        try {
			        	if (ordLineaux.isNeoIsnewprod()){
				 	       NEOCrearProducto[] crearProducto=NEOCrearProducto.crearProducto(conn, ordLineaux.getNeoNewname(), ordLineaux.getNeoMPcategory().getId(), ordLineaux.getNeoTipoProducto(), ordLineaux.getTax().getId());
				 	       String idProducto=(crearProducto[0].productoid);
				 	       NEOCrearProducto.actualizarLinea(conn, idProducto, ordLineaux.getId());
				        }
					} catch (Exception er) {
						System.out.println(er.getMessage());
					}
		      }
		      
		      NEOCrearProducto[] actOrdenFlag=NEOCrearProducto.updateFlagOrder(conn,orderIdaux);
		      OBError result = OBErrorBuilder.buildMessage(null, "success", "@Productos creados correctamente@ ");
			      bundle.setResult(result);
		} catch (Exception e) {
			 Throwable t = DbUtility.getUnderlyingSQLException(e);
			 final OBError error = OBMessageUtils.translateError(bundle.getConnection(), vars,
			          vars.getLanguage(), t.getMessage());
			bundle.setResult(error);
		}
		
	}
	  
}
