package com.atrums.neomedia.ad_process;

import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.base.model.ModelProvider;

public class NEOGeneraFactura extends BaseActionHandler {
	static Logger log4j = Logger.getLogger(NEOGeneraFactura.class);

	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {

		JSONObject result = new JSONObject();
		try {
			JSONObject message = new JSONObject();
			final JSONObject jsonData = new JSONObject(content);
			final JSONArray lineasIds = jsonData.getJSONArray("lineas");

			int cont = 0;
			
			int contauxter = 0;
			
			/*
			 * El invoiceId representa el valor que retorna de la validacion para verificar
			 * si la linea ya ha sido generada, mirar xsql
			 */
			
			
			CapaNeoIntermedia proceso = new CapaNeoIntermedia();
			
			//PRIMER ID
			String idValidador="";
			NEOGenerarFactura valiTercero = proceso.obtieneIdTercero(lineasIds.getString(0));		
			idValidador=valiTercero.dato1;
			
			//BUCLE PARA VERIFICAR SI EXISTEN DIFERENTES TERCEROS
			for (int i = 0; i < lineasIds.length(); i++) {
				NEOGenerarFactura val = proceso.obtieneIdTercero(lineasIds.getString(i));		
				if (!idValidador.equals(val.dato1)) {
					contauxter++;
				}
			}
			////
			
			for (int i = 0; i < lineasIds.length(); i++) {
				NEOGenerarFactura val = proceso.validarLinea(lineasIds.getString(i));
				if ((val.invoiceId.equals("T")) || (val.invoiceId.equals("P"))) {
					cont++;
				}
			}
			
			/*
			 * AGREADO PARA CONTROLAR CANTIDADES EN PUBLICIDAD
			 * 
			 * */
			NEOGenerarFactura ValidacionPubli = proceso.obtenerEsPublicidad(lineasIds.getString(0));
			NEOGenerarFactura ValidacionFactParte = proceso.obtenerEsFacturaParte(lineasIds.getString(0));
			String facturaparte=ValidacionFactParte.dato1;
			int valcant=0;
			int valvacio=0;
			
			/*
			 * Inicialmente era solo para publicidad luego se abrio por eso algunos 
			 * Pero todo gira en base ak camppo esfacturaparte
			 */
			
			if(facturaparte.equals("Y")) {
				for (int i = 0; i < lineasIds.length(); i++) {
					NEOGenerarFactura valpubli = proceso.obtenerCantFacturar(lineasIds.getString(i));
					if(valpubli.dato1.isEmpty()||valpubli.dato2.isEmpty()) {
						valvacio++;
					}else {
						Double d1 = Double.valueOf(valpubli.dato1);
						Double d2 = Double.valueOf(valpubli.dato2);
						if ((d1 > d2)) {
							valcant++;
						}
					}
				}
			}
				
			
			/*
			 * */
			
			if (contauxter>0) {
				String mensaje = "LAS LINEA(S) SELECCIONADA(S) CORRESPONDEN A DIFERENTES TERCEROS";
				message.put("severity", "warning");
				message.put("text", mensaje);
				result.put("message", message);
			}
			else {
				if (cont == 0) {
					String mensaje = "LAS LINEA(S) SELECCIONADA(S) YA HAN SIDO FACTURADAS";
					message.put("severity", "warning");
					message.put("text", mensaje);
					result.put("message", message);
				} else {
					if(facturaparte.equals("Y")) {
						if(valcant>0) {
							String mensaje = "UNA DE LAS LINEAS SELECCIONADAS CONTIENE UNA CANTIDAD A FACTURAR MAYOR A LA PERMITIDA";
							message.put("severity", "warning");
							message.put("text", mensaje);
							result.put("message", message);
						}
				    		else {
							if(valvacio>0) {
								String mensaje = "UNA DE LAS LINEAS SELECCIONADAS NO CONTIENE UN VALOR EN EL CAMPO CANTIDAD A FACTURAR";
								message.put("severity", "warning");
								message.put("text", mensaje);
								result.put("message", message);
							}
							else {
								String idInvoice;
								/*NEOGenerarFactura valTercero = proceso.obtieneIdTercero(lineasIds.getString(0));
								*String valFacAbierta = proceso.obtieneIdInvoice(valTercero.dato1);
								
								*if(!valFacAbierta.equals("0")) {
								*	idInvoice=valFacAbierta;
								*}else {
								*/
								NEOGenerarFactura cInvoiceID = proceso.generarCabecera(lineasIds.getString(0));
								idInvoice=cInvoiceID.invoiceId;
								/*}*/
								
								if (!idInvoice.isEmpty()) {
									int numlinea=1;
									
									for (int i = 0; i < lineasIds.length(); i++) {
										NEOGenerarFactura val = proceso.validarLinea(lineasIds.getString(i));
										if ((val.invoiceId.equals("T")) || (val.invoiceId.equals("P"))) {
											
											NEOGenerarFactura resp = proceso.generarLineas(lineasIds.getString(i), idInvoice,
											String.valueOf(numlinea));
											System.out.println(lineasIds.getString(i) + resp);
											numlinea++;
										}
									}
									/*
									 * El numDoc.invoiceId representa el valor que retorna con el numero de documento
									 */
									NEOGenerarFactura numDoc = proceso.obtenerNumDoc(idInvoice);
									String mensaje = "EL DOCUMENTO DE VENTA HA SIDO GENERADO EXITOSAMENTE, NUMERO DE DOCUMENTO:  "+numDoc.invoiceId;
									message.put("severity", "success");
									message.put("text", mensaje);
									result.put("message", message);
								}
						
							}
						}
							
						}else {
							
						String idInvoice;
						/*
						NEOGenerarFactura valTercero = proceso.obtieneIdTercero(lineasIds.getString(0));
						String valFacAbierta = proceso.obtieneIdInvoice(valTercero.dato1);
						if(!valFacAbierta.equals("0")) {
							idInvoice=valFacAbierta;
						}else {*/
						
							NEOGenerarFactura cInvoiceID = proceso.generarCabecera(lineasIds.getString(0));
							idInvoice=cInvoiceID.invoiceId;
						/*}*/
						
						
						if (!idInvoice.isEmpty()) {
							int numlinea=1;
							
							for (int i = 0; i < lineasIds.length(); i++) {
								NEOGenerarFactura val = proceso.validarLinea(lineasIds.getString(i));
								if ((val.invoiceId.equals("T")) || (val.invoiceId.equals("P"))) {
									
									NEOGenerarFactura resp = proceso.generarLineas(lineasIds.getString(i), idInvoice,
										String.valueOf(numlinea));
									System.out.println(lineasIds.getString(i) + resp);
									numlinea++;
									
								}
							}
							/*
							 * El numDoc.invoiceId representa el valor que retorna con el numero de documento
							 */
							NEOGenerarFactura numDoc = proceso.obtenerNumDoc(idInvoice);
							String mensaje = "EL DOCUMENTO DE VENTA HA SIDO GENERADO EXITOSAMENTE, NUMERO DE DOCUMENTO:  "+numDoc.invoiceId;
							message.put("severity", "success");
							message.put("text", mensaje);
							result.put("message", message);
						
						  }
						 
					}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			OBDal.getInstance().rollbackAndClose();
			log4j.error("BaseInvoiceProcess error: " + e.getMessage(), e);

			Throwable ex = DbUtility.getUnderlyingSQLException(e);
			String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();

			try {
				result.put("retryExecution", true);
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", message);
				result.put("message", errorMessage);
				e.printStackTrace();
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return result;

	}

}
