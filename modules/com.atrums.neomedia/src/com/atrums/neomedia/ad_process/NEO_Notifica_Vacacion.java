package com.atrums.neomedia.ad_process;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

public class NEO_Notifica_Vacacion implements Process {

	static Logger log4j = Logger.getLogger(NEO_Notifica_Vacacion.class);
	
	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		
		final StringBuilder sb = new StringBuilder();
		sb.append("Resultados de envío de correos:<br/>");
		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();
		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(),
		varsAux.getOrg());
		
		try {			
			NEOCorreos[] vacaciones=NEOCorreos.obtenerVacacionesAP(conn);
			if((vacaciones==null)||(vacaciones.length==0)){
				sb.append("NO EXISTEN VACACIONES APROBADAS<br/>");
			}else {
				NEO_Funciones_Aux opeaux= new NEO_Funciones_Aux();
				//SimpleDateFormat fechaFormato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				for (NEOCorreos s : vacaciones) {
						
					/*
					 * CONTENIDO USADO EN CORREO
					 */
					
					String strReceptor = s.datocorreo.toString();
					
					if(!strReceptor.isEmpty()) {
						
						 String asunto="APROBACION DE VACACIONES";			
						 
						 String cuerpo = "Estimado(a) " + s.dato2 + ".\n";
						 cuerpo=cuerpo + "Sus vacaciones del " + s.dato3 + " al " + s.dato4 + " han sido aprobadas.\n";
						 cuerpo=cuerpo + "\n";
						 
						 String cuerpo1="FELICES VACACIONES.\n";
						 cuerpo1=cuerpo1+"Saludos,\n";
						 cuerpo1=cuerpo1+ "\n";
						 cuerpo1=cuerpo1+"NEOMEDIA S.A";
						
						 //String cuerpo2="Fecha de Inicio: "+fechaFormato.format(csop.getInicio())+"\n";
						 												 
						 if (opeaux.enviarCorreo(strReceptor, asunto, cuerpo+cuerpo1, null, null)) {
							
							 if((NEOCorreos.actualizaVacacion(conn, s.dato1))==1) {
								 sb.append("SE ENVIO CORRECTAMENTE EL CORREO DE LA APROBACION DE VACACIONES");
								 log4j.info("SE ENVIO CORRECTAMENTE EL CORREO DE LA APROBACION DE VACACIONES");
							 }else {
								 log4j.info("ERROR AL ACTUALIZAR EL ESTADO DE VACACIONES");
							 }
						 }
						 			 
					} else {
						sb.append("NO TIENE CONFIGURADO EL CORREO DEL CONTACTO EN EL TERCERO");
						log4j.info("NO TIENE CONFIGURADO EL CORREO DEL CONTACTO EN EL TERCERO");
					}				
					/*
					 * 
					 */					 
				} 
				OBDal.getInstance().commitAndClose();
			}
			
			log4j.info("PROCESO DE NOTIFICACION FINALIZADO CORRECTAMENTE");
			
			  final OBError msg = new OBError();
		      msg.setType("Success");
		      msg.setTitle("Resultados!");
		      msg.setMessage(sb.toString());
		      bundle.setResult(msg);
		} catch (Exception ex) {
			log4j.info("ERROR EN EL PROCESO DE ENVIO DE CORREO" + ex.getMessage().toString());
			final OBError msg = new OBError();
		      msg.setType("Error");
		      msg.setMessage(ex.getMessage());
		      msg.setTitle("Ocurrió un Error¡");
		      bundle.setResult(msg);
		      return;
		 } finally {
		      OBDal.getInstance().rollbackAndClose();
		 }	
	}
}
