OB.NEOM = OB.NEOM || {};

OB.NEOM.Process = {
	execute: function (params, view) {
		//alert('You clicked InCorrecto');
	var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
		lineas = [], messageBar = view.getView(params.adTabId).messageBar, 
		callback;
		
		callback = function (rpcResponse, data, rpcRequest) {
			var status = rpcResponse.status,
			view = rpcRequest.clientContext.view.getView(params.adTabId);
			view.messageBar.setMessage(data.message.severity, null, data.message.text);
			
			params.button.closeProcessPopup();
		};
		
		for (i = 0; i < selection.length; i++) {
			lineas.push(selection[i].id);
		};
		
		OB.RemoteCallManager.call('com.atrums.neomedia.ad_process.NEOGeneraFactura', {
			lineas: lineas,
		    action: params.action
		}, {}, callback, {
      view: view
    });
	},
	
	genfactpubli: function (params, view) {
		params.action = 'PROCESS';
		params.adTabId = 'C3616C3F2AA44BC295D1F44A663AE65D';		
		OB.NEOM.Process.execute(params, view);
	},
	
	genfact: function (params, view) {
		params.action = 'PROCESS';
		params.adTabId = '187';		
		OB.NEOM.Process.execute(params, view);
	},
	
	genfactnew: function (params, view) {
		params.action = 'PROCESS';
		params.adTabId = 'ABAFA613A48742B6A3FA778038C00919';		
		OB.NEOM.Process.execute(params, view);
	}
};