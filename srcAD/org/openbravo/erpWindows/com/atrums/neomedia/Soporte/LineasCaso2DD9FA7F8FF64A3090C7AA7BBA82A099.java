
package org.openbravo.erpWindows.com.atrums.neomedia.Soporte;


import org.openbravo.erpCommon.reference.*;



import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "A5C247AE417447BA98E235046492F35D";
  private static final String tabId = "2DD9FA7F8FF64A3090C7AA7BBA82A099";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "5FE0DB370CD142F38DF6A56A426D7CEB";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     
      if (command.contains("3E8681D5D3D146FF8E1DE942F2E2CEFA")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("3E8681D5D3D146FF8E1DE942F2E2CEFA");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("3E8681D5D3D146FF8E1DE942F2E2CEFA")) {
          classInfo.type = "P";
          classInfo.id = "3E8681D5D3D146FF8E1DE942F2E2CEFA";
        }
      }
     
      if (command.contains("D51C1E722D934F87A6380893A21BF330")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("D51C1E722D934F87A6380893A21BF330");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("D51C1E722D934F87A6380893A21BF330")) {
          classInfo.type = "P";
          classInfo.id = "D51C1E722D934F87A6380893A21BF330";
        }
      }
     

     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strneoCasolineId = request.getParameter("inpneoCasolineId");
         String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strneoCasolineId.equals(""))
              total = saveRecord(vars, myError, 'U', strPNEO_Caso_ID);
          else
              total = saveRecord(vars, myError, 'I', strPNEO_Caso_ID);
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID", "");

      String strNEO_Casoline_ID = vars.getGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID", "");
            if (strPNEO_Caso_ID.equals("")) {
        strPNEO_Caso_ID = getParentID(vars, strNEO_Casoline_ID);
        if (strPNEO_Caso_ID.equals("")) throw new ServletException("Required parameter :" + windowId + "|NEO_Caso_ID");
        vars.setSessionValue(windowId + "|NEO_Caso_ID", strPNEO_Caso_ID);

        refreshParentSession(vars, strPNEO_Caso_ID);
      }


      String strView = vars.getSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strNEO_Casoline_ID.equals("")) strNEO_Casoline_ID = firstElement(vars, tableSQL);
          if (strNEO_Casoline_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strNEO_Casoline_ID, strPNEO_Caso_ID, tableSQL);

      else printPageDataSheet(response, vars, strPNEO_Caso_ID, strNEO_Casoline_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strNEO_Casoline_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strNEO_Casoline_ID.equals("")) strNEO_Casoline_ID = vars.getRequiredGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID");
      else vars.setSessionValue(windowId + "|NEO_Casoline_ID", strNEO_Casoline_ID);
      
      
      String strPNEO_Caso_ID = getParentID(vars, strNEO_Casoline_ID);
      
      vars.setSessionValue(windowId + "|NEO_Caso_ID", strPNEO_Caso_ID);
      vars.setSessionValue("9F4F4FB9795948F89E386CF4DD91931D|Caso.view", "EDIT");

      refreshParentSession(vars, strPNEO_Caso_ID);

      vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view", "EDIT");

      printPageEdit(response, request, vars, false, strNEO_Casoline_ID, strPNEO_Caso_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID", false, false, true, "");
      vars.removeSessionValue(windowId + "|NEO_Casoline_ID");
      refreshParentSession(vars, strPNEO_Caso_ID);


      String strView = vars.getSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view");
      String strNEO_Casoline_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strNEO_Casoline_ID = firstElement(vars, tableSQL);
          if (strNEO_Casoline_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strNEO_Casoline_ID.equals("")) strNEO_Casoline_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strNEO_Casoline_ID, strPNEO_Caso_ID, tableSQL);

      } else printPageDataSheet(response, vars, strPNEO_Caso_ID, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamCBP_Tecnico_ID", tabId + "|paramCBP_Tecnico_ID");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
            String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      
      vars.removeSessionValue(windowId + "|NEO_Casoline_ID");
      String strNEO_Casoline_ID="";

      String strView = vars.getSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strNEO_Casoline_ID = firstElement(vars, tableSQL);
        if (strNEO_Casoline_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strNEO_Casoline_ID, strPNEO_Caso_ID, tableSQL);

      else printPageDataSheet(response, vars, strPNEO_Caso_ID, strNEO_Casoline_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
            String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      

      String strNEO_Casoline_ID = vars.getGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID", "");
      vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view", "RELATION");
      printPageDataSheet(response, vars, strPNEO_Caso_ID, strNEO_Casoline_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");


      printPageEdit(response, request, vars, true, "", strPNEO_Caso_ID, tableSQL);

    } else if (vars.commandIn("EDIT")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      String strNEO_Casoline_ID = vars.getGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID", "");
      vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strNEO_Casoline_ID, strPNEO_Caso_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      String strNEO_Casoline_ID = vars.getRequiredStringParameter("inpneoCasolineId");
      
      String strNext = nextElement(vars, strNEO_Casoline_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, strPNEO_Caso_ID, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      String strNEO_Casoline_ID = vars.getRequiredStringParameter("inpneoCasolineId");
      
      String strPrevious = previousElement(vars, strNEO_Casoline_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, strPNEO_Caso_ID, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {
vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|NEO_Casoline_ID");
      vars.setSessionValue(windowId + "|NEO_Caso_ID", strPNEO_Caso_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|NEO_Casoline_ID");
      vars.setSessionValue(windowId + "|NEO_Caso_ID", strPNEO_Caso_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, strPNEO_Caso_ID, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strPNEO_Caso_ID, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, strPNEO_Caso_ID, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I', strPNEO_Caso_ID);      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      String strNEO_Casoline_ID = vars.getRequiredGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U', strPNEO_Caso_ID);      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strNEO_Casoline_ID, tableSQL);
          vars.setSessionValue(windowId + "|NEO_Casoline_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");

      String strNEO_Casoline_ID = vars.getRequiredStringParameter("inpneoCasolineId");
      //LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data data = getEditVariables(vars, strPNEO_Caso_ID);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.delete(this, strNEO_Casoline_ID, strPNEO_Caso_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|neoCasolineId");
        vars.setSessionValue(tabId + "|LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONProcesarini3E8681D5D3D146FF8E1DE942F2E2CEFA")) {
        vars.setSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strprocesarini", vars.getStringParameter("inpprocesarini"));
        vars.setSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button3E8681D5D3D146FF8E1DE942F2E2CEFA.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "3E8681D5D3D146FF8E1DE942F2E2CEFA", request.getServletPath());    
     } else if (vars.commandIn("BUTTON3E8681D5D3D146FF8E1DE942F2E2CEFA")) {
        String strNEO_Casoline_ID = vars.getGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID", "");
        String strprocesarini = vars.getSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strprocesarini");
        String strProcessing = vars.getSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strProcessing");
        String strOrg = vars.getSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strOrg");
        String strClient = vars.getSessionValue("button3E8681D5D3D146FF8E1DE942F2E2CEFA.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesarini3E8681D5D3D146FF8E1DE942F2E2CEFA(response, vars, strNEO_Casoline_ID, strprocesarini, strProcessing);
        }

     } else if (vars.commandIn("BUTTONProcesarfinD51C1E722D934F87A6380893A21BF330")) {
        vars.setSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strprocesarfin", vars.getStringParameter("inpprocesarfin"));
        vars.setSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonD51C1E722D934F87A6380893A21BF330.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "D51C1E722D934F87A6380893A21BF330", request.getServletPath());    
     } else if (vars.commandIn("BUTTOND51C1E722D934F87A6380893A21BF330")) {
        String strNEO_Casoline_ID = vars.getGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID", "");
        String strprocesarfin = vars.getSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strprocesarfin");
        String strProcessing = vars.getSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strProcessing");
        String strOrg = vars.getSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strOrg");
        String strClient = vars.getSessionValue("buttonD51C1E722D934F87A6380893A21BF330.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesarfinD51C1E722D934F87A6380893A21BF330(response, vars, strNEO_Casoline_ID, strprocesarfin, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONProcesarini3E8681D5D3D146FF8E1DE942F2E2CEFA")) {
        String strNEO_Casoline_ID = vars.getGlobalVariable("inpKey", windowId + "|NEO_Casoline_ID", "");
        String strprocesarini = vars.getStringParameter("inpprocesarini");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "3E8681D5D3D146FF8E1DE942F2E2CEFA", (("NEO_Casoline_ID".equalsIgnoreCase("AD_Language"))?"0":strNEO_Casoline_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONProcesarfinD51C1E722D934F87A6380893A21BF330")) {
        String strNEO_Casoline_ID = vars.getGlobalVariable("inpKey", windowId + "|NEO_Casoline_ID", "");
        String strprocesarfin = vars.getStringParameter("inpprocesarfin");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "D51C1E722D934F87A6380893A21BF330", (("NEO_Casoline_ID".equalsIgnoreCase("AD_Language"))?"0":strNEO_Casoline_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      String strPNEO_Caso_ID = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType, strPNEO_Caso_ID);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data getEditVariables(Connection con, VariablesSecureApp vars, String strPNEO_Caso_ID) throws IOException,ServletException {
    LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data data = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data();
    ServletException ex = null;
    try {
   try {   data.line = vars.getRequiredNumericParameter("inpline");  } catch (ServletException paramEx) { ex = paramEx; }     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.fechaLineacaso = vars.getStringParameter("inpfechaLineacaso");     data.cbpTecnicoId = vars.getRequiredStringParameter("inpcbpTecnicoId");     data.cbpTecnicoIdr = vars.getStringParameter("inpcbpTecnicoId_R");     data.estadolinea = vars.getRequestGlobalVariable("inpestadolinea", windowId + "|Estadolinea");     data.estadolinear = vars.getStringParameter("inpestadolinea_R");    try {   data.tiempoEjecutado = vars.getNumericParameter("inptiempoEjecutado");  } catch (ServletException paramEx) { ex = paramEx; }     data.finicio = vars.getStringParameter("inpfinicio");     data.neoCasoId = vars.getRequiredStringParameter("inpneoCasoId");     data.ffin = vars.getStringParameter("inpffin");     data.comentarios = vars.getStringParameter("inpcomentarios");     data.escalamiento = vars.getRequiredStringParameter("inpescalamiento");     data.escalamientor = vars.getStringParameter("inpescalamiento_R");     data.isejecutado = vars.getStringParameter("inpisejecutado", "N");     data.comentariotec = vars.getStringParameter("inpcomentariotec");     data.mpreventivo = vars.getStringParameter("inpmpreventivo", "N");     data.mcorrectivo = vars.getStringParameter("inpmcorrectivo", "N");     data.instalacion = vars.getStringParameter("inpinstalacion", "N");     data.desinstalacion = vars.getStringParameter("inpdesinstalacion", "N");     data.inspeccion = vars.getStringParameter("inpinspeccion", "N");     data.fotografia = vars.getStringParameter("inpfotografia", "N");     data.encuesta = vars.getStringParameter("inpencuesta", "N");     data.demo = vars.getStringParameter("inpdemo", "N");     data.procesarini = vars.getStringParameter("inpprocesarini");     data.procesarfin = vars.getStringParameter("inpprocesarfin");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.neoCasolineId = vars.getRequestGlobalVariable("inpneoCasolineId", windowId + "|NEO_Casoline_ID");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");

      data.neoCasoId = vars.getGlobalVariable("inpneoCasoId", windowId + "|NEO_Caso_ID");


    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }


  private void refreshParentSession(VariablesSecureApp vars, String strPNEO_Caso_ID) throws IOException,ServletException {
      
      Caso9F4F4FB9795948F89E386CF4DD91931DData[] data = Caso9F4F4FB9795948F89E386CF4DD91931DData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Caso_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].adOrgId);    vars.setSessionValue(windowId + "|DocStatus", data[0].docstatus);    vars.setSessionValue(windowId + "|CBP_Cliente_ID", data[0].cbpClienteId);    vars.setSessionValue(windowId + "|Agenciacanal", data[0].agenciacanal);    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].adClientId);    vars.setSessionValue(windowId + "|NEO_Caso_ID", data[0].neoCasoId);
      vars.setSessionValue(windowId + "|NEO_Caso_ID", strPNEO_Caso_ID); //to ensure key parent is set for EM_* cols

      FieldProvider dataField = null; // Define this so that auxiliar inputs using SQL will work
      
  }
  
  
  private String getParentID(VariablesSecureApp vars, String strNEO_Casoline_ID) throws ServletException {
    String strPNEO_Caso_ID = LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectParentID(this, strNEO_Casoline_ID);
    if (strPNEO_Caso_ID.equals("")) {
      log4j.error("Parent record not found for id: " + strNEO_Casoline_ID);
      throw new ServletException("Parent record not found");
    }
    return strPNEO_Caso_ID;
  }

    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|Estadolinea", data[0].getField("estadolinea"));    vars.setSessionValue(windowId + "|NEO_Casoline_ID", data[0].getField("neoCasolineId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars, String strPNEO_Caso_ID) throws IOException,ServletException {
      LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] data = LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Caso_ID, vars.getStringParameter("inpneoCasolineId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strPNEO_Caso_ID, String strNEO_Casoline_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamCBP_Tecnico_ID = vars.getSessionValue(tabId + "|paramCBP_Tecnico_ID");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamCBP_Tecnico_ID)) || !(("").equals(strParamCBP_Tecnico_ID) || ("%").equals(strParamCBP_Tecnico_ID)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strNEO_Casoline_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strNEO_Casoline_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Soporte/LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099", false, "document.frmMain.inpneoCasolineId", "grid", "..", "".equals("Y"), "Soporte", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    xmlDocument.setParameter("keyParent", strPNEO_Caso_ID);
    xmlDocument.setParameter("parentFieldName", Utility.getFieldName("8A3141FC645A40978F73C316F7810A86", vars.getLanguage()));


    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "neoCasolineId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Relation.html", "Soporte", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }
    if (vars.getLanguage().equals("en_US")) xmlDocument.setParameter("parent", LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectParent(this, strPNEO_Caso_ID));
    else xmlDocument.setParameter("parent", LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectParentTrl(this, strPNEO_Caso_ID));

    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strNEO_Casoline_ID, String strPNEO_Caso_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamCBP_Tecnico_ID = vars.getSessionValue(tabId + "|paramCBP_Tecnico_ID");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamCBP_Tecnico_ID)) || !(("").equals(strParamCBP_Tecnico_ID) || ("%").equals(strParamCBP_Tecnico_ID)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Caso_ID, strNEO_Casoline_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strNEO_Casoline_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("neoCasolineId") == null || dataField.getField("neoCasolineId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars, strPNEO_Caso_ID);
        data = LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.set(strPNEO_Caso_ID, Utility.getDefault(this, vars, "Comentariotec", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Mcorrectivo", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Comentarios", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Desinstalacion", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectDef39EA8AB0705F48029552559B19BF58D6(this, strPNEO_Caso_ID), Utility.getDefault(this, vars, "Escalamiento", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "CBP_Tecnico_ID", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Demo", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Isejecutado", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Fecha_Lineacaso", "@#Date@", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "A5C247AE417447BA98E235046492F35D", "", dataField), LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectDef66908ECE63BC4995AE4DC4B9092C3D28_0(this, Utility.getDefault(this, vars, "Updatedby", "", "A5C247AE417447BA98E235046492F35D", "", dataField)), Utility.getDefault(this, vars, "Encuesta", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Mpreventivo", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Finicio", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Estadolinea", "BOR", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Instalacion", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Ffin", "", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Procesarini", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Fotografia", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), "Y", Utility.getDefault(this, vars, "Procesarfin", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "Tiempo_Ejecutado", "", "A5C247AE417447BA98E235046492F35D", "", dataField), "", Utility.getDefault(this, vars, "Inspeccion", "N", "A5C247AE417447BA98E235046492F35D", "N", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "A5C247AE417447BA98E235046492F35D", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "A5C247AE417447BA98E235046492F35D", "", dataField), LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.selectDefDE7D759E11224AECB3DCF159DC746AFC_1(this, Utility.getDefault(this, vars, "Createdby", "", "A5C247AE417447BA98E235046492F35D", "", dataField)));
        
      }
     }
      
    String currentPOrg=Caso9F4F4FB9795948F89E386CF4DD91931DData.selectOrg(this, strPNEO_Caso_ID);
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Soporte/LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Soporte/LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpneoCasolineId", "", "..", "".equals("Y"), "Soporte", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strNEO_Casoline_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strNEO_Casoline_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Relation.html", "Soporte", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    xmlDocument.setParameter("parentOrg", currentPOrg);
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
xmlDocument.setParameter("Fecha_Lineacaso_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "18", "CBP_Tecnico_ID", "138", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cbpTecnicoId"):dataField.getField("cbpTecnicoId")));
xmlDocument.setData("reportCBP_Tecnico_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Estadolinea", "5A673B369799440881D5E132BE93CE65", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("estadolinea"):dataField.getField("estadolinea")));
xmlDocument.setData("reportEstadolinea","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonTiempo_Ejecutado", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Finicio_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Finicio_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Ffin_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Ffin_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
comboTableData = new ComboTableData(vars, this, "17", "Escalamiento", "3F16C355DDBB4B7887E5A6456FC01C17", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("escalamiento"):dataField.getField("escalamiento")));
xmlDocument.setData("reportEscalamiento","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Procesarini_BTNname", Utility.getButtonName(this, vars, "E0B8B87376214413B9EDC10A3472F061", "Procesarini_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesarini = org.openbravo.erpCommon.utility.Utility.isModalProcess("3E8681D5D3D146FF8E1DE942F2E2CEFA"); 
xmlDocument.setParameter("Procesarini_Modal", modalProcesarini?"true":"false");
xmlDocument.setParameter("Procesarfin_BTNname", Utility.getButtonName(this, vars, "CE55F05AD2104DCE8BEA5A56DF5D82F3", "Procesarfin_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesarfin = org.openbravo.erpCommon.utility.Utility.isModalProcess("D51C1E722D934F87A6380893A21BF330"); 
xmlDocument.setParameter("Procesarfin_Modal", modalProcesarfin?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonProcesarini3E8681D5D3D146FF8E1DE942F2E2CEFA(HttpServletResponse response, VariablesSecureApp vars, String strNEO_Casoline_ID, String strprocesarini, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 3E8681D5D3D146FF8E1DE942F2E2CEFA");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Procesarini3E8681D5D3D146FF8E1DE942F2E2CEFA", discard).createXmlDocument();
      xmlDocument.setParameter("key", strNEO_Casoline_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "3E8681D5D3D146FF8E1DE942F2E2CEFA");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("3E8681D5D3D146FF8E1DE942F2E2CEFA");
        vars.removeMessage("3E8681D5D3D146FF8E1DE942F2E2CEFA");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonProcesarfinD51C1E722D934F87A6380893A21BF330(HttpServletResponse response, VariablesSecureApp vars, String strNEO_Casoline_ID, String strprocesarfin, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process D51C1E722D934F87A6380893A21BF330");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/ProcesarfinD51C1E722D934F87A6380893A21BF330", discard).createXmlDocument();
      xmlDocument.setParameter("key", strNEO_Casoline_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "D51C1E722D934F87A6380893A21BF330");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("D51C1E722D934F87A6380893A21BF330");
        vars.removeMessage("D51C1E722D934F87A6380893A21BF330");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strDocStatus=\"" + Utility.getContext(this, vars, "DocStatus", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type, String strPNEO_Caso_ID) throws IOException, ServletException {
    LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars, strPNEO_Caso_ID);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.neoCasolineId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.getCurrentDBTimestamp(this, data.neoCasolineId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.neoCasolineId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|NEO_Casoline_ID", data.neoCasolineId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
