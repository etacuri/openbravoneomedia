
package org.openbravo.erpWindows.com.atrums.neomedia.Cotizaciondeventas;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class Header14F6810D5E014E80BC1DC729700C3A8D extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "BE83AB829DE24D589F163C3952CEE31D";
  private static final String tabId = "14F6810D5E014E80BC1DC729700C3A8D";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 1;
  private static final String moduleId = "5FE0DB370CD142F38DF6A56A426D7CEB";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     
      if (command.contains("104")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("104");
        SessionInfo.setModuleId("0");
        if (securedProcess || explicitAccess.contains("104")) {
          classInfo.type = "P";
          classInfo.id = "104";
        }
      }
     
      if (command.contains("5498EEC3373243B1B373300DBA387D64")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("5498EEC3373243B1B373300DBA387D64");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("5498EEC3373243B1B373300DBA387D64")) {
          classInfo.type = "P";
          classInfo.id = "5498EEC3373243B1B373300DBA387D64";
        }
      }
     
      if (command.contains("C52D3F0B3E214B40A17C1B0BBC717FF0")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("C52D3F0B3E214B40A17C1B0BBC717FF0");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("C52D3F0B3E214B40A17C1B0BBC717FF0")) {
          classInfo.type = "P";
          classInfo.id = "C52D3F0B3E214B40A17C1B0BBC717FF0";
        }
      }
     

     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strcOrderId = request.getParameter("inpcOrderId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strcOrderId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strC_Order_ID.equals("")) strC_Order_ID = firstElement(vars, tableSQL);
          if (strC_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strC_Order_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strC_Order_ID.equals("")) strC_Order_ID = vars.getRequiredGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");
      else vars.setSessionValue(windowId + "|C_Order_ID", strC_Order_ID);
      
      vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view", "EDIT");

      printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view");
      String strC_Order_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strC_Order_ID = firstElement(vars, tableSQL);
          if (strC_Order_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strC_Order_ID.equals("")) strC_Order_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamAD_Org_ID", tabId + "|paramAD_Org_ID");
vars.getRequestGlobalVariable("inpParamDocumentNo", tabId + "|paramDocumentNo");
vars.getRequestGlobalVariable("inpParamPOReference", tabId + "|paramPOReference");
vars.getRequestGlobalVariable("inpParamDateOrdered", tabId + "|paramDateOrdered");
vars.getRequestGlobalVariable("inpParamC_DocTypeTarget_ID", tabId + "|paramC_DocTypeTarget_ID");
vars.getRequestGlobalVariable("inpParamC_BPartner_ID", tabId + "|paramC_BPartner_ID");
vars.getRequestGlobalVariable("inpParamInvoiceRule", tabId + "|paramInvoiceRule");
vars.getRequestGlobalVariable("inpParamGrandTotal", tabId + "|paramGrandTotal");
vars.getRequestGlobalVariable("inpParamDocStatus", tabId + "|paramDocStatus");
vars.getRequestGlobalVariable("inpParamDateOrdered_f", tabId + "|paramDateOrdered_f");
vars.getRequestGlobalVariable("inpParamGrandTotal_f", tabId + "|paramGrandTotal_f");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|C_Order_ID");
      String strC_Order_ID="";

      String strView = vars.getSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strC_Order_ID = firstElement(vars, tableSQL);
        if (strC_Order_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

      else printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view", "RELATION");
      printPageDataSheet(response, vars, strC_Order_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
      vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strC_Order_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      
      String strNext = nextElement(vars, strC_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      
      String strPrevious = previousElement(vars, strC_Order_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|C_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|C_Order_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strC_Order_ID = vars.getRequiredGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strC_Order_ID, tableSQL);
          vars.setSessionValue(windowId + "|C_Order_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strC_Order_ID = vars.getRequiredStringParameter("inpcOrderId");
      //Header14F6810D5E014E80BC1DC729700C3A8DData data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = Header14F6810D5E014E80BC1DC729700C3A8DData.delete(this, strC_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|cOrderId");
        vars.setSessionValue(tabId + "|Header14F6810D5E014E80BC1DC729700C3A8D.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONDocAction104")) {
        vars.setSessionValue("button104.strdocaction", vars.getStringParameter("inpdocaction"));
        vars.setSessionValue("button104.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button104.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button104.strClient", vars.getStringParameter("inpadClientId"));
        vars.setSessionValue("button104.inpdocstatus", vars.getRequiredStringParameter("inpdocstatus"));

        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button104.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "104", request.getServletPath());    
     } else if (vars.commandIn("BUTTON104")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String strdocaction = vars.getSessionValue("button104.strdocaction");
        String strProcessing = vars.getSessionValue("button104.strProcessing");
        String strOrg = vars.getSessionValue("button104.strOrg");
        String strClient = vars.getSessionValue("button104.strClient");
        
        String strdocstatus = vars.getSessionValue("button104.inpdocstatus");
String stradTableId = "259";

        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonDocAction104(response, vars, strC_Order_ID, strdocaction, strProcessing, strdocstatus, stradTableId);
        }

     } else if (vars.commandIn("BUTTONEM_Neo_Process_Cotiza5498EEC3373243B1B373300DBA387D64")) {
        vars.setSessionValue("button5498EEC3373243B1B373300DBA387D64.stremNeoProcessCotiza", vars.getStringParameter("inpemNeoProcessCotiza"));
        vars.setSessionValue("button5498EEC3373243B1B373300DBA387D64.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button5498EEC3373243B1B373300DBA387D64.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button5498EEC3373243B1B373300DBA387D64.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("Docstatus", vars.getStringParameter("inpdocstatus"));

        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button5498EEC3373243B1B373300DBA387D64.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "5498EEC3373243B1B373300DBA387D64", request.getServletPath());    
     } else if (vars.commandIn("BUTTON5498EEC3373243B1B373300DBA387D64")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String stremNeoProcessCotiza = vars.getSessionValue("button5498EEC3373243B1B373300DBA387D64.stremNeoProcessCotiza");
        String strProcessing = vars.getSessionValue("button5498EEC3373243B1B373300DBA387D64.strProcessing");
        String strOrg = vars.getSessionValue("button5498EEC3373243B1B373300DBA387D64.strOrg");
        String strClient = vars.getSessionValue("button5498EEC3373243B1B373300DBA387D64.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonEM_Neo_Process_Cotiza5498EEC3373243B1B373300DBA387D64(response, vars, strC_Order_ID, stremNeoProcessCotiza, strProcessing);
        }

     } else if (vars.commandIn("BUTTONEM_Neo_Duplica_OrdenC52D3F0B3E214B40A17C1B0BBC717FF0")) {
        vars.setSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.stremNeoDuplicaOrden", vars.getStringParameter("inpemNeoDuplicaOrden"));
        vars.setSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "C52D3F0B3E214B40A17C1B0BBC717FF0", request.getServletPath());    
     } else if (vars.commandIn("BUTTONC52D3F0B3E214B40A17C1B0BBC717FF0")) {
        String strC_Order_ID = vars.getGlobalVariable("inpcOrderId", windowId + "|C_Order_ID", "");
        String stremNeoDuplicaOrden = vars.getSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.stremNeoDuplicaOrden");
        String strProcessing = vars.getSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strProcessing");
        String strOrg = vars.getSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strOrg");
        String strClient = vars.getSessionValue("buttonC52D3F0B3E214B40A17C1B0BBC717FF0.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonEM_Neo_Duplica_OrdenC52D3F0B3E214B40A17C1B0BBC717FF0(response, vars, strC_Order_ID, stremNeoDuplicaOrden, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONDocAction104")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        String strdocaction = vars.getStringParameter("inpdocaction");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "104", (("C_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strC_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          Header14F6810D5E014E80BC1DC729700C3A8DData.updateDocAction(this, strdocaction, strC_Order_ID);

          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONEM_Neo_Process_Cotiza5498EEC3373243B1B373300DBA387D64")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        String stremNeoProcessCotiza = vars.getStringParameter("inpemNeoProcessCotiza");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "5498EEC3373243B1B373300DBA387D64", (("C_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strC_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          String strestado = vars.getStringParameter("inpestado");
PInstanceProcessData.insertPInstanceParam(this, pinstance, "10", "Estado", strestado, vars.getClient(), vars.getOrg(), vars.getUser());

          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONEM_Neo_Duplica_OrdenC52D3F0B3E214B40A17C1B0BBC717FF0")) {
        String strC_Order_ID = vars.getGlobalVariable("inpKey", windowId + "|C_Order_ID", "");
        String stremNeoDuplicaOrden = vars.getStringParameter("inpemNeoDuplicaOrden");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "C52D3F0B3E214B40A17C1B0BBC717FF0", (("C_Order_ID".equalsIgnoreCase("AD_Language"))?"0":strC_Order_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private Header14F6810D5E014E80BC1DC729700C3A8DData getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    Header14F6810D5E014E80BC1DC729700C3A8DData data = new Header14F6810D5E014E80BC1DC729700C3A8DData();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.cDoctypetargetId = vars.getRequiredGlobalVariable("inpcDoctypetargetId", windowId + "|C_DocTypeTarget_ID");     data.cDoctypetargetIdr = vars.getStringParameter("inpcDoctypetargetId_R");     data.cDoctypeId = vars.getRequiredGlobalVariable("inpcDoctypeId", windowId + "|C_DocType_ID");     data.documentno = vars.getRequiredStringParameter("inpdocumentno");     data.cReturnReasonId = vars.getStringParameter("inpcReturnReasonId");     data.dateordered = vars.getRequiredGlobalVariable("inpdateordered", windowId + "|DateOrdered");     data.cBpartnerId = vars.getRequiredGlobalVariable("inpcBpartnerId", windowId + "|C_BPartner_ID");     data.cBpartnerIdr = vars.getStringParameter("inpcBpartnerId_R");     data.cBpartnerLocationId = vars.getRequiredGlobalVariable("inpcBpartnerLocationId", windowId + "|C_BPartner_Location_ID");     data.cBpartnerLocationIdr = vars.getStringParameter("inpcBpartnerLocationId_R");     data.rmPickfromshipment = vars.getStringParameter("inprmPickfromshipment");     data.emNeoSegComercialId = vars.getRequiredStringParameter("inpemNeoSegComercialId");     data.mPricelistId = vars.getRequiredGlobalVariable("inpmPricelistId", windowId + "|M_PriceList_ID");     data.mPricelistIdr = vars.getStringParameter("inpmPricelistId_R");     data.validuntil = vars.getRequiredStringParameter("inpvaliduntil");     data.rmReceivematerials = vars.getStringParameter("inprmReceivematerials");     data.datepromised = vars.getRequiredGlobalVariable("inpdatepromised", windowId + "|DatePromised");     data.rmCreateinvoice = vars.getStringParameter("inprmCreateinvoice");     data.finPaymentmethodId = vars.getRequiredStringParameter("inpfinPaymentmethodId");     data.finPaymentmethodIdr = vars.getStringParameter("inpfinPaymentmethodId_R");     data.cPaymenttermId = vars.getRequiredStringParameter("inpcPaymenttermId");     data.cPaymenttermIdr = vars.getStringParameter("inpcPaymenttermId_R");     data.mWarehouseId = vars.getRequiredGlobalVariable("inpmWarehouseId", windowId + "|M_Warehouse_ID");     data.cRejectReasonId = vars.getStringParameter("inpcRejectReasonId");     data.cRejectReasonIdr = vars.getStringParameter("inpcRejectReasonId_R");     data.invoicerule = vars.getRequiredStringParameter("inpinvoicerule");     data.salesrepId = vars.getRequiredStringParameter("inpsalesrepId");     data.salesrepIdr = vars.getStringParameter("inpsalesrepId_R");     data.description = vars.getStringParameter("inpdescription");     data.emNeoDocstatusOrden = vars.getStringParameter("inpemNeoDocstatusOrden");     data.docstatus = vars.getRequiredGlobalVariable("inpdocstatus", windowId + "|DocStatus");    try {   data.grandtotal = vars.getRequiredNumericParameter("inpgrandtotal");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totallines = vars.getRequiredNumericParameter("inptotallines");  } catch (ServletException paramEx) { ex = paramEx; }     data.cCurrencyId = vars.getRequiredGlobalVariable("inpcCurrencyId", windowId + "|C_Currency_ID");     data.adUserId = vars.getStringParameter("inpadUserId");     data.poreference = vars.getStringParameter("inpporeference");     data.billtoId = vars.getRequiredStringParameter("inpbilltoId");     data.deliveryLocationId = vars.getStringParameter("inpdeliveryLocationId");     data.copyfrom = vars.getStringParameter("inpcopyfrom");     data.copyfrompo = vars.getStringParameter("inpcopyfrompo");     data.docaction = vars.getRequiredStringParameter("inpdocaction");     data.deliveryviarule = vars.getRequiredGlobalVariable("inpdeliveryviarule", windowId + "|DeliveryViaRule");     data.mShipperId = vars.getRequestGlobalVariable("inpmShipperId", windowId + "|M_Shipper_ID");     data.deliveryrule = vars.getRequiredStringParameter("inpdeliveryrule");     data.freightcostrule = vars.getRequiredGlobalVariable("inpfreightcostrule", windowId + "|FreightCostRule");    try {   data.freightamt = vars.getRequiredNumericParameter("inpfreightamt");  } catch (ServletException paramEx) { ex = paramEx; }     data.isdiscountprinted = vars.getStringParameter("inpisdiscountprinted", "N");     data.priorityrule = vars.getRequiredStringParameter("inppriorityrule");    try {   data.chargeamt = vars.getNumericParameter("inpchargeamt");  } catch (ServletException paramEx) { ex = paramEx; }     data.cChargeId = vars.getStringParameter("inpcChargeId");     data.convertquotation = vars.getStringParameter("inpconvertquotation");     data.emNeoCreaOrdenCot = vars.getStringParameter("inpemNeoCreaOrdenCot");     data.calculatePromotions = vars.getStringParameter("inpcalculatePromotions");     data.cActivityId = vars.getStringParameter("inpcActivityId");     data.cProjectId = vars.getStringParameter("inpcProjectId");     data.cProjectIdr = vars.getStringParameter("inpcProjectId_R");     data.cCampaignId = vars.getStringParameter("inpcCampaignId");     data.adOrgtrxId = vars.getStringParameter("inpadOrgtrxId");     data.user1Id = vars.getStringParameter("inpuser1Id");     data.user2Id = vars.getStringParameter("inpuser2Id");     data.emNeoCreaProdNuevo = vars.getStringParameter("inpemNeoCreaProdNuevo");     data.emNeoNuevosProd = vars.getRequiredInputGlobalVariable("inpemNeoNuevosProd", windowId + "|EM_Neo_Nuevos_Prod", "N");     data.emNeoProcessCotiza = vars.getStringParameter("inpemNeoProcessCotiza");     data.emNeoGeneraordentrabajo = vars.getStringParameter("inpemNeoGeneraordentrabajo");     data.emNeoEspublicidad = vars.getRequiredInputGlobalVariable("inpemNeoEspublicidad", windowId + "|EM_Neo_Espublicidad", "N");     data.emNeoDuplicaOrden = vars.getStringParameter("inpemNeoDuplicaOrden");     data.emNeoGenordenTrabajo = vars.getStringParameter("inpemNeoGenordenTrabajo");     data.generatetemplate = vars.getStringParameter("inpgeneratetemplate");     data.isselfservice = vars.getStringParameter("inpisselfservice", "N");     data.dateacct = vars.getRequiredStringParameter("inpdateacct");     data.dropshipBpartnerId = vars.getStringParameter("inpdropshipBpartnerId");     data.isinvoiced = vars.getStringParameter("inpisinvoiced", "N");     data.processed = vars.getRequiredInputGlobalVariable("inpprocessed", windowId + "|Processed", "N");     data.issotrx = vars.getRequiredInputGlobalVariable("inpissotrx", windowId + "|IsSOTrx", "N");     data.dropshipLocationId = vars.getStringParameter("inpdropshipLocationId");     data.istaxincluded = vars.getStringParameter("inpistaxincluded", "N");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.dropshipUserId = vars.getStringParameter("inpdropshipUserId");     data.cIncotermsId = vars.getStringParameter("inpcIncotermsId");     data.isdelivered = vars.getStringParameter("inpisdelivered", "N");     data.incotermsdescription = vars.getStringParameter("inpincotermsdescription");     data.isprinted = vars.getStringParameter("inpisprinted", "N");     data.processing = vars.getStringParameter("inpprocessing");     data.deliverynotes = vars.getStringParameter("inpdeliverynotes");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.paymentrule = vars.getRequiredStringParameter("inppaymentrule");     data.cOrderId = vars.getRequestGlobalVariable("inpcOrderId", windowId + "|C_Order_ID");     data.posted = vars.getRequiredGlobalVariable("inpposted", windowId + "|Posted");     data.dateprinted = vars.getStringParameter("inpdateprinted");     data.isselected = vars.getStringParameter("inpisselected", "N"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
         if (data.documentno.startsWith("<")) data.documentno = Utility.getDocumentNo(con, this, vars, windowId, "C_Order", data.cDoctypetargetId, data.cDoctypeId, false, true);

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|C_DocTypeTarget_ID", data[0].getField("cDoctypetargetId"));    vars.setSessionValue(windowId + "|C_DocType_ID", data[0].getField("cDoctypeId"));    vars.setSessionValue(windowId + "|DateOrdered", data[0].getField("dateordered"));    vars.setSessionValue(windowId + "|C_BPartner_ID", data[0].getField("cBpartnerId"));    vars.setSessionValue(windowId + "|C_BPartner_Location_ID", data[0].getField("cBpartnerLocationId"));    vars.setSessionValue(windowId + "|M_PriceList_ID", data[0].getField("mPricelistId"));    vars.setSessionValue(windowId + "|DatePromised", data[0].getField("datepromised"));    vars.setSessionValue(windowId + "|M_Warehouse_ID", data[0].getField("mWarehouseId"));    vars.setSessionValue(windowId + "|DocStatus", data[0].getField("docstatus"));    vars.setSessionValue(windowId + "|C_Currency_ID", data[0].getField("cCurrencyId"));    vars.setSessionValue(windowId + "|DeliveryViaRule", data[0].getField("deliveryviarule"));    vars.setSessionValue(windowId + "|M_Shipper_ID", data[0].getField("mShipperId"));    vars.setSessionValue(windowId + "|FreightCostRule", data[0].getField("freightcostrule"));    vars.setSessionValue(windowId + "|EM_Neo_Nuevos_Prod", data[0].getField("emNeoNuevosProd"));    vars.setSessionValue(windowId + "|EM_Neo_Espublicidad", data[0].getField("emNeoEspublicidad"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|Processed", data[0].getField("processed"));    vars.setSessionValue(windowId + "|IsSOTrx", data[0].getField("issotrx"));    vars.setSessionValue(windowId + "|C_Order_ID", data[0].getField("cOrderId"));    vars.setSessionValue(windowId + "|Posted", data[0].getField("posted"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      Header14F6810D5E014E80BC1DC729700C3A8DData[] data = Header14F6810D5E014E80BC1DC729700C3A8DData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpcOrderId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamAD_Org_ID = vars.getSessionValue(tabId + "|paramAD_Org_ID");
String strParamDocumentNo = vars.getSessionValue(tabId + "|paramDocumentNo");
String strParamPOReference = vars.getSessionValue(tabId + "|paramPOReference");
String strParamDateOrdered = vars.getSessionValue(tabId + "|paramDateOrdered");
String strParamC_DocTypeTarget_ID = vars.getSessionValue(tabId + "|paramC_DocTypeTarget_ID");
String strParamC_BPartner_ID = vars.getSessionValue(tabId + "|paramC_BPartner_ID");
String strParamInvoiceRule = vars.getSessionValue(tabId + "|paramInvoiceRule");
String strParamGrandTotal = vars.getSessionValue(tabId + "|paramGrandTotal");
String strParamDocStatus = vars.getSessionValue(tabId + "|paramDocStatus");
String strParamDateOrdered_f = vars.getSessionValue(tabId + "|paramDateOrdered_f");
String strParamGrandTotal_f = vars.getSessionValue(tabId + "|paramGrandTotal_f");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamAD_Org_ID) && ("").equals(strParamDocumentNo) && ("").equals(strParamPOReference) && ("").equals(strParamDateOrdered) && ("").equals(strParamC_DocTypeTarget_ID) && ("").equals(strParamC_BPartner_ID) && ("").equals(strParamInvoiceRule) && ("").equals(strParamGrandTotal) && ("").equals(strParamDocStatus) && ("").equals(strParamDateOrdered_f) && ("").equals(strParamGrandTotal_f)) || !(("").equals(strParamAD_Org_ID) || ("%").equals(strParamAD_Org_ID))  || !(("").equals(strParamDocumentNo) || ("%").equals(strParamDocumentNo))  || !(("").equals(strParamPOReference) || ("%").equals(strParamPOReference))  || !(("").equals(strParamDateOrdered) || ("%").equals(strParamDateOrdered))  || !(("").equals(strParamC_DocTypeTarget_ID) || ("%").equals(strParamC_DocTypeTarget_ID))  || !(("").equals(strParamC_BPartner_ID) || ("%").equals(strParamC_BPartner_ID))  || !(("").equals(strParamInvoiceRule) || ("%").equals(strParamInvoiceRule))  || !(("").equals(strParamGrandTotal) || ("%").equals(strParamGrandTotal))  || !(("").equals(strParamDocStatus) || ("%").equals(strParamDocStatus))  || !(("").equals(strParamDateOrdered_f) || ("%").equals(strParamDateOrdered_f))  || !(("").equals(strParamGrandTotal_f) || ("%").equals(strParamGrandTotal_f)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strC_Order_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strC_Order_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Cotizaciondeventas/Header14F6810D5E014E80BC1DC729700C3A8D_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D", false, "document.frmMain.inpcOrderId", "grid", "../orders/print.html", "N".equals("Y"), "Cotizaciondeventas", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "cOrderId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D_Relation.html", "Cotizaciondeventas", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strC_Order_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " C_Order.DocumentNo DESC";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    Header14F6810D5E014E80BC1DC729700C3A8DData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamAD_Org_ID = vars.getSessionValue(tabId + "|paramAD_Org_ID");
String strParamDocumentNo = vars.getSessionValue(tabId + "|paramDocumentNo");
String strParamPOReference = vars.getSessionValue(tabId + "|paramPOReference");
String strParamDateOrdered = vars.getSessionValue(tabId + "|paramDateOrdered");
String strParamC_DocTypeTarget_ID = vars.getSessionValue(tabId + "|paramC_DocTypeTarget_ID");
String strParamC_BPartner_ID = vars.getSessionValue(tabId + "|paramC_BPartner_ID");
String strParamInvoiceRule = vars.getSessionValue(tabId + "|paramInvoiceRule");
String strParamGrandTotal = vars.getSessionValue(tabId + "|paramGrandTotal");
String strParamDocStatus = vars.getSessionValue(tabId + "|paramDocStatus");
String strParamDateOrdered_f = vars.getSessionValue(tabId + "|paramDateOrdered_f");
String strParamGrandTotal_f = vars.getSessionValue(tabId + "|paramGrandTotal_f");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamAD_Org_ID) && ("").equals(strParamDocumentNo) && ("").equals(strParamPOReference) && ("").equals(strParamDateOrdered) && ("").equals(strParamC_DocTypeTarget_ID) && ("").equals(strParamC_BPartner_ID) && ("").equals(strParamInvoiceRule) && ("").equals(strParamGrandTotal) && ("").equals(strParamDocStatus) && ("").equals(strParamDateOrdered_f) && ("").equals(strParamGrandTotal_f)) || !(("").equals(strParamAD_Org_ID) || ("%").equals(strParamAD_Org_ID))  || !(("").equals(strParamDocumentNo) || ("%").equals(strParamDocumentNo))  || !(("").equals(strParamPOReference) || ("%").equals(strParamPOReference))  || !(("").equals(strParamDateOrdered) || ("%").equals(strParamDateOrdered))  || !(("").equals(strParamC_DocTypeTarget_ID) || ("%").equals(strParamC_DocTypeTarget_ID))  || !(("").equals(strParamC_BPartner_ID) || ("%").equals(strParamC_BPartner_ID))  || !(("").equals(strParamInvoiceRule) || ("%").equals(strParamInvoiceRule))  || !(("").equals(strParamGrandTotal) || ("%").equals(strParamGrandTotal))  || !(("").equals(strParamDocStatus) || ("%").equals(strParamDocStatus))  || !(("").equals(strParamDateOrdered_f) || ("%").equals(strParamDateOrdered_f))  || !(("").equals(strParamGrandTotal_f) || ("%").equals(strParamGrandTotal_f)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = Header14F6810D5E014E80BC1DC729700C3A8DData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strC_Order_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strC_Order_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new Header14F6810D5E014E80BC1DC729700C3A8DData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("cOrderId") == null || dataField.getField("cOrderId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = Header14F6810D5E014E80BC1DC729700C3A8DData.set(Utility.getDefault(this, vars, "Convertquotation", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "validuntil", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), "", Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@#AD_Org_ID@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), "Y", Utility.getDefault(this, vars, "CreatedBy", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Header14F6810D5E014E80BC1DC729700C3A8DData.selectDef2166_0(this, Utility.getDefault(this, vars, "CreatedBy", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField)), Utility.getDefault(this, vars, "UpdatedBy", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Header14F6810D5E014E80BC1DC729700C3A8DData.selectDef2168_1(this, Utility.getDefault(this, vars, "UpdatedBy", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField)), Utility.getDefault(this, vars, "DocumentNo", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DocStatus", "DR", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DocAction", "CO", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "FF80818130217A35013021A672400035", Utility.getDefault(this, vars, "DocAction", "CO", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "FF80818130217A35013021A672400035", Utility.getDefault(this, vars, "DocAction", "CO", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField))), Utility.getDefault(this, vars, "C_DocType_ID", "0", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_DocTypeTarget_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "Description", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "IsDelivered", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "IsInvoiced", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "IsPrinted", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "DateOrdered", "@#Date@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DatePromised", "@#Date@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DateAcct", "@#Date@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "SalesRep_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_PaymentTerm_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "BillTo_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_Currency_ID", "@C_Currency_ID@", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "InvoiceRule", "D", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "FreightAmt", "", "BE83AB829DE24D589F163C3952CEE31D", "0", dataField), Utility.getDefault(this, vars, "DeliveryViaRule", "P", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "M_Shipper_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "PriorityRule", "5", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "TotalLines", "", "BE83AB829DE24D589F163C3952CEE31D", "0", dataField), Utility.getDefault(this, vars, "GrandTotal", "", "BE83AB829DE24D589F163C3952CEE31D", "0", dataField), Utility.getDefault(this, vars, "M_Warehouse_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "M_PriceList_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "Processing", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "C_Campaign_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "EM_Neo_Espublicidad", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "C_BPartner_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Header14F6810D5E014E80BC1DC729700C3A8DData.selectDef2762_2(this, Utility.getDefault(this, vars, "C_BPartner_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField)), Utility.getDefault(this, vars, "AD_User_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "POReference", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_Charge_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "ChargeAmt", "0", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "EM_Neo_Genorden_Trabajo", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "Processed", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "C_BPartner_Location_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_Project_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Header14F6810D5E014E80BC1DC729700C3A8DData.selectDef3402_3(this, Utility.getDefault(this, vars, "C_Project_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField)), Utility.getDefault(this, vars, "C_Activity_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "IsSOTrx", "@IsSOTrx@", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "DatePrinted", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DeliveryRule", "A", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "FreightCostRule", "I", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "PaymentRule", "B", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "IsDiscountPrinted", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "Posted", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "IsTaxIncluded", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "IsSelected", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "EM_Neo_Seg_Comercial_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "EM_Neo_Duplica_Orden", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "Deliverynotes", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_Incoterms_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "Incotermsdescription", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "Generatetemplate", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "Delivery_Location_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "CopyFromPO", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "FIN_Paymentmethod_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DropShip_User_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "DropShip_BPartner_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "CopyFrom", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "DropShip_Location_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "IsSelfService", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "EM_Neo_Crea_Orden_Cot", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "AD_OrgTrx_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "User2_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "User1_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "Calculate_Promotions", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "EM_Neo_Crea_Prod_Nuevo", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "RM_PickFromShipment", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "RM_ReceiveMaterials", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "RM_CreateInvoice", "", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "C_Return_Reason_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "EM_Neo_Nuevos_Prod", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "EM_Neo_Docstatus_Orden", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "C_Reject_Reason_ID", "", "BE83AB829DE24D589F163C3952CEE31D", "", dataField), Utility.getDefault(this, vars, "EM_Neo_Generaordentrabajo", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField), Utility.getDefault(this, vars, "EM_Neo_Process_Cotiza", "N", "BE83AB829DE24D589F163C3952CEE31D", "N", dataField));
             data[0].documentno = "<" + Utility.getDocumentNo( this, vars, windowId, "C_Order", data[0].cDoctypetargetId, data[0].cDoctypeId, false, false) + ">";
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Cotizaciondeventas/Header14F6810D5E014E80BC1DC729700C3A8D_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Cotizaciondeventas/Header14F6810D5E014E80BC1DC729700C3A8D_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpcOrderId", "", "../orders/print.html", "N".equals("Y"), "Cotizaciondeventas", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strC_Order_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strC_Order_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D_Relation.html", "Cotizaciondeventas", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Header14F6810D5E014E80BC1DC729700C3A8D_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "49DC1D6F086945AB82F84C66F5F13F16", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "C_DocTypeTarget_ID", "22F546D49D3A48E1B2B4F50446A8DE58", "133", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cDoctypetargetId"):dataField.getField("cDoctypetargetId")));
xmlDocument.setData("reportC_DocTypeTarget_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("DateOrdered_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "19", "C_BPartner_Location_ID", "", "167", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cBpartnerLocationId"):dataField.getField("cBpartnerLocationId")));
xmlDocument.setData("reportC_BPartner_Location_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "M_PriceList_ID", "", "DB85A30739C3487988921CE3FFFD3BAD", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mPricelistId"):dataField.getField("mPricelistId")));
xmlDocument.setData("reportM_PriceList_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("DatePromised_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("validuntil_Format", vars.getSessionValue("#AD_SqlDateFormat"));
comboTableData = new ComboTableData(vars, this, "19", "FIN_Paymentmethod_ID", "", "FF80808130B107670130B1115F22000D", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("finPaymentmethodId"):dataField.getField("finPaymentmethodId")));
xmlDocument.setData("reportFIN_Paymentmethod_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_PaymentTerm_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cPaymenttermId"):dataField.getField("cPaymenttermId")));
xmlDocument.setData("reportC_PaymentTerm_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_Reject_Reason_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cRejectReasonId"):dataField.getField("cRejectReasonId")));
xmlDocument.setData("reportC_Reject_Reason_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "SalesRep_ID", "190", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("salesrepId"):dataField.getField("salesrepId")));
xmlDocument.setData("reportSalesRep_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonGrandTotal", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotalLines", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("DocAction_BTNname", Utility.getButtonName(this, vars, "FF80818130217A35013021A672400035", (dataField==null?data[0].getField("docaction"):dataField.getField("docaction")), "DocAction_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalDocAction = org.openbravo.erpCommon.utility.Utility.isModalProcess("104"); 
xmlDocument.setParameter("DocAction_Modal", modalDocAction?"true":"false");
xmlDocument.setParameter("buttonFreightAmt", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonChargeAmt", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("EM_Neo_Process_Cotiza_BTNname", Utility.getButtonName(this, vars, "F9D9C7F811E84C22B0728F31D08FABAF", "EM_Neo_Process_Cotiza_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Neo_Process_Cotiza = org.openbravo.erpCommon.utility.Utility.isModalProcess("5498EEC3373243B1B373300DBA387D64"); 
xmlDocument.setParameter("EM_Neo_Process_Cotiza_Modal", modalEM_Neo_Process_Cotiza?"true":"false");
xmlDocument.setParameter("EM_Neo_Duplica_Orden_BTNname", Utility.getButtonName(this, vars, "7C77D0F611A8418D9019BC871E8B7114", "EM_Neo_Duplica_Orden_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Neo_Duplica_Orden = org.openbravo.erpCommon.utility.Utility.isModalProcess("C52D3F0B3E214B40A17C1B0BBC717FF0"); 
xmlDocument.setParameter("EM_Neo_Duplica_Orden_Modal", modalEM_Neo_Duplica_Orden?"true":"false");
xmlDocument.setParameter("EM_Neo_Genorden_Trabajo_BTNname", Utility.getButtonName(this, vars, "82736129DA7C4C87B8B06408DE17F8D9", "EM_Neo_Genorden_Trabajo_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEM_Neo_Genorden_Trabajo = org.openbravo.erpCommon.utility.Utility.isModalProcess("ECB6CAB27B46454F8B4CCEDF16DCB137"); 
xmlDocument.setParameter("EM_Neo_Genorden_Trabajo_Modal", modalEM_Neo_Genorden_Trabajo?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("DateAcct_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("DatePrinted_Format", vars.getSessionValue("#AD_SqlDateFormat"));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonDocAction104(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String strdocaction, String strProcessing, String strdocstatus, String stradTableId)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 104");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/DocAction", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Header14F6810D5E014E80BC1DC729700C3A8D_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "104");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("104");
        vars.removeMessage("104");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

      xmlDocument.setParameter("docstatus", strdocstatus);
xmlDocument.setParameter("adTableId", stradTableId);
    try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
xmlDocument.setParameter("processId", "104");
xmlDocument.setParameter("processDescription", "Process Order");
xmlDocument.setParameter("docaction", (strdocaction.equals("--")?"CL":strdocaction));
FieldProvider[] dataDocAction = ActionButtonUtility.docAction(this, vars, strdocaction, "FF80818130217A35013021A672400035", strdocstatus, strProcessing, stradTableId, tabId);
xmlDocument.setData("reportdocaction", "liststructure", dataDocAction);
StringBuffer dact = new StringBuffer();
if (dataDocAction!=null) {
  dact.append("var arrDocAction = new Array(\n");
  for (int i=0;i<dataDocAction.length;i++) {
    dact.append("new Array(\"" + dataDocAction[i].getField("id") + "\", \"" + dataDocAction[i].getField("name") + "\", \"" + dataDocAction[i].getField("description") + "\")\n");
    if (i<dataDocAction.length-1) dact.append(",\n");
  }
  dact.append(");");
} else dact.append("var arrDocAction = null");
xmlDocument.setParameter("array", dact.toString());

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonEM_Neo_Process_Cotiza5498EEC3373243B1B373300DBA387D64(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String stremNeoProcessCotiza, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 5498EEC3373243B1B373300DBA387D64");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/EM_Neo_Process_Cotiza5498EEC3373243B1B373300DBA387D64", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Header14F6810D5E014E80BC1DC729700C3A8D_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "5498EEC3373243B1B373300DBA387D64");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("5498EEC3373243B1B373300DBA387D64");
        vars.removeMessage("5498EEC3373243B1B373300DBA387D64");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    ComboTableData comboTableData = null;
    xmlDocument.setParameter("Estado", "");
    comboTableData = new ComboTableData(vars, this, "17", "Estado", "CFED1F26C5F74733BA9FEBF7827BF7BB", "FC70FBA784354C87AE876B08E50511BA", Utility.getContext(this, vars, "#AccessibleOrgTree", ""), Utility.getContext(this, vars, "#User_Client", ""), 0);
    Utility.fillSQLParameters(this, vars, (FieldProvider) vars.getSessionObject("button5498EEC3373243B1B373300DBA387D64.originalParams"), comboTableData, windowId, "");
    xmlDocument.setData("reportEstado", "liststructure", comboTableData.select(false));
comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonEM_Neo_Duplica_OrdenC52D3F0B3E214B40A17C1B0BBC717FF0(HttpServletResponse response, VariablesSecureApp vars, String strC_Order_ID, String stremNeoDuplicaOrden, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process C52D3F0B3E214B40A17C1B0BBC717FF0");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/EM_Neo_Duplica_OrdenC52D3F0B3E214B40A17C1B0BBC717FF0", discard).createXmlDocument();
      xmlDocument.setParameter("key", strC_Order_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Header14F6810D5E014E80BC1DC729700C3A8D_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "C52D3F0B3E214B40A17C1B0BBC717FF0");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("C52D3F0B3E214B40A17C1B0BBC717FF0");
        vars.removeMessage("C52D3F0B3E214B40A17C1B0BBC717FF0");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strACCT_DIMENSION_DISPLAY=\"" +Utility.getContext(this, vars, "ACCT_DIMENSION_DISPLAY", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "var strEM_Atecco_Docstatus=\"" + Utility.getContext(this, vars, "EM_Atecco_Docstatus", windowId) + "\";\n";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    Header14F6810D5E014E80BC1DC729700C3A8DData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.cOrderId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (Header14F6810D5E014E80BC1DC729700C3A8DData.getCurrentDBTimestamp(this, data.cOrderId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.cOrderId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.docactionBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "FF80818130217A35013021A672400035", data.getField("DocAction"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|C_Order_ID", data.cOrderId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet Header14F6810D5E014E80BC1DC729700C3A8D. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
