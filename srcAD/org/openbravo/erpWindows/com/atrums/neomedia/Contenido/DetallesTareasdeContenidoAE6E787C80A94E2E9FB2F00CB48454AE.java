
package org.openbravo.erpWindows.com.atrums.neomedia.Contenido;


import org.openbravo.erpCommon.reference.*;



import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "938E38F4496C4E2998F21E95FAB40859";
  private static final String tabId = "AE6E787C80A94E2E9FB2F00CB48454AE";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "5FE0DB370CD142F38DF6A56A426D7CEB";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     
      if (command.contains("A3400E7263034DBCB5180A65E0679C48")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("A3400E7263034DBCB5180A65E0679C48");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("A3400E7263034DBCB5180A65E0679C48")) {
          classInfo.type = "P";
          classInfo.id = "A3400E7263034DBCB5180A65E0679C48";
        }
      }
     
      if (command.contains("28C2ACA37DA74A34AE3C9B6C37CFB528")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("28C2ACA37DA74A34AE3C9B6C37CFB528");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("28C2ACA37DA74A34AE3C9B6C37CFB528")) {
          classInfo.type = "P";
          classInfo.id = "28C2ACA37DA74A34AE3C9B6C37CFB528";
        }
      }
     
      if (command.contains("0EBE7D053E6645718CB125EC65527BA8")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("0EBE7D053E6645718CB125EC65527BA8");
        SessionInfo.setModuleId("5FE0DB370CD142F38DF6A56A426D7CEB");
        if (securedProcess || explicitAccess.contains("0EBE7D053E6645718CB125EC65527BA8")) {
          classInfo.type = "P";
          classInfo.id = "0EBE7D053E6645718CB125EC65527BA8";
        }
      }
     

     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strneoMonitoreolineaId = request.getParameter("inpneoMonitoreolineaId");
         String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strneoMonitoreolineaId.equals(""))
              total = saveRecord(vars, myError, 'U', strPNEO_Monitoreo_ID);
          else
              total = saveRecord(vars, myError, 'I', strPNEO_Monitoreo_ID);
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID", "");

      String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
            if (strPNEO_Monitoreo_ID.equals("")) {
        strPNEO_Monitoreo_ID = getParentID(vars, strNEO_Monitoreolinea_ID);
        if (strPNEO_Monitoreo_ID.equals("")) throw new ServletException("Required parameter :" + windowId + "|NEO_Monitoreo_ID");
        vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", strPNEO_Monitoreo_ID);

        refreshParentSession(vars, strPNEO_Monitoreo_ID);
      }


      String strView = vars.getSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strNEO_Monitoreolinea_ID.equals("")) strNEO_Monitoreolinea_ID = firstElement(vars, tableSQL);
          if (strNEO_Monitoreolinea_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, tableSQL);

      else printPageDataSheet(response, vars, strPNEO_Monitoreo_ID, strNEO_Monitoreolinea_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strNEO_Monitoreolinea_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strNEO_Monitoreolinea_ID.equals("")) strNEO_Monitoreolinea_ID = vars.getRequiredGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID");
      else vars.setSessionValue(windowId + "|NEO_Monitoreolinea_ID", strNEO_Monitoreolinea_ID);
      
      
      String strPNEO_Monitoreo_ID = getParentID(vars, strNEO_Monitoreolinea_ID);
      
      vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", strPNEO_Monitoreo_ID);
      vars.setSessionValue("77866D1358084CE19BC044FED00A074C|Contenido.view", "EDIT");

      refreshParentSession(vars, strPNEO_Monitoreo_ID);

      vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view", "EDIT");

      printPageEdit(response, request, vars, false, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID", false, false, true, "");
      vars.removeSessionValue(windowId + "|NEO_Monitoreolinea_ID");
      refreshParentSession(vars, strPNEO_Monitoreo_ID);


      String strView = vars.getSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view");
      String strNEO_Monitoreolinea_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strNEO_Monitoreolinea_ID = firstElement(vars, tableSQL);
          if (strNEO_Monitoreolinea_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strNEO_Monitoreolinea_ID.equals("")) strNEO_Monitoreolinea_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, tableSQL);

      } else printPageDataSheet(response, vars, strPNEO_Monitoreo_ID, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamLine", tabId + "|paramLine");
vars.getRequestGlobalVariable("inpParamLine_f", tabId + "|paramLine_f");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
            String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      
      vars.removeSessionValue(windowId + "|NEO_Monitoreolinea_ID");
      String strNEO_Monitoreolinea_ID="";

      String strView = vars.getSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strNEO_Monitoreolinea_ID = firstElement(vars, tableSQL);
        if (strNEO_Monitoreolinea_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, tableSQL);

      else printPageDataSheet(response, vars, strPNEO_Monitoreo_ID, strNEO_Monitoreolinea_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
            String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      

      String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
      vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view", "RELATION");
      printPageDataSheet(response, vars, strPNEO_Monitoreo_ID, strNEO_Monitoreolinea_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");


      printPageEdit(response, request, vars, true, "", strPNEO_Monitoreo_ID, tableSQL);

    } else if (vars.commandIn("EDIT")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
      vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      String strNEO_Monitoreolinea_ID = vars.getRequiredStringParameter("inpneoMonitoreolineaId");
      
      String strNext = nextElement(vars, strNEO_Monitoreolinea_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, strPNEO_Monitoreo_ID, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      String strNEO_Monitoreolinea_ID = vars.getRequiredStringParameter("inpneoMonitoreolineaId");
      
      String strPrevious = previousElement(vars, strNEO_Monitoreolinea_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, strPNEO_Monitoreo_ID, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {
vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|NEO_Monitoreolinea_ID");
      vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", strPNEO_Monitoreo_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      String strInitRecord = vars.getSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|NEO_Monitoreolinea_ID");
      vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", strPNEO_Monitoreo_ID);
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, strPNEO_Monitoreo_ID, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strPNEO_Monitoreo_ID, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, strPNEO_Monitoreo_ID, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I', strPNEO_Monitoreo_ID);      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      String strNEO_Monitoreolinea_ID = vars.getRequiredGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U', strPNEO_Monitoreo_ID);      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strNEO_Monitoreolinea_ID, tableSQL);
          vars.setSessionValue(windowId + "|NEO_Monitoreolinea_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");

      String strNEO_Monitoreolinea_ID = vars.getRequiredStringParameter("inpneoMonitoreolineaId");
      //DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData data = getEditVariables(vars, strPNEO_Monitoreo_ID);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.delete(this, strNEO_Monitoreolinea_ID, strPNEO_Monitoreo_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|neoMonitoreolineaId");
        vars.setSessionValue(tabId + "|DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONProcesariniA3400E7263034DBCB5180A65E0679C48")) {
        vars.setSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strprocesarini", vars.getStringParameter("inpprocesarini"));
        vars.setSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonA3400E7263034DBCB5180A65E0679C48.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "A3400E7263034DBCB5180A65E0679C48", request.getServletPath());    
     } else if (vars.commandIn("BUTTONA3400E7263034DBCB5180A65E0679C48")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
        String strprocesarini = vars.getSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strprocesarini");
        String strProcessing = vars.getSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strProcessing");
        String strOrg = vars.getSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strOrg");
        String strClient = vars.getSessionValue("buttonA3400E7263034DBCB5180A65E0679C48.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesariniA3400E7263034DBCB5180A65E0679C48(response, vars, strNEO_Monitoreolinea_ID, strprocesarini, strProcessing);
        }

     } else if (vars.commandIn("BUTTONProcesarfin28C2ACA37DA74A34AE3C9B6C37CFB528")) {
        vars.setSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strprocesarfin", vars.getStringParameter("inpprocesarfin"));
        vars.setSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button28C2ACA37DA74A34AE3C9B6C37CFB528.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "28C2ACA37DA74A34AE3C9B6C37CFB528", request.getServletPath());    
     } else if (vars.commandIn("BUTTON28C2ACA37DA74A34AE3C9B6C37CFB528")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
        String strprocesarfin = vars.getSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strprocesarfin");
        String strProcessing = vars.getSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strProcessing");
        String strOrg = vars.getSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strOrg");
        String strClient = vars.getSessionValue("button28C2ACA37DA74A34AE3C9B6C37CFB528.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesarfin28C2ACA37DA74A34AE3C9B6C37CFB528(response, vars, strNEO_Monitoreolinea_ID, strprocesarfin, strProcessing);
        }

     } else if (vars.commandIn("BUTTONEliminarlinea0EBE7D053E6645718CB125EC65527BA8")) {
        vars.setSessionValue("button0EBE7D053E6645718CB125EC65527BA8.streliminarlinea", vars.getStringParameter("inpeliminarlinea"));
        vars.setSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button0EBE7D053E6645718CB125EC65527BA8.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "0EBE7D053E6645718CB125EC65527BA8", request.getServletPath());    
     } else if (vars.commandIn("BUTTON0EBE7D053E6645718CB125EC65527BA8")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID", "");
        String streliminarlinea = vars.getSessionValue("button0EBE7D053E6645718CB125EC65527BA8.streliminarlinea");
        String strProcessing = vars.getSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strProcessing");
        String strOrg = vars.getSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strOrg");
        String strClient = vars.getSessionValue("button0EBE7D053E6645718CB125EC65527BA8.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonEliminarlinea0EBE7D053E6645718CB125EC65527BA8(response, vars, strNEO_Monitoreolinea_ID, streliminarlinea, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONProcesariniA3400E7263034DBCB5180A65E0679C48")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpKey", windowId + "|NEO_Monitoreolinea_ID", "");
        String strprocesarini = vars.getStringParameter("inpprocesarini");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "A3400E7263034DBCB5180A65E0679C48", (("NEO_Monitoreolinea_ID".equalsIgnoreCase("AD_Language"))?"0":strNEO_Monitoreolinea_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONProcesarfin28C2ACA37DA74A34AE3C9B6C37CFB528")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpKey", windowId + "|NEO_Monitoreolinea_ID", "");
        String strprocesarfin = vars.getStringParameter("inpprocesarfin");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "28C2ACA37DA74A34AE3C9B6C37CFB528", (("NEO_Monitoreolinea_ID".equalsIgnoreCase("AD_Language"))?"0":strNEO_Monitoreolinea_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONEliminarlinea0EBE7D053E6645718CB125EC65527BA8")) {
        String strNEO_Monitoreolinea_ID = vars.getGlobalVariable("inpKey", windowId + "|NEO_Monitoreolinea_ID", "");
        String streliminarlinea = vars.getStringParameter("inpeliminarlinea");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "0EBE7D053E6645718CB125EC65527BA8", (("NEO_Monitoreolinea_ID".equalsIgnoreCase("AD_Language"))?"0":strNEO_Monitoreolinea_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      String strPNEO_Monitoreo_ID = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType, strPNEO_Monitoreo_ID);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData getEditVariables(Connection con, VariablesSecureApp vars, String strPNEO_Monitoreo_ID) throws IOException,ServletException {
    DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData data = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData();
    ServletException ex = null;
    try {
   try {   data.line = vars.getRequiredNumericParameter("inpline");  } catch (ServletException paramEx) { ex = paramEx; }     data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.fecharegistro = vars.getStringParameter("inpfecharegistro");     data.tipolinea = vars.getRequiredStringParameter("inptipolinea");     data.tipolinear = vars.getStringParameter("inptipolinea_R");     data.neoMonitoreoId = vars.getRequiredStringParameter("inpneoMonitoreoId");     data.descripcionlinea = vars.getStringParameter("inpdescripcionlinea");     data.finicio = vars.getStringParameter("inpfinicio");     data.ffin = vars.getStringParameter("inpffin");    try {   data.tiempo = vars.getNumericParameter("inptiempo");  } catch (ServletException paramEx) { ex = paramEx; }     data.estadolinea = vars.getStringParameter("inpestadolinea");     data.estadolinear = vars.getStringParameter("inpestadolinea_R");     data.procesarini = vars.getStringParameter("inpprocesarini");     data.procesarfin = vars.getStringParameter("inpprocesarfin");     data.eliminarlinea = vars.getStringParameter("inpeliminarlinea");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.neoMonitoreolineaId = vars.getRequestGlobalVariable("inpneoMonitoreolineaId", windowId + "|NEO_Monitoreolinea_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");

      data.neoMonitoreoId = vars.getGlobalVariable("inpneoMonitoreoId", windowId + "|NEO_Monitoreo_ID");


    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }


  private void refreshParentSession(VariablesSecureApp vars, String strPNEO_Monitoreo_ID) throws IOException,ServletException {
      
      Contenido77866D1358084CE19BC044FED00A074CData[] data = Contenido77866D1358084CE19BC044FED00A074CData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Monitoreo_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].adOrgId);    vars.setSessionValue(windowId + "|DocStatus", data[0].docstatus);    vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", data[0].neoMonitoreoId);    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].adClientId);
      vars.setSessionValue(windowId + "|NEO_Monitoreo_ID", strPNEO_Monitoreo_ID); //to ensure key parent is set for EM_* cols

      FieldProvider dataField = null; // Define this so that auxiliar inputs using SQL will work
      
  }
  
  
  private String getParentID(VariablesSecureApp vars, String strNEO_Monitoreolinea_ID) throws ServletException {
    String strPNEO_Monitoreo_ID = DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectParentID(this, strNEO_Monitoreolinea_ID);
    if (strPNEO_Monitoreo_ID.equals("")) {
      log4j.error("Parent record not found for id: " + strNEO_Monitoreolinea_ID);
      throw new ServletException("Parent record not found");
    }
    return strPNEO_Monitoreo_ID;
  }

    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|NEO_Monitoreolinea_ID", data[0].getField("neoMonitoreolineaId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars, String strPNEO_Monitoreo_ID) throws IOException,ServletException {
      DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] data = DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Monitoreo_ID, vars.getStringParameter("inpneoMonitoreolineaId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strPNEO_Monitoreo_ID, String strNEO_Monitoreolinea_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamLine = vars.getSessionValue(tabId + "|paramLine");
String strParamLine_f = vars.getSessionValue(tabId + "|paramLine_f");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLine) && ("").equals(strParamLine_f)) || !(("").equals(strParamLine) || ("%").equals(strParamLine))  || !(("").equals(strParamLine_f) || ("%").equals(strParamLine_f)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strNEO_Monitoreolinea_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strNEO_Monitoreolinea_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Contenido/DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE", false, "document.frmMain.inpneoMonitoreolineaId", "grid", "..", "".equals("Y"), "Contenido", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    xmlDocument.setParameter("keyParent", strPNEO_Monitoreo_ID);
    xmlDocument.setParameter("parentFieldName", Utility.getFieldName("0E445E069FBC4769A86AE66AA67B8872", vars.getLanguage()));


    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "neoMonitoreolineaId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Relation.html", "Contenido", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }
    if (vars.getLanguage().equals("en_US")) xmlDocument.setParameter("parent", DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectParent(this, strPNEO_Monitoreo_ID));
    else xmlDocument.setParameter("parent", DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectParentTrl(this, strPNEO_Monitoreo_ID));

    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strNEO_Monitoreolinea_ID, String strPNEO_Monitoreo_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamLine = vars.getSessionValue(tabId + "|paramLine");
String strParamLine_f = vars.getSessionValue(tabId + "|paramLine_f");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamLine) && ("").equals(strParamLine_f)) || !(("").equals(strParamLine) || ("%").equals(strParamLine))  || !(("").equals(strParamLine_f) || ("%").equals(strParamLine_f)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strPNEO_Monitoreo_ID, strNEO_Monitoreolinea_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strNEO_Monitoreolinea_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("neoMonitoreolineaId") == null || dataField.getField("neoMonitoreolineaId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars, strPNEO_Monitoreo_ID);
        data = DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.set(strPNEO_Monitoreo_ID, Utility.getDefault(this, vars, "Procesarini", "N", "938E38F4496C4E2998F21E95FAB40859", "N", dataField), "", Utility.getDefault(this, vars, "Tiempo", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@AD_ORG_ID@", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Estadolinea", "BOR", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Descripcionlinea", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Tipolinea", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Createdby", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectDef965FA361A9F644109731C624214AAEA8_0(this, Utility.getDefault(this, vars, "Createdby", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField)), DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectDef9EC68EDFEE814881A47AC169C7FB7375(this, strPNEO_Monitoreo_ID), "Y", Utility.getDefault(this, vars, "Eliminarlinea", "N", "938E38F4496C4E2998F21E95FAB40859", "N", dataField), Utility.getDefault(this, vars, "Updatedby", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.selectDefA657727788954A059FF37DEF7EBD5BAF_1(this, Utility.getDefault(this, vars, "Updatedby", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField)), Utility.getDefault(this, vars, "Finicio", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Procesarfin", "N", "938E38F4496C4E2998F21E95FAB40859", "N", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Ffin", "", "938E38F4496C4E2998F21E95FAB40859", "", dataField), Utility.getDefault(this, vars, "Fecharegistro", "@#DATE@", "938E38F4496C4E2998F21E95FAB40859", "", dataField));
        
      }
     }
      
    String currentPOrg=Contenido77866D1358084CE19BC044FED00A074CData.selectOrg(this, strPNEO_Monitoreo_ID);
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Contenido/DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/neomedia/Contenido/DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpneoMonitoreolineaId", "", "..", "".equals("Y"), "Contenido", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strNEO_Monitoreolinea_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strNEO_Monitoreolinea_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Relation.html", "Contenido", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    xmlDocument.setParameter("parentOrg", currentPOrg);
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
xmlDocument.setParameter("Fecharegistro_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Fecharegistro_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
comboTableData = new ComboTableData(vars, this, "17", "Tipolinea", "7570B11FD59F451BBAC2A5C7A186FFC5", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("tipolinea"):dataField.getField("tipolinea")));
xmlDocument.setData("reportTipolinea","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Finicio_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Finicio_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Ffin_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Ffin_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("buttonTiempo", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "17", "Estadolinea", "5A673B369799440881D5E132BE93CE65", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("estadolinea"):dataField.getField("estadolinea")));
xmlDocument.setData("reportEstadolinea","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Procesarini_BTNname", Utility.getButtonName(this, vars, "D690B6AB6D3C4868A7221EDCBE038730", "Procesarini_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesarini = org.openbravo.erpCommon.utility.Utility.isModalProcess("A3400E7263034DBCB5180A65E0679C48"); 
xmlDocument.setParameter("Procesarini_Modal", modalProcesarini?"true":"false");
xmlDocument.setParameter("Procesarfin_BTNname", Utility.getButtonName(this, vars, "A80DE62B04F24DBABBCA4B89EDA28499", "Procesarfin_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesarfin = org.openbravo.erpCommon.utility.Utility.isModalProcess("28C2ACA37DA74A34AE3C9B6C37CFB528"); 
xmlDocument.setParameter("Procesarfin_Modal", modalProcesarfin?"true":"false");
xmlDocument.setParameter("Eliminarlinea_BTNname", Utility.getButtonName(this, vars, "7B8287FF2A4140228F407C16B6D05626", "Eliminarlinea_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalEliminarlinea = org.openbravo.erpCommon.utility.Utility.isModalProcess("0EBE7D053E6645718CB125EC65527BA8"); 
xmlDocument.setParameter("Eliminarlinea_Modal", modalEliminarlinea?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonProcesariniA3400E7263034DBCB5180A65E0679C48(HttpServletResponse response, VariablesSecureApp vars, String strNEO_Monitoreolinea_ID, String strprocesarini, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process A3400E7263034DBCB5180A65E0679C48");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/ProcesariniA3400E7263034DBCB5180A65E0679C48", discard).createXmlDocument();
      xmlDocument.setParameter("key", strNEO_Monitoreolinea_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "A3400E7263034DBCB5180A65E0679C48");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("A3400E7263034DBCB5180A65E0679C48");
        vars.removeMessage("A3400E7263034DBCB5180A65E0679C48");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonProcesarfin28C2ACA37DA74A34AE3C9B6C37CFB528(HttpServletResponse response, VariablesSecureApp vars, String strNEO_Monitoreolinea_ID, String strprocesarfin, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 28C2ACA37DA74A34AE3C9B6C37CFB528");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Procesarfin28C2ACA37DA74A34AE3C9B6C37CFB528", discard).createXmlDocument();
      xmlDocument.setParameter("key", strNEO_Monitoreolinea_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "28C2ACA37DA74A34AE3C9B6C37CFB528");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("28C2ACA37DA74A34AE3C9B6C37CFB528");
        vars.removeMessage("28C2ACA37DA74A34AE3C9B6C37CFB528");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonEliminarlinea0EBE7D053E6645718CB125EC65527BA8(HttpServletResponse response, VariablesSecureApp vars, String strNEO_Monitoreolinea_ID, String streliminarlinea, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 0EBE7D053E6645718CB125EC65527BA8");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Eliminarlinea0EBE7D053E6645718CB125EC65527BA8", discard).createXmlDocument();
      xmlDocument.setParameter("key", strNEO_Monitoreolinea_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "0EBE7D053E6645718CB125EC65527BA8");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("0EBE7D053E6645718CB125EC65527BA8");
        vars.removeMessage("0EBE7D053E6645718CB125EC65527BA8");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strAD_USER_ID=\"" +Utility.getContext(this, vars, "AD_USER_ID", windowId) + "\";\nvar strAD_ROLE_ID=\"" +Utility.getContext(this, vars, "AD_ROLE_ID", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type, String strPNEO_Monitoreo_ID) throws IOException, ServletException {
    DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars, strPNEO_Monitoreo_ID);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.neoMonitoreolineaId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.getCurrentDBTimestamp(this, data.neoMonitoreolineaId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.neoMonitoreolineaId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|NEO_Monitoreolinea_ID", data.neoMonitoreolineaId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AE. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
