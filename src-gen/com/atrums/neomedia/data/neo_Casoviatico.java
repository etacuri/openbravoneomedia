/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.City;
/**
 * Entity class for entity neo_casoviatico (stored in table neo_casoviatico).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neo_Casoviatico extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_casoviatico";
    public static final String ENTITY_NAME = "neo_casoviatico";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CBPEJECUTIVO = "cBPEjecutivo";
    public static final String PROPERTY_FECHAVIAJE = "fechaViaje";
    public static final String PROPERTY_NEOCITY = "nEOCity";
    public static final String PROPERTY_DESCRIPCION = "descripcion";
    public static final String PROPERTY_VTOTAL = "vtotal";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_VIATICOSTATUS = "viaticostatus";
    public static final String PROPERTY_FECHARETORNO = "fechaRetorno";
    public static final String PROPERTY_CANTHOSPEDAJE = "canthospedaje";
    public static final String PROPERTY_CANTMOVILIZACION = "cantmovilizacion";
    public static final String PROPERTY_CANTALIMENTACION = "cantalimentacion";
    public static final String PROPERTY_VALORHOSPEDAJE = "valorhospedaje";
    public static final String PROPERTY_VALORMOVILIZACION = "valormovilizacion";
    public static final String PROPERTY_VALORALIMENTACION = "valoralimentacion";
    public static final String PROPERTY_TOTLAHOSPEDAJE = "totlahospedaje";
    public static final String PROPERTY_TOTALMOVILIZACION = "totalmovilizacion";
    public static final String PROPERTY_TOTALALIMENTACION = "totalalimentacion";
    public static final String PROPERTY_CANTMOVILIZACIONINTER = "cantmovilizacioninter";
    public static final String PROPERTY_VALORMOVILIZACIONINTER = "valormovilizacioninter";
    public static final String PROPERTY_TOTALMOVILIZACIONINTER = "totalmovilizacioninter";
    public static final String PROPERTY_CANTMOVILIZACIONTAXI = "cantmovilizaciontaxi";
    public static final String PROPERTY_VALORMOVILIZACIONTAXI = "valormovilizaciontaxi";
    public static final String PROPERTY_TOTALMOVILIZACIONTAXI = "totalmovilizaciontaxi";

    public neo_Casoviatico() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_VIATICOSTATUS, "BR");
        setDefaultValue(PROPERTY_VALORMOVILIZACION, new BigDecimal(4.00));
        setDefaultValue(PROPERTY_VALORALIMENTACION, new BigDecimal(4.00));
        setDefaultValue(PROPERTY_VALORMOVILIZACIONTAXI, new BigDecimal(6.00));
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public BusinessPartner getCBPEjecutivo() {
        return (BusinessPartner) get(PROPERTY_CBPEJECUTIVO);
    }

    public void setCBPEjecutivo(BusinessPartner cBPEjecutivo) {
        set(PROPERTY_CBPEJECUTIVO, cBPEjecutivo);
    }

    public Date getFechaViaje() {
        return (Date) get(PROPERTY_FECHAVIAJE);
    }

    public void setFechaViaje(Date fechaViaje) {
        set(PROPERTY_FECHAVIAJE, fechaViaje);
    }

    public City getNEOCity() {
        return (City) get(PROPERTY_NEOCITY);
    }

    public void setNEOCity(City nEOCity) {
        set(PROPERTY_NEOCITY, nEOCity);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

    public BigDecimal getVtotal() {
        return (BigDecimal) get(PROPERTY_VTOTAL);
    }

    public void setVtotal(BigDecimal vtotal) {
        set(PROPERTY_VTOTAL, vtotal);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public String getViaticostatus() {
        return (String) get(PROPERTY_VIATICOSTATUS);
    }

    public void setViaticostatus(String viaticostatus) {
        set(PROPERTY_VIATICOSTATUS, viaticostatus);
    }

    public Date getFechaRetorno() {
        return (Date) get(PROPERTY_FECHARETORNO);
    }

    public void setFechaRetorno(Date fechaRetorno) {
        set(PROPERTY_FECHARETORNO, fechaRetorno);
    }

    public BigDecimal getCanthospedaje() {
        return (BigDecimal) get(PROPERTY_CANTHOSPEDAJE);
    }

    public void setCanthospedaje(BigDecimal canthospedaje) {
        set(PROPERTY_CANTHOSPEDAJE, canthospedaje);
    }

    public BigDecimal getCantmovilizacion() {
        return (BigDecimal) get(PROPERTY_CANTMOVILIZACION);
    }

    public void setCantmovilizacion(BigDecimal cantmovilizacion) {
        set(PROPERTY_CANTMOVILIZACION, cantmovilizacion);
    }

    public BigDecimal getCantalimentacion() {
        return (BigDecimal) get(PROPERTY_CANTALIMENTACION);
    }

    public void setCantalimentacion(BigDecimal cantalimentacion) {
        set(PROPERTY_CANTALIMENTACION, cantalimentacion);
    }

    public BigDecimal getValorhospedaje() {
        return (BigDecimal) get(PROPERTY_VALORHOSPEDAJE);
    }

    public void setValorhospedaje(BigDecimal valorhospedaje) {
        set(PROPERTY_VALORHOSPEDAJE, valorhospedaje);
    }

    public BigDecimal getValormovilizacion() {
        return (BigDecimal) get(PROPERTY_VALORMOVILIZACION);
    }

    public void setValormovilizacion(BigDecimal valormovilizacion) {
        set(PROPERTY_VALORMOVILIZACION, valormovilizacion);
    }

    public BigDecimal getValoralimentacion() {
        return (BigDecimal) get(PROPERTY_VALORALIMENTACION);
    }

    public void setValoralimentacion(BigDecimal valoralimentacion) {
        set(PROPERTY_VALORALIMENTACION, valoralimentacion);
    }

    public BigDecimal getTotlahospedaje() {
        return (BigDecimal) get(PROPERTY_TOTLAHOSPEDAJE);
    }

    public void setTotlahospedaje(BigDecimal totlahospedaje) {
        set(PROPERTY_TOTLAHOSPEDAJE, totlahospedaje);
    }

    public BigDecimal getTotalmovilizacion() {
        return (BigDecimal) get(PROPERTY_TOTALMOVILIZACION);
    }

    public void setTotalmovilizacion(BigDecimal totalmovilizacion) {
        set(PROPERTY_TOTALMOVILIZACION, totalmovilizacion);
    }

    public BigDecimal getTotalalimentacion() {
        return (BigDecimal) get(PROPERTY_TOTALALIMENTACION);
    }

    public void setTotalalimentacion(BigDecimal totalalimentacion) {
        set(PROPERTY_TOTALALIMENTACION, totalalimentacion);
    }

    public BigDecimal getCantmovilizacioninter() {
        return (BigDecimal) get(PROPERTY_CANTMOVILIZACIONINTER);
    }

    public void setCantmovilizacioninter(BigDecimal cantmovilizacioninter) {
        set(PROPERTY_CANTMOVILIZACIONINTER, cantmovilizacioninter);
    }

    public BigDecimal getValormovilizacioninter() {
        return (BigDecimal) get(PROPERTY_VALORMOVILIZACIONINTER);
    }

    public void setValormovilizacioninter(BigDecimal valormovilizacioninter) {
        set(PROPERTY_VALORMOVILIZACIONINTER, valormovilizacioninter);
    }

    public BigDecimal getTotalmovilizacioninter() {
        return (BigDecimal) get(PROPERTY_TOTALMOVILIZACIONINTER);
    }

    public void setTotalmovilizacioninter(BigDecimal totalmovilizacioninter) {
        set(PROPERTY_TOTALMOVILIZACIONINTER, totalmovilizacioninter);
    }

    public BigDecimal getCantmovilizaciontaxi() {
        return (BigDecimal) get(PROPERTY_CANTMOVILIZACIONTAXI);
    }

    public void setCantmovilizaciontaxi(BigDecimal cantmovilizaciontaxi) {
        set(PROPERTY_CANTMOVILIZACIONTAXI, cantmovilizaciontaxi);
    }

    public BigDecimal getValormovilizaciontaxi() {
        return (BigDecimal) get(PROPERTY_VALORMOVILIZACIONTAXI);
    }

    public void setValormovilizaciontaxi(BigDecimal valormovilizaciontaxi) {
        set(PROPERTY_VALORMOVILIZACIONTAXI, valormovilizaciontaxi);
    }

    public BigDecimal getTotalmovilizaciontaxi() {
        return (BigDecimal) get(PROPERTY_TOTALMOVILIZACIONTAXI);
    }

    public void setTotalmovilizaciontaxi(BigDecimal totalmovilizaciontaxi) {
        set(PROPERTY_TOTALMOVILIZACIONTAXI, totalmovilizaciontaxi);
    }

}
