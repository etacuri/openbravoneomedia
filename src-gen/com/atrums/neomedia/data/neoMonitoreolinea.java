/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity neo_monitoreolinea (stored in table neo_monitoreolinea).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoMonitoreolinea extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_monitoreolinea";
    public static final String ENTITY_NAME = "neo_monitoreolinea";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NEOMONITOREO = "nEOMonitoreo";
    public static final String PROPERTY_LINENO = "lineNo";
    public static final String PROPERTY_FECHAMONITOREO = "fechamonitoreo";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_FUNCIONANDO = "funcionando";
    public static final String PROPERTY_SINFUNCIONAR = "sinfuncionar";
    public static final String PROPERTY_PORCENTAJE = "porcentaje";
    public static final String PROPERTY_FECHAREGISTRO = "fecharegistro";
    public static final String PROPERTY_FINICIO = "finicio";
    public static final String PROPERTY_FFIN = "ffin";
    public static final String PROPERTY_TIPOLINEA = "tipolinea";
    public static final String PROPERTY_DESCRIPCIONLINEA = "descripcionlinea";
    public static final String PROPERTY_TIEMPO = "tiempo";
    public static final String PROPERTY_ESTADOLINEA = "estadolinea";
    public static final String PROPERTY_PROCESARINI = "procesarini";
    public static final String PROPERTY_PROCESARFIN = "procesarfin";
    public static final String PROPERTY_ELIMINARLINEA = "eliminarlinea";

    public neoMonitoreolinea() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_ESTADOLINEA, "BOR");
        setDefaultValue(PROPERTY_PROCESARINI, false);
        setDefaultValue(PROPERTY_PROCESARFIN, false);
        setDefaultValue(PROPERTY_ELIMINARLINEA, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public neoMonitoreo getNEOMonitoreo() {
        return (neoMonitoreo) get(PROPERTY_NEOMONITOREO);
    }

    public void setNEOMonitoreo(neoMonitoreo nEOMonitoreo) {
        set(PROPERTY_NEOMONITOREO, nEOMonitoreo);
    }

    public Long getLineNo() {
        return (Long) get(PROPERTY_LINENO);
    }

    public void setLineNo(Long lineNo) {
        set(PROPERTY_LINENO, lineNo);
    }

    public Date getFechamonitoreo() {
        return (Date) get(PROPERTY_FECHAMONITOREO);
    }

    public void setFechamonitoreo(Date fechamonitoreo) {
        set(PROPERTY_FECHAMONITOREO, fechamonitoreo);
    }

    public BigDecimal getCantidad() {
        return (BigDecimal) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(BigDecimal cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public BigDecimal getFuncionando() {
        return (BigDecimal) get(PROPERTY_FUNCIONANDO);
    }

    public void setFuncionando(BigDecimal funcionando) {
        set(PROPERTY_FUNCIONANDO, funcionando);
    }

    public BigDecimal getSinfuncionar() {
        return (BigDecimal) get(PROPERTY_SINFUNCIONAR);
    }

    public void setSinfuncionar(BigDecimal sinfuncionar) {
        set(PROPERTY_SINFUNCIONAR, sinfuncionar);
    }

    public BigDecimal getPorcentaje() {
        return (BigDecimal) get(PROPERTY_PORCENTAJE);
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        set(PROPERTY_PORCENTAJE, porcentaje);
    }

    public Date getFecharegistro() {
        return (Date) get(PROPERTY_FECHAREGISTRO);
    }

    public void setFecharegistro(Date fecharegistro) {
        set(PROPERTY_FECHAREGISTRO, fecharegistro);
    }

    public Date getFinicio() {
        return (Date) get(PROPERTY_FINICIO);
    }

    public void setFinicio(Date finicio) {
        set(PROPERTY_FINICIO, finicio);
    }

    public Date getFfin() {
        return (Date) get(PROPERTY_FFIN);
    }

    public void setFfin(Date ffin) {
        set(PROPERTY_FFIN, ffin);
    }

    public String getTipolinea() {
        return (String) get(PROPERTY_TIPOLINEA);
    }

    public void setTipolinea(String tipolinea) {
        set(PROPERTY_TIPOLINEA, tipolinea);
    }

    public String getDescripcionlinea() {
        return (String) get(PROPERTY_DESCRIPCIONLINEA);
    }

    public void setDescripcionlinea(String descripcionlinea) {
        set(PROPERTY_DESCRIPCIONLINEA, descripcionlinea);
    }

    public BigDecimal getTiempo() {
        return (BigDecimal) get(PROPERTY_TIEMPO);
    }

    public void setTiempo(BigDecimal tiempo) {
        set(PROPERTY_TIEMPO, tiempo);
    }

    public String getEstadolinea() {
        return (String) get(PROPERTY_ESTADOLINEA);
    }

    public void setEstadolinea(String estadolinea) {
        set(PROPERTY_ESTADOLINEA, estadolinea);
    }

    public Boolean isProcesarini() {
        return (Boolean) get(PROPERTY_PROCESARINI);
    }

    public void setProcesarini(Boolean procesarini) {
        set(PROPERTY_PROCESARINI, procesarini);
    }

    public Boolean isProcesarfin() {
        return (Boolean) get(PROPERTY_PROCESARFIN);
    }

    public void setProcesarfin(Boolean procesarfin) {
        set(PROPERTY_PROCESARFIN, procesarfin);
    }

    public Boolean isEliminarlinea() {
        return (Boolean) get(PROPERTY_ELIMINARLINEA);
    }

    public void setEliminarlinea(Boolean eliminarlinea) {
        set(PROPERTY_ELIMINARLINEA, eliminarlinea);
    }

}
