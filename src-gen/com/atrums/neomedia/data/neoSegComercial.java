/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.Order;
/**
 * Entity class for entity neo_seg_comercial (stored in table neo_seg_comercial).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoSegComercial extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_seg_comercial";
    public static final String ENTITY_NAME = "neo_seg_comercial";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NOMBRE = "nombre";
    public static final String PROPERTY_CLIENTE = "cliente";
    public static final String PROPERTY_FECHA = "fecha";
    public static final String PROPERTY_DESCRIPCION = "descripcion";
    public static final String PROPERTY_RESPONSABLE = "responsable";
    public static final String PROPERTY_ISONESHOT = "isoneshot";
    public static final String PROPERTY_ISRECURRENTE = "isrecurrente";
    public static final String PROPERTY_ISVENTAPUBLICIDAD = "isventapublicidad";
    public static final String PROPERTY_ISVENTAHWSW = "isventahwsw";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_CBPLOCATION = "cBPLocation";
    public static final String PROPERTY_DOCUMENTSTATUS = "documentStatus";
    public static final String PROPERTY_DOCUMENTACTION = "documentAction";
    public static final String PROPERTY_PROCESASEG = "procesaseg";
    public static final String PROPERTY_ORDEREMNEOSEGCOMERCIALIDLIST = "orderEMNeoSegComercialIDList";
    public static final String PROPERTY_NEOCITASLIST = "neoCitasList";
    public static final String PROPERTY_NEOMONITOREOOPORTUNIDADIDLIST = "neoMonitoreoOportunidadIDList";

    public neoSegComercial() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_ISONESHOT, false);
        setDefaultValue(PROPERTY_ISRECURRENTE, false);
        setDefaultValue(PROPERTY_ISVENTAPUBLICIDAD, false);
        setDefaultValue(PROPERTY_ISVENTAHWSW, false);
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_DOCUMENTSTATUS, "AB");
        setDefaultValue(PROPERTY_DOCUMENTACTION, "CR");
        setDefaultValue(PROPERTY_PROCESASEG, false);
        setDefaultValue(PROPERTY_ORDEREMNEOSEGCOMERCIALIDLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_NEOCITASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_NEOMONITOREOOPORTUNIDADIDLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getNombre() {
        return (String) get(PROPERTY_NOMBRE);
    }

    public void setNombre(String nombre) {
        set(PROPERTY_NOMBRE, nombre);
    }

    public BusinessPartner getCliente() {
        return (BusinessPartner) get(PROPERTY_CLIENTE);
    }

    public void setCliente(BusinessPartner cliente) {
        set(PROPERTY_CLIENTE, cliente);
    }

    public Date getFecha() {
        return (Date) get(PROPERTY_FECHA);
    }

    public void setFecha(Date fecha) {
        set(PROPERTY_FECHA, fecha);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

    public BusinessPartner getResponsable() {
        return (BusinessPartner) get(PROPERTY_RESPONSABLE);
    }

    public void setResponsable(BusinessPartner responsable) {
        set(PROPERTY_RESPONSABLE, responsable);
    }

    public Boolean isOneshot() {
        return (Boolean) get(PROPERTY_ISONESHOT);
    }

    public void setOneshot(Boolean isoneshot) {
        set(PROPERTY_ISONESHOT, isoneshot);
    }

    public Boolean isRecurrente() {
        return (Boolean) get(PROPERTY_ISRECURRENTE);
    }

    public void setRecurrente(Boolean isrecurrente) {
        set(PROPERTY_ISRECURRENTE, isrecurrente);
    }

    public Boolean isVentapublicidad() {
        return (Boolean) get(PROPERTY_ISVENTAPUBLICIDAD);
    }

    public void setVentapublicidad(Boolean isventapublicidad) {
        set(PROPERTY_ISVENTAPUBLICIDAD, isventapublicidad);
    }

    public Boolean isVentahwsw() {
        return (Boolean) get(PROPERTY_ISVENTAHWSW);
    }

    public void setVentahwsw(Boolean isventahwsw) {
        set(PROPERTY_ISVENTAHWSW, isventahwsw);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public Location getCBPLocation() {
        return (Location) get(PROPERTY_CBPLOCATION);
    }

    public void setCBPLocation(Location cBPLocation) {
        set(PROPERTY_CBPLOCATION, cBPLocation);
    }

    public String getDocumentStatus() {
        return (String) get(PROPERTY_DOCUMENTSTATUS);
    }

    public void setDocumentStatus(String documentStatus) {
        set(PROPERTY_DOCUMENTSTATUS, documentStatus);
    }

    public String getDocumentAction() {
        return (String) get(PROPERTY_DOCUMENTACTION);
    }

    public void setDocumentAction(String documentAction) {
        set(PROPERTY_DOCUMENTACTION, documentAction);
    }

    public Boolean isProcesaseg() {
        return (Boolean) get(PROPERTY_PROCESASEG);
    }

    public void setProcesaseg(Boolean procesaseg) {
        set(PROPERTY_PROCESASEG, procesaseg);
    }

    @SuppressWarnings("unchecked")
    public List<Order> getOrderEMNeoSegComercialIDList() {
      return (List<Order>) get(PROPERTY_ORDEREMNEOSEGCOMERCIALIDLIST);
    }

    public void setOrderEMNeoSegComercialIDList(List<Order> orderEMNeoSegComercialIDList) {
        set(PROPERTY_ORDEREMNEOSEGCOMERCIALIDLIST, orderEMNeoSegComercialIDList);
    }

    @SuppressWarnings("unchecked")
    public List<neoCitas> getNeoCitasList() {
      return (List<neoCitas>) get(PROPERTY_NEOCITASLIST);
    }

    public void setNeoCitasList(List<neoCitas> neoCitasList) {
        set(PROPERTY_NEOCITASLIST, neoCitasList);
    }

    @SuppressWarnings("unchecked")
    public List<neoMonitoreo> getNeoMonitoreoOportunidadIDList() {
      return (List<neoMonitoreo>) get(PROPERTY_NEOMONITOREOOPORTUNIDADIDLIST);
    }

    public void setNeoMonitoreoOportunidadIDList(List<neoMonitoreo> neoMonitoreoOportunidadIDList) {
        set(PROPERTY_NEOMONITOREOOPORTUNIDADIDLIST, neoMonitoreoOportunidadIDList);
    }

}
