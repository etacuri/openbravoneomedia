/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity neo_citas (stored in table neo_citas).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoCitas extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_citas";
    public static final String ENTITY_NAME = "neo_citas";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NEOSEGCOMERCIAL = "nEOSegComercial";
    public static final String PROPERTY_CONTACTO = "contacto";
    public static final String PROPERTY_FECHAVISITA = "fechavisita";
    public static final String PROPERTY_TIPOCITA = "tipocita";
    public static final String PROPERTY_TIEMPOEJECUTADO = "tiempoejecutado";
    public static final String PROPERTY_ISEJECUTADO = "isejecutado";
    public static final String PROPERTY_TELEFONOCONTACTO = "telefonoContacto";
    public static final String PROPERTY_EJECUTIVO = "ejecutivo";
    public static final String PROPERTY_EMAILCONTACTO = "emailContacto";
    public static final String PROPERTY_RESUMENCITA = "resumenCita";
    public static final String PROPERTY_CBPLOCATION = "cBPLocation";

    public neoCitas() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TIEMPOEJECUTADO, (long) 0);
        setDefaultValue(PROPERTY_ISEJECUTADO, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public neoSegComercial getNEOSegComercial() {
        return (neoSegComercial) get(PROPERTY_NEOSEGCOMERCIAL);
    }

    public void setNEOSegComercial(neoSegComercial nEOSegComercial) {
        set(PROPERTY_NEOSEGCOMERCIAL, nEOSegComercial);
    }

    public User getContacto() {
        return (User) get(PROPERTY_CONTACTO);
    }

    public void setContacto(User contacto) {
        set(PROPERTY_CONTACTO, contacto);
    }

    public Date getFechavisita() {
        return (Date) get(PROPERTY_FECHAVISITA);
    }

    public void setFechavisita(Date fechavisita) {
        set(PROPERTY_FECHAVISITA, fechavisita);
    }

    public String getTipocita() {
        return (String) get(PROPERTY_TIPOCITA);
    }

    public void setTipocita(String tipocita) {
        set(PROPERTY_TIPOCITA, tipocita);
    }

    public Long getTiempoejecutado() {
        return (Long) get(PROPERTY_TIEMPOEJECUTADO);
    }

    public void setTiempoejecutado(Long tiempoejecutado) {
        set(PROPERTY_TIEMPOEJECUTADO, tiempoejecutado);
    }

    public Boolean isEjecutado() {
        return (Boolean) get(PROPERTY_ISEJECUTADO);
    }

    public void setEjecutado(Boolean isejecutado) {
        set(PROPERTY_ISEJECUTADO, isejecutado);
    }

    public String getTelefonoContacto() {
        return (String) get(PROPERTY_TELEFONOCONTACTO);
    }

    public void setTelefonoContacto(String telefonoContacto) {
        set(PROPERTY_TELEFONOCONTACTO, telefonoContacto);
    }

    public BusinessPartner getEjecutivo() {
        return (BusinessPartner) get(PROPERTY_EJECUTIVO);
    }

    public void setEjecutivo(BusinessPartner ejecutivo) {
        set(PROPERTY_EJECUTIVO, ejecutivo);
    }

    public String getEmailContacto() {
        return (String) get(PROPERTY_EMAILCONTACTO);
    }

    public void setEmailContacto(String emailContacto) {
        set(PROPERTY_EMAILCONTACTO, emailContacto);
    }

    public String getResumenCita() {
        return (String) get(PROPERTY_RESUMENCITA);
    }

    public void setResumenCita(String resumenCita) {
        set(PROPERTY_RESUMENCITA, resumenCita);
    }

    public Location getCBPLocation() {
        return (Location) get(PROPERTY_CBPLOCATION);
    }

    public void setCBPLocation(Location cBPLocation) {
        set(PROPERTY_CBPLOCATION, cBPLocation);
    }

}
