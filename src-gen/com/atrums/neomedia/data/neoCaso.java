/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import com.atrums.nomina.data.noAreaEmpresa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity neo_caso (stored in table neo_caso).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoCaso extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_caso";
    public static final String ENTITY_NAME = "neo_caso";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_DOCUMENTTYPE = "documentType";
    public static final String PROPERTY_DOCUMENTNO = "documentNo";
    public static final String PROPERTY_CBPCLIENTE = "cBPCliente";
    public static final String PROPERTY_NOMBRECASO = "nombrecaso";
    public static final String PROPERTY_DESCRIPCIONCASO = "descripcioncaso";
    public static final String PROPERTY_CBPEJECUTIVO = "cBPEjecutivo";
    public static final String PROPERTY_TIPOCASO = "tipocaso";
    public static final String PROPERTY_TIEMPOSUMATORIA = "tiemposumatoria";
    public static final String PROPERTY_NEOZONA = "nEOZona";
    public static final String PROPERTY_DOCUMENTSTATUS = "documentStatus";
    public static final String PROPERTY_INICIO = "inicio";
    public static final String PROPERTY_FINAL = "final";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_NEOAREAEMPRESA = "nEOAreaEmpresa";
    public static final String PROPERTY_CIUDAD = "ciudad";
    public static final String PROPERTY_AGENCIA = "agencia";
    public static final String PROPERTY_PRIORIDAD = "prioridad";
    public static final String PROPERTY_ORIGEN = "origen";
    public static final String PROPERTY_DIRECCIONCLI = "direccioncli";
    public static final String PROPERTY_CONTANTOCLI = "contantocli";
    public static final String PROPERTY_ISENVIOEMAIL = "isenvioemail";
    public static final String PROPERTY_CANALTERCERO = "canaltercero";
    public static final String PROPERTY_AGENCIACANAL = "agenciacanal";
    public static final String PROPERTY_ESTADOCORREO = "estadocorreo";
    public static final String PROPERTY_NEOCASOLINELIST = "neoCasolineList";

    public neoCaso() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TIEMPOSUMATORIA, new BigDecimal(0));
        setDefaultValue(PROPERTY_DOCUMENTSTATUS, "AB");
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_ISENVIOEMAIL, false);
        setDefaultValue(PROPERTY_ESTADOCORREO, "NA");
        setDefaultValue(PROPERTY_NEOCASOLINELIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public DocumentType getDocumentType() {
        return (DocumentType) get(PROPERTY_DOCUMENTTYPE);
    }

    public void setDocumentType(DocumentType documentType) {
        set(PROPERTY_DOCUMENTTYPE, documentType);
    }

    public String getDocumentNo() {
        return (String) get(PROPERTY_DOCUMENTNO);
    }

    public void setDocumentNo(String documentNo) {
        set(PROPERTY_DOCUMENTNO, documentNo);
    }

    public BusinessPartner getCBPCliente() {
        return (BusinessPartner) get(PROPERTY_CBPCLIENTE);
    }

    public void setCBPCliente(BusinessPartner cBPCliente) {
        set(PROPERTY_CBPCLIENTE, cBPCliente);
    }

    public String getNombrecaso() {
        return (String) get(PROPERTY_NOMBRECASO);
    }

    public void setNombrecaso(String nombrecaso) {
        set(PROPERTY_NOMBRECASO, nombrecaso);
    }

    public String getDescripcioncaso() {
        return (String) get(PROPERTY_DESCRIPCIONCASO);
    }

    public void setDescripcioncaso(String descripcioncaso) {
        set(PROPERTY_DESCRIPCIONCASO, descripcioncaso);
    }

    public User getCBPEjecutivo() {
        return (User) get(PROPERTY_CBPEJECUTIVO);
    }

    public void setCBPEjecutivo(User cBPEjecutivo) {
        set(PROPERTY_CBPEJECUTIVO, cBPEjecutivo);
    }

    public String getTipocaso() {
        return (String) get(PROPERTY_TIPOCASO);
    }

    public void setTipocaso(String tipocaso) {
        set(PROPERTY_TIPOCASO, tipocaso);
    }

    public BigDecimal getTiemposumatoria() {
        return (BigDecimal) get(PROPERTY_TIEMPOSUMATORIA);
    }

    public void setTiemposumatoria(BigDecimal tiemposumatoria) {
        set(PROPERTY_TIEMPOSUMATORIA, tiemposumatoria);
    }

    public neoZona getNEOZona() {
        return (neoZona) get(PROPERTY_NEOZONA);
    }

    public void setNEOZona(neoZona nEOZona) {
        set(PROPERTY_NEOZONA, nEOZona);
    }

    public String getDocumentStatus() {
        return (String) get(PROPERTY_DOCUMENTSTATUS);
    }

    public void setDocumentStatus(String documentStatus) {
        set(PROPERTY_DOCUMENTSTATUS, documentStatus);
    }

    public Date getInicio() {
        return (Date) get(PROPERTY_INICIO);
    }

    public void setInicio(Date inicio) {
        set(PROPERTY_INICIO, inicio);
    }

    public Date getFinal() {
        return (Date) get(PROPERTY_FINAL);
    }

    public void setFinal(Date fnl) {
        set(PROPERTY_FINAL, fnl);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public noAreaEmpresa getNEOAreaEmpresa() {
        return (noAreaEmpresa) get(PROPERTY_NEOAREAEMPRESA);
    }

    public void setNEOAreaEmpresa(noAreaEmpresa nEOAreaEmpresa) {
        set(PROPERTY_NEOAREAEMPRESA, nEOAreaEmpresa);
    }

    public String getCiudad() {
        return (String) get(PROPERTY_CIUDAD);
    }

    public void setCiudad(String ciudad) {
        set(PROPERTY_CIUDAD, ciudad);
    }

    public String getAgencia() {
        return (String) get(PROPERTY_AGENCIA);
    }

    public void setAgencia(String agencia) {
        set(PROPERTY_AGENCIA, agencia);
    }

    public String getPrioridad() {
        return (String) get(PROPERTY_PRIORIDAD);
    }

    public void setPrioridad(String prioridad) {
        set(PROPERTY_PRIORIDAD, prioridad);
    }

    public String getOrigen() {
        return (String) get(PROPERTY_ORIGEN);
    }

    public void setOrigen(String origen) {
        set(PROPERTY_ORIGEN, origen);
    }

    public Location getDireccioncli() {
        return (Location) get(PROPERTY_DIRECCIONCLI);
    }

    public void setDireccioncli(Location direccioncli) {
        set(PROPERTY_DIRECCIONCLI, direccioncli);
    }

    public User getContantocli() {
        return (User) get(PROPERTY_CONTANTOCLI);
    }

    public void setContantocli(User contantocli) {
        set(PROPERTY_CONTANTOCLI, contantocli);
    }

    public Boolean isEnvioemail() {
        return (Boolean) get(PROPERTY_ISENVIOEMAIL);
    }

    public void setEnvioemail(Boolean isenvioemail) {
        set(PROPERTY_ISENVIOEMAIL, isenvioemail);
    }

    public neoAgencia getCanaltercero() {
        return (neoAgencia) get(PROPERTY_CANALTERCERO);
    }

    public void setCanaltercero(neoAgencia canaltercero) {
        set(PROPERTY_CANALTERCERO, canaltercero);
    }

    public neoAgencialinea getAgenciacanal() {
        return (neoAgencialinea) get(PROPERTY_AGENCIACANAL);
    }

    public void setAgenciacanal(neoAgencialinea agenciacanal) {
        set(PROPERTY_AGENCIACANAL, agenciacanal);
    }

    public String getEstadocorreo() {
        return (String) get(PROPERTY_ESTADOCORREO);
    }

    public void setEstadocorreo(String estadocorreo) {
        set(PROPERTY_ESTADOCORREO, estadocorreo);
    }

    @SuppressWarnings("unchecked")
    public List<neoCasoline> getNeoCasolineList() {
      return (List<neoCasoline>) get(PROPERTY_NEOCASOLINELIST);
    }

    public void setNeoCasolineList(List<neoCasoline> neoCasolineList) {
        set(PROPERTY_NEOCASOLINELIST, neoCasolineList);
    }

}
