/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
/**
 * Entity class for entity neo_lineasfacturar_v (stored in table neo_lineasfacturar_v).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoLineasfacturarV extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_lineasfacturar_v";
    public static final String ENTITY_NAME = "neo_lineasfacturar_v";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_NUMDOCUMENTO = "numdocumento";
    public static final String PROPERTY_DOCUMENTTYPE = "documentType";
    public static final String PROPERTY_EMPLEADO = "empleado";
    public static final String PROPERTY_FECHADEORDEN = "fechadeorden";
    public static final String PROPERTY_NUMCOTIZA = "numcotiza";
    public static final String PROPERTY_PRODUCT = "product";
    public static final String PROPERTY_CANTIDAD = "cantidad";
    public static final String PROPERTY_ESTADOFACTURADO = "estadofacturado";
    public static final String PROPERTY_CANTPENDFACTURAR = "cantPendFacturar";
    public static final String PROPERTY_INICIO = "inicio";
    public static final String PROPERTY_FIN = "fin";
    public static final String PROPERTY_FACTURAR = "facturar";
    public static final String PROPERTY_UNITPRICE = "unitPrice";
    public static final String PROPERTY_LINENETAMOUNT = "lineNetAmount";
    public static final String PROPERTY_CANTFACTURAR = "cantfacturar";
    public static final String PROPERTY_GENFACTURAR = "genfacturar";

    public neoLineasfacturarV() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, false);
        setDefaultValue(PROPERTY_GENFACTURAR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isProcessed() {
        return (Boolean) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(Boolean processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public String getNumdocumento() {
        return (String) get(PROPERTY_NUMDOCUMENTO);
    }

    public void setNumdocumento(String numdocumento) {
        set(PROPERTY_NUMDOCUMENTO, numdocumento);
    }

    public DocumentType getDocumentType() {
        return (DocumentType) get(PROPERTY_DOCUMENTTYPE);
    }

    public void setDocumentType(DocumentType documentType) {
        set(PROPERTY_DOCUMENTTYPE, documentType);
    }

    public BusinessPartner getEmpleado() {
        return (BusinessPartner) get(PROPERTY_EMPLEADO);
    }

    public void setEmpleado(BusinessPartner empleado) {
        set(PROPERTY_EMPLEADO, empleado);
    }

    public Date getFechadeorden() {
        return (Date) get(PROPERTY_FECHADEORDEN);
    }

    public void setFechadeorden(Date fechadeorden) {
        set(PROPERTY_FECHADEORDEN, fechadeorden);
    }

    public String getNumcotiza() {
        return (String) get(PROPERTY_NUMCOTIZA);
    }

    public void setNumcotiza(String numcotiza) {
        set(PROPERTY_NUMCOTIZA, numcotiza);
    }

    public Product getProduct() {
        return (Product) get(PROPERTY_PRODUCT);
    }

    public void setProduct(Product product) {
        set(PROPERTY_PRODUCT, product);
    }

    public Long getCantidad() {
        return (Long) get(PROPERTY_CANTIDAD);
    }

    public void setCantidad(Long cantidad) {
        set(PROPERTY_CANTIDAD, cantidad);
    }

    public String getEstadofacturado() {
        return (String) get(PROPERTY_ESTADOFACTURADO);
    }

    public void setEstadofacturado(String estadofacturado) {
        set(PROPERTY_ESTADOFACTURADO, estadofacturado);
    }

    public Long getCantPendFacturar() {
        return (Long) get(PROPERTY_CANTPENDFACTURAR);
    }

    public void setCantPendFacturar(Long cantPendFacturar) {
        set(PROPERTY_CANTPENDFACTURAR, cantPendFacturar);
    }

    public Date getInicio() {
        return (Date) get(PROPERTY_INICIO);
    }

    public void setInicio(Date inicio) {
        set(PROPERTY_INICIO, inicio);
    }

    public Date getFin() {
        return (Date) get(PROPERTY_FIN);
    }

    public void setFin(Date fin) {
        set(PROPERTY_FIN, fin);
    }

    public Date getFacturar() {
        return (Date) get(PROPERTY_FACTURAR);
    }

    public void setFacturar(Date facturar) {
        set(PROPERTY_FACTURAR, facturar);
    }

    public BigDecimal getUnitPrice() {
        return (BigDecimal) get(PROPERTY_UNITPRICE);
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        set(PROPERTY_UNITPRICE, unitPrice);
    }

    public BigDecimal getLineNetAmount() {
        return (BigDecimal) get(PROPERTY_LINENETAMOUNT);
    }

    public void setLineNetAmount(BigDecimal lineNetAmount) {
        set(PROPERTY_LINENETAMOUNT, lineNetAmount);
    }

    public Long getCantfacturar() {
        return (Long) get(PROPERTY_CANTFACTURAR);
    }

    public void setCantfacturar(Long cantfacturar) {
        set(PROPERTY_CANTFACTURAR, cantfacturar);
    }

    public Boolean isGenfacturar() {
        return (Boolean) get(PROPERTY_GENFACTURAR);
    }

    public void setGenfacturar(Boolean genfacturar) {
        set(PROPERTY_GENFACTURAR, genfacturar);
    }

}
