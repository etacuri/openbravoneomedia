/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.assetmgmt.Asset;
/**
 * Entity class for entity neo_inventario (stored in table neo_inventario).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoInventario extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_inventario";
    public static final String ENTITY_NAME = "neo_inventario";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NEOAGENCIALINEA = "nEOAgencialinea";
    public static final String PROPERTY_PROPIETARIO = "propietario";
    public static final String PROPERTY_TIPOINVENTARIO = "tipoinventario";
    public static final String PROPERTY_MARCA = "marca";
    public static final String PROPERTY_NUMEROSERIE = "numeroserie";
    public static final String PROPERTY_MODELO = "modelo";
    public static final String PROPERTY_MACADDRES = "macaddres";
    public static final String PROPERTY_FECHACOMPRA = "fechacompra";
    public static final String PROPERTY_ANIOSGARANTIA = "aniosgarantia";
    public static final String PROPERTY_IP = "ip";
    public static final String PROPERTY_MASCARA = "mascara";
    public static final String PROPERTY_GATEWAY = "gateway";
    public static final String PROPERTY_TIPOLICENCIA = "tipolicencia";
    public static final String PROPERTY_MARCALICENCIA = "marcalicencia";
    public static final String PROPERTY_TIPOPROPIETARIO = "tipopropietario";
    public static final String PROPERTY_ACTIVOINVENTARIO = "activoinventario";

    public neoInventario() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_TIPOINVENTARIO, "EQ");
        setDefaultValue(PROPERTY_TIPOPROPIETARIO, "EQ");
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public neoAgencialinea getNEOAgencialinea() {
        return (neoAgencialinea) get(PROPERTY_NEOAGENCIALINEA);
    }

    public void setNEOAgencialinea(neoAgencialinea nEOAgencialinea) {
        set(PROPERTY_NEOAGENCIALINEA, nEOAgencialinea);
    }

    public BusinessPartner getPropietario() {
        return (BusinessPartner) get(PROPERTY_PROPIETARIO);
    }

    public void setPropietario(BusinessPartner propietario) {
        set(PROPERTY_PROPIETARIO, propietario);
    }

    public String getTipoinventario() {
        return (String) get(PROPERTY_TIPOINVENTARIO);
    }

    public void setTipoinventario(String tipoinventario) {
        set(PROPERTY_TIPOINVENTARIO, tipoinventario);
    }

    public String getMarca() {
        return (String) get(PROPERTY_MARCA);
    }

    public void setMarca(String marca) {
        set(PROPERTY_MARCA, marca);
    }

    public String getNumeroserie() {
        return (String) get(PROPERTY_NUMEROSERIE);
    }

    public void setNumeroserie(String numeroserie) {
        set(PROPERTY_NUMEROSERIE, numeroserie);
    }

    public String getModelo() {
        return (String) get(PROPERTY_MODELO);
    }

    public void setModelo(String modelo) {
        set(PROPERTY_MODELO, modelo);
    }

    public String getMacaddres() {
        return (String) get(PROPERTY_MACADDRES);
    }

    public void setMacaddres(String macaddres) {
        set(PROPERTY_MACADDRES, macaddres);
    }

    public Date getFechacompra() {
        return (Date) get(PROPERTY_FECHACOMPRA);
    }

    public void setFechacompra(Date fechacompra) {
        set(PROPERTY_FECHACOMPRA, fechacompra);
    }

    public Long getAniosgarantia() {
        return (Long) get(PROPERTY_ANIOSGARANTIA);
    }

    public void setAniosgarantia(Long aniosgarantia) {
        set(PROPERTY_ANIOSGARANTIA, aniosgarantia);
    }

    public String getIp() {
        return (String) get(PROPERTY_IP);
    }

    public void setIp(String ip) {
        set(PROPERTY_IP, ip);
    }

    public String getMascara() {
        return (String) get(PROPERTY_MASCARA);
    }

    public void setMascara(String mascara) {
        set(PROPERTY_MASCARA, mascara);
    }

    public String getGateway() {
        return (String) get(PROPERTY_GATEWAY);
    }

    public void setGateway(String gateway) {
        set(PROPERTY_GATEWAY, gateway);
    }

    public String getTipolicencia() {
        return (String) get(PROPERTY_TIPOLICENCIA);
    }

    public void setTipolicencia(String tipolicencia) {
        set(PROPERTY_TIPOLICENCIA, tipolicencia);
    }

    public String getMarcalicencia() {
        return (String) get(PROPERTY_MARCALICENCIA);
    }

    public void setMarcalicencia(String marcalicencia) {
        set(PROPERTY_MARCALICENCIA, marcalicencia);
    }

    public String getTipopropietario() {
        return (String) get(PROPERTY_TIPOPROPIETARIO);
    }

    public void setTipopropietario(String tipopropietario) {
        set(PROPERTY_TIPOPROPIETARIO, tipopropietario);
    }

    public Asset getActivoinventario() {
        return (Asset) get(PROPERTY_ACTIVOINVENTARIO);
    }

    public void setActivoinventario(Asset activoinventario) {
        set(PROPERTY_ACTIVOINVENTARIO, activoinventario);
    }

}
