/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
/**
 * Entity class for entity neo_monitoreo (stored in table neo_monitoreo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoMonitoreo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_monitoreo";
    public static final String ENTITY_NAME = "neo_monitoreo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_DOCUMENTTYPE = "documentType";
    public static final String PROPERTY_DOCUMENTNO = "documentNo";
    public static final String PROPERTY_FECHAINICIO = "fechaInicio";
    public static final String PROPERTY_FECHAFINAL = "fechaFinal";
    public static final String PROPERTY_CBPSUPERVISOR = "cBPSupervisor";
    public static final String PROPERTY_MONITOR = "monitor";
    public static final String PROPERTY_DESCRIPCION = "descripcion";
    public static final String PROPERTY_DOCUMENTSTATUS = "documentStatus";
    public static final String PROPERTY_PROCESAR = "procesar";
    public static final String PROPERTY_CBPCLIENTE = "cBPCliente";
    public static final String PROPERTY_ORDENTRABAJO = "ordentrabajo";
    public static final String PROPERTY_OPORTUNIDAD = "oportunidad";
    public static final String PROPERTY_CONTACTOPUBLI = "contactopubli";
    public static final String PROPERTY_ORDENTRABAJOLINEA = "ordentrabajolinea";
    public static final String PROPERTY_REPRODUCTORES = "reproductores";
    public static final String PROPERTY_FECHAFACTURA = "fechaFactura";
    public static final String PROPERTY_GENERARFACTURA = "generarFactura";
    public static final String PROPERTY_NEOMONITOREOLINEALIST = "neoMonitoreolineaList";

    public neoMonitoreo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_DOCUMENTSTATUS, "AB");
        setDefaultValue(PROPERTY_PROCESAR, false);
        setDefaultValue(PROPERTY_GENERARFACTURA, false);
        setDefaultValue(PROPERTY_NEOMONITOREOLINEALIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public DocumentType getDocumentType() {
        return (DocumentType) get(PROPERTY_DOCUMENTTYPE);
    }

    public void setDocumentType(DocumentType documentType) {
        set(PROPERTY_DOCUMENTTYPE, documentType);
    }

    public String getDocumentNo() {
        return (String) get(PROPERTY_DOCUMENTNO);
    }

    public void setDocumentNo(String documentNo) {
        set(PROPERTY_DOCUMENTNO, documentNo);
    }

    public Date getFechaInicio() {
        return (Date) get(PROPERTY_FECHAINICIO);
    }

    public void setFechaInicio(Date fechaInicio) {
        set(PROPERTY_FECHAINICIO, fechaInicio);
    }

    public Date getFechaFinal() {
        return (Date) get(PROPERTY_FECHAFINAL);
    }

    public void setFechaFinal(Date fechaFinal) {
        set(PROPERTY_FECHAFINAL, fechaFinal);
    }

    public BusinessPartner getCBPSupervisor() {
        return (BusinessPartner) get(PROPERTY_CBPSUPERVISOR);
    }

    public void setCBPSupervisor(BusinessPartner cBPSupervisor) {
        set(PROPERTY_CBPSUPERVISOR, cBPSupervisor);
    }

    public String getMonitor() {
        return (String) get(PROPERTY_MONITOR);
    }

    public void setMonitor(String monitor) {
        set(PROPERTY_MONITOR, monitor);
    }

    public String getDescripcion() {
        return (String) get(PROPERTY_DESCRIPCION);
    }

    public void setDescripcion(String descripcion) {
        set(PROPERTY_DESCRIPCION, descripcion);
    }

    public String getDocumentStatus() {
        return (String) get(PROPERTY_DOCUMENTSTATUS);
    }

    public void setDocumentStatus(String documentStatus) {
        set(PROPERTY_DOCUMENTSTATUS, documentStatus);
    }

    public Boolean isProcesar() {
        return (Boolean) get(PROPERTY_PROCESAR);
    }

    public void setProcesar(Boolean procesar) {
        set(PROPERTY_PROCESAR, procesar);
    }

    public BusinessPartner getCBPCliente() {
        return (BusinessPartner) get(PROPERTY_CBPCLIENTE);
    }

    public void setCBPCliente(BusinessPartner cBPCliente) {
        set(PROPERTY_CBPCLIENTE, cBPCliente);
    }

    public Order getOrdentrabajo() {
        return (Order) get(PROPERTY_ORDENTRABAJO);
    }

    public void setOrdentrabajo(Order ordentrabajo) {
        set(PROPERTY_ORDENTRABAJO, ordentrabajo);
    }

    public neoSegComercial getOportunidad() {
        return (neoSegComercial) get(PROPERTY_OPORTUNIDAD);
    }

    public void setOportunidad(neoSegComercial oportunidad) {
        set(PROPERTY_OPORTUNIDAD, oportunidad);
    }

    public User getContactopubli() {
        return (User) get(PROPERTY_CONTACTOPUBLI);
    }

    public void setContactopubli(User contactopubli) {
        set(PROPERTY_CONTACTOPUBLI, contactopubli);
    }

    public OrderLine getOrdentrabajolinea() {
        return (OrderLine) get(PROPERTY_ORDENTRABAJOLINEA);
    }

    public void setOrdentrabajolinea(OrderLine ordentrabajolinea) {
        set(PROPERTY_ORDENTRABAJOLINEA, ordentrabajolinea);
    }

    public BigDecimal getReproductores() {
        return (BigDecimal) get(PROPERTY_REPRODUCTORES);
    }

    public void setReproductores(BigDecimal reproductores) {
        set(PROPERTY_REPRODUCTORES, reproductores);
    }

    public Date getFechaFactura() {
        return (Date) get(PROPERTY_FECHAFACTURA);
    }

    public void setFechaFactura(Date fechaFactura) {
        set(PROPERTY_FECHAFACTURA, fechaFactura);
    }

    public Boolean isGenerarFactura() {
        return (Boolean) get(PROPERTY_GENERARFACTURA);
    }

    public void setGenerarFactura(Boolean generarFactura) {
        set(PROPERTY_GENERARFACTURA, generarFactura);
    }

    @SuppressWarnings("unchecked")
    public List<neoMonitoreolinea> getNeoMonitoreolineaList() {
      return (List<neoMonitoreolinea>) get(PROPERTY_NEOMONITOREOLINEALIST);
    }

    public void setNeoMonitoreolineaList(List<neoMonitoreolinea> neoMonitoreolineaList) {
        set(PROPERTY_NEOMONITOREOLINEALIST, neoMonitoreolineaList);
    }

}
