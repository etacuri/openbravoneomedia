/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.OrderLine;
/**
 * Entity class for entity neo_lineprod (stored in table neo_lineprod).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoLineprod extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_lineprod";
    public static final String ENTITY_NAME = "neo_lineprod";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_SALESORDERLINE = "salesOrderLine";
    public static final String PROPERTY_NEOCANTIDAD = "nEOCantidad";
    public static final String PROPERTY_NEOUNITFOB = "nEOUnitFob";
    public static final String PROPERTY_NEOTOTALFOB = "nEOTotalFob";
    public static final String PROPERTY_NEOPESOBRUTOUNIT = "nEOPesobrutoUnit";
    public static final String PROPERTY_NEOPESOBRUTOTOTAL = "nEOPesobrutoTotal";
    public static final String PROPERTY_NEOANCHO = "nEOAncho";
    public static final String PROPERTY_NEOALTO = "nEOAlto";
    public static final String PROPERTY_NEOPROFUNDIDAD = "nEOProfundidad";
    public static final String PROPERTY_NEOPESOVOL = "nEOPesoVol";
    public static final String PROPERTY_NEOPESOCALCULO = "nEOPesoCalculo";
    public static final String PROPERTY_NEOINTETCPOR = "nEOInteTcPor";
    public static final String PROPERTY_NEOINTETC = "nEOInteTc";
    public static final String PROPERTY_NEOSALIDACAPPOR = "nEOSalidaCapPor";
    public static final String PROPERTY_NEOSALIDACAP = "nEOSalidaCap";
    public static final String PROPERTY_NEOFLETE = "nEOFlete";
    public static final String PROPERTY_NEOSEGURO = "nEOSeguro";
    public static final String PROPERTY_NEOFODIN = "nEOFodin";
    public static final String PROPERTY_NEOARANCELPOR = "nEOArancelPor";
    public static final String PROPERTY_NEOARANCEL = "nEOArancel";
    public static final String PROPERTY_NEORETENCIONESPOR = "nEORetencionesPor";
    public static final String PROPERTY_NEORETENCIONES = "nEORetenciones";
    public static final String PROPERTY_NEOMARGENVENTAPORC = "nEOMargenVentaporc";
    public static final String PROPERTY_NEOMARGENVENTA = "nEOMargenVenta";
    public static final String PROPERTY_NEOCOSTOFIN = "nEOCostoFin";
    public static final String PROPERTY_NEOPMUNITARIORENTA = "nEOPmunitarioRenta";
    public static final String PROPERTY_NEOPLAZO = "nEOPlazo";
    public static final String PROPERTY_NEOCIF = "nEOCif";
    public static final String PROPERTY_NEONACIONALIZACION = "nEONacionalizacion";
    public static final String PROPERTY_NEOSEGUROADUANA = "nEOSeguroAduana";
    public static final String PROPERTY_NEOCOSTOADUANA = "nEOCostoAduana";
    public static final String PROPERTY_NEOTRANSPLOCAL = "nEOTranspLocal";
    public static final String PROPERTY_NEOSEGUROLOCAL = "nEOSeguroLocal";
    public static final String PROPERTY_NEOCOSTOSERVIMPORT = "nEOCostoServimport";
    public static final String PROPERTY_NEOCOSTOTOTALIMPORT = "nEOCostoTotalimport";
    public static final String PROPERTY_NEOPRECIOTOTAL = "nEOPreciototal";
    public static final String PROPERTY_NEOPRECIOUNIT = "nEOPreciounit";
    public static final String PROPERTY_NEOVALIDADO = "nEOValidado";
    public static final String PROPERTY_NEOISLOCAL = "nEOIslocal";
    public static final String PROPERTY_NEOISLICENCIA = "nEOIslicencia";

    public neoLineprod() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_NEOCANTIDAD, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOUNITFOB, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOTOTALFOB, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPESOBRUTOUNIT, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPESOBRUTOTOTAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOANCHO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOALTO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPROFUNDIDAD, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPESOVOL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPESOCALCULO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOINTETCPOR, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOINTETC, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOSALIDACAPPOR, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOSALIDACAP, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOFLETE, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOSEGURO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOFODIN, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOARANCELPOR, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOARANCEL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEORETENCIONESPOR, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEORETENCIONES, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOMARGENVENTAPORC, new BigDecimal(30));
        setDefaultValue(PROPERTY_NEOMARGENVENTA, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOCOSTOFIN, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPMUNITARIORENTA, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPLAZO, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOCIF, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEONACIONALIZACION, new BigDecimal(59));
        setDefaultValue(PROPERTY_NEOSEGUROADUANA, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOCOSTOADUANA, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOTRANSPLOCAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOSEGUROLOCAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOCOSTOSERVIMPORT, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOCOSTOTOTALIMPORT, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPRECIOTOTAL, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOPRECIOUNIT, new BigDecimal(0));
        setDefaultValue(PROPERTY_NEOVALIDADO, false);
        setDefaultValue(PROPERTY_NEOISLOCAL, false);
        setDefaultValue(PROPERTY_NEOISLICENCIA, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public OrderLine getSalesOrderLine() {
        return (OrderLine) get(PROPERTY_SALESORDERLINE);
    }

    public void setSalesOrderLine(OrderLine salesOrderLine) {
        set(PROPERTY_SALESORDERLINE, salesOrderLine);
    }

    public BigDecimal getNEOCantidad() {
        return (BigDecimal) get(PROPERTY_NEOCANTIDAD);
    }

    public void setNEOCantidad(BigDecimal nEOCantidad) {
        set(PROPERTY_NEOCANTIDAD, nEOCantidad);
    }

    public BigDecimal getNEOUnitFob() {
        return (BigDecimal) get(PROPERTY_NEOUNITFOB);
    }

    public void setNEOUnitFob(BigDecimal nEOUnitFob) {
        set(PROPERTY_NEOUNITFOB, nEOUnitFob);
    }

    public BigDecimal getNEOTotalFob() {
        return (BigDecimal) get(PROPERTY_NEOTOTALFOB);
    }

    public void setNEOTotalFob(BigDecimal nEOTotalFob) {
        set(PROPERTY_NEOTOTALFOB, nEOTotalFob);
    }

    public BigDecimal getNEOPesobrutoUnit() {
        return (BigDecimal) get(PROPERTY_NEOPESOBRUTOUNIT);
    }

    public void setNEOPesobrutoUnit(BigDecimal nEOPesobrutoUnit) {
        set(PROPERTY_NEOPESOBRUTOUNIT, nEOPesobrutoUnit);
    }

    public BigDecimal getNEOPesobrutoTotal() {
        return (BigDecimal) get(PROPERTY_NEOPESOBRUTOTOTAL);
    }

    public void setNEOPesobrutoTotal(BigDecimal nEOPesobrutoTotal) {
        set(PROPERTY_NEOPESOBRUTOTOTAL, nEOPesobrutoTotal);
    }

    public BigDecimal getNEOAncho() {
        return (BigDecimal) get(PROPERTY_NEOANCHO);
    }

    public void setNEOAncho(BigDecimal nEOAncho) {
        set(PROPERTY_NEOANCHO, nEOAncho);
    }

    public BigDecimal getNEOAlto() {
        return (BigDecimal) get(PROPERTY_NEOALTO);
    }

    public void setNEOAlto(BigDecimal nEOAlto) {
        set(PROPERTY_NEOALTO, nEOAlto);
    }

    public BigDecimal getNEOProfundidad() {
        return (BigDecimal) get(PROPERTY_NEOPROFUNDIDAD);
    }

    public void setNEOProfundidad(BigDecimal nEOProfundidad) {
        set(PROPERTY_NEOPROFUNDIDAD, nEOProfundidad);
    }

    public BigDecimal getNEOPesoVol() {
        return (BigDecimal) get(PROPERTY_NEOPESOVOL);
    }

    public void setNEOPesoVol(BigDecimal nEOPesoVol) {
        set(PROPERTY_NEOPESOVOL, nEOPesoVol);
    }

    public BigDecimal getNEOPesoCalculo() {
        return (BigDecimal) get(PROPERTY_NEOPESOCALCULO);
    }

    public void setNEOPesoCalculo(BigDecimal nEOPesoCalculo) {
        set(PROPERTY_NEOPESOCALCULO, nEOPesoCalculo);
    }

    public BigDecimal getNEOInteTcPor() {
        return (BigDecimal) get(PROPERTY_NEOINTETCPOR);
    }

    public void setNEOInteTcPor(BigDecimal nEOInteTcPor) {
        set(PROPERTY_NEOINTETCPOR, nEOInteTcPor);
    }

    public BigDecimal getNEOInteTc() {
        return (BigDecimal) get(PROPERTY_NEOINTETC);
    }

    public void setNEOInteTc(BigDecimal nEOInteTc) {
        set(PROPERTY_NEOINTETC, nEOInteTc);
    }

    public BigDecimal getNEOSalidaCapPor() {
        return (BigDecimal) get(PROPERTY_NEOSALIDACAPPOR);
    }

    public void setNEOSalidaCapPor(BigDecimal nEOSalidaCapPor) {
        set(PROPERTY_NEOSALIDACAPPOR, nEOSalidaCapPor);
    }

    public BigDecimal getNEOSalidaCap() {
        return (BigDecimal) get(PROPERTY_NEOSALIDACAP);
    }

    public void setNEOSalidaCap(BigDecimal nEOSalidaCap) {
        set(PROPERTY_NEOSALIDACAP, nEOSalidaCap);
    }

    public BigDecimal getNEOFlete() {
        return (BigDecimal) get(PROPERTY_NEOFLETE);
    }

    public void setNEOFlete(BigDecimal nEOFlete) {
        set(PROPERTY_NEOFLETE, nEOFlete);
    }

    public BigDecimal getNEOSeguro() {
        return (BigDecimal) get(PROPERTY_NEOSEGURO);
    }

    public void setNEOSeguro(BigDecimal nEOSeguro) {
        set(PROPERTY_NEOSEGURO, nEOSeguro);
    }

    public BigDecimal getNEOFodin() {
        return (BigDecimal) get(PROPERTY_NEOFODIN);
    }

    public void setNEOFodin(BigDecimal nEOFodin) {
        set(PROPERTY_NEOFODIN, nEOFodin);
    }

    public BigDecimal getNEOArancelPor() {
        return (BigDecimal) get(PROPERTY_NEOARANCELPOR);
    }

    public void setNEOArancelPor(BigDecimal nEOArancelPor) {
        set(PROPERTY_NEOARANCELPOR, nEOArancelPor);
    }

    public BigDecimal getNEOArancel() {
        return (BigDecimal) get(PROPERTY_NEOARANCEL);
    }

    public void setNEOArancel(BigDecimal nEOArancel) {
        set(PROPERTY_NEOARANCEL, nEOArancel);
    }

    public BigDecimal getNEORetencionesPor() {
        return (BigDecimal) get(PROPERTY_NEORETENCIONESPOR);
    }

    public void setNEORetencionesPor(BigDecimal nEORetencionesPor) {
        set(PROPERTY_NEORETENCIONESPOR, nEORetencionesPor);
    }

    public BigDecimal getNEORetenciones() {
        return (BigDecimal) get(PROPERTY_NEORETENCIONES);
    }

    public void setNEORetenciones(BigDecimal nEORetenciones) {
        set(PROPERTY_NEORETENCIONES, nEORetenciones);
    }

    public BigDecimal getNEOMargenVentaporc() {
        return (BigDecimal) get(PROPERTY_NEOMARGENVENTAPORC);
    }

    public void setNEOMargenVentaporc(BigDecimal nEOMargenVentaporc) {
        set(PROPERTY_NEOMARGENVENTAPORC, nEOMargenVentaporc);
    }

    public BigDecimal getNEOMargenVenta() {
        return (BigDecimal) get(PROPERTY_NEOMARGENVENTA);
    }

    public void setNEOMargenVenta(BigDecimal nEOMargenVenta) {
        set(PROPERTY_NEOMARGENVENTA, nEOMargenVenta);
    }

    public BigDecimal getNEOCostoFin() {
        return (BigDecimal) get(PROPERTY_NEOCOSTOFIN);
    }

    public void setNEOCostoFin(BigDecimal nEOCostoFin) {
        set(PROPERTY_NEOCOSTOFIN, nEOCostoFin);
    }

    public BigDecimal getNEOPmunitarioRenta() {
        return (BigDecimal) get(PROPERTY_NEOPMUNITARIORENTA);
    }

    public void setNEOPmunitarioRenta(BigDecimal nEOPmunitarioRenta) {
        set(PROPERTY_NEOPMUNITARIORENTA, nEOPmunitarioRenta);
    }

    public BigDecimal getNEOPlazo() {
        return (BigDecimal) get(PROPERTY_NEOPLAZO);
    }

    public void setNEOPlazo(BigDecimal nEOPlazo) {
        set(PROPERTY_NEOPLAZO, nEOPlazo);
    }

    public BigDecimal getNEOCif() {
        return (BigDecimal) get(PROPERTY_NEOCIF);
    }

    public void setNEOCif(BigDecimal nEOCif) {
        set(PROPERTY_NEOCIF, nEOCif);
    }

    public BigDecimal getNEONacionalizacion() {
        return (BigDecimal) get(PROPERTY_NEONACIONALIZACION);
    }

    public void setNEONacionalizacion(BigDecimal nEONacionalizacion) {
        set(PROPERTY_NEONACIONALIZACION, nEONacionalizacion);
    }

    public BigDecimal getNEOSeguroAduana() {
        return (BigDecimal) get(PROPERTY_NEOSEGUROADUANA);
    }

    public void setNEOSeguroAduana(BigDecimal nEOSeguroAduana) {
        set(PROPERTY_NEOSEGUROADUANA, nEOSeguroAduana);
    }

    public BigDecimal getNEOCostoAduana() {
        return (BigDecimal) get(PROPERTY_NEOCOSTOADUANA);
    }

    public void setNEOCostoAduana(BigDecimal nEOCostoAduana) {
        set(PROPERTY_NEOCOSTOADUANA, nEOCostoAduana);
    }

    public BigDecimal getNEOTranspLocal() {
        return (BigDecimal) get(PROPERTY_NEOTRANSPLOCAL);
    }

    public void setNEOTranspLocal(BigDecimal nEOTranspLocal) {
        set(PROPERTY_NEOTRANSPLOCAL, nEOTranspLocal);
    }

    public BigDecimal getNEOSeguroLocal() {
        return (BigDecimal) get(PROPERTY_NEOSEGUROLOCAL);
    }

    public void setNEOSeguroLocal(BigDecimal nEOSeguroLocal) {
        set(PROPERTY_NEOSEGUROLOCAL, nEOSeguroLocal);
    }

    public BigDecimal getNEOCostoServimport() {
        return (BigDecimal) get(PROPERTY_NEOCOSTOSERVIMPORT);
    }

    public void setNEOCostoServimport(BigDecimal nEOCostoServimport) {
        set(PROPERTY_NEOCOSTOSERVIMPORT, nEOCostoServimport);
    }

    public BigDecimal getNEOCostoTotalimport() {
        return (BigDecimal) get(PROPERTY_NEOCOSTOTOTALIMPORT);
    }

    public void setNEOCostoTotalimport(BigDecimal nEOCostoTotalimport) {
        set(PROPERTY_NEOCOSTOTOTALIMPORT, nEOCostoTotalimport);
    }

    public BigDecimal getNEOPreciototal() {
        return (BigDecimal) get(PROPERTY_NEOPRECIOTOTAL);
    }

    public void setNEOPreciototal(BigDecimal nEOPreciototal) {
        set(PROPERTY_NEOPRECIOTOTAL, nEOPreciototal);
    }

    public BigDecimal getNEOPreciounit() {
        return (BigDecimal) get(PROPERTY_NEOPRECIOUNIT);
    }

    public void setNEOPreciounit(BigDecimal nEOPreciounit) {
        set(PROPERTY_NEOPRECIOUNIT, nEOPreciounit);
    }

    public Boolean isNEOValidado() {
        return (Boolean) get(PROPERTY_NEOVALIDADO);
    }

    public void setNEOValidado(Boolean nEOValidado) {
        set(PROPERTY_NEOVALIDADO, nEOValidado);
    }

    public Boolean isNEOIslocal() {
        return (Boolean) get(PROPERTY_NEOISLOCAL);
    }

    public void setNEOIslocal(Boolean nEOIslocal) {
        set(PROPERTY_NEOISLOCAL, nEOIslocal);
    }

    public Boolean isNEOIslicencia() {
        return (Boolean) get(PROPERTY_NEOISLICENCIA);
    }

    public void setNEOIslicencia(Boolean nEOIslicencia) {
        set(PROPERTY_NEOISLICENCIA, nEOIslicencia);
    }

}
