/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.neomedia.data;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity neo_casoline (stored in table neo_casoline).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class neoCasoline extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "neo_casoline";
    public static final String ENTITY_NAME = "neo_casoline";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LINENO = "lineNo";
    public static final String PROPERTY_NEOCASO = "nEOCaso";
    public static final String PROPERTY_CBPTECNICO = "cBPTecnico";
    public static final String PROPERTY_TIEMPOEJECUTADO = "tiempoEjecutado";
    public static final String PROPERTY_COMENTARIOS = "comentarios";
    public static final String PROPERTY_COSTOS = "costos";
    public static final String PROPERTY_ISFACTURADO = "isfacturado";
    public static final String PROPERTY_COMENTARIOTEC = "comentariotec";
    public static final String PROPERTY_ISEJECUTADO = "isejecutado";
    public static final String PROPERTY_FINICIO = "finicio";
    public static final String PROPERTY_FFIN = "ffin";
    public static final String PROPERTY_ESCALAMIENTO = "escalamiento";
    public static final String PROPERTY_MPREVENTIVO = "mpreventivo";
    public static final String PROPERTY_MCORRECTIVO = "mcorrectivo";
    public static final String PROPERTY_INSTALACION = "instalacion";
    public static final String PROPERTY_DESINSTALACION = "desinstalacion";
    public static final String PROPERTY_INSPECCION = "inspeccion";
    public static final String PROPERTY_FOTOGRAFIA = "fotografia";
    public static final String PROPERTY_ENCUESTA = "encuesta";
    public static final String PROPERTY_DEMO = "demo";
    public static final String PROPERTY_FECHALINEACASO = "fechaLineacaso";
    public static final String PROPERTY_ESTADOLINEA = "estadolinea";
    public static final String PROPERTY_PROCESARINI = "procesarini";
    public static final String PROPERTY_PROCESARFIN = "procesarfin";

    public neoCasoline() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_ISFACTURADO, false);
        setDefaultValue(PROPERTY_ISEJECUTADO, false);
        setDefaultValue(PROPERTY_MPREVENTIVO, false);
        setDefaultValue(PROPERTY_MCORRECTIVO, false);
        setDefaultValue(PROPERTY_INSTALACION, false);
        setDefaultValue(PROPERTY_DESINSTALACION, false);
        setDefaultValue(PROPERTY_INSPECCION, false);
        setDefaultValue(PROPERTY_FOTOGRAFIA, false);
        setDefaultValue(PROPERTY_ENCUESTA, false);
        setDefaultValue(PROPERTY_DEMO, false);
        setDefaultValue(PROPERTY_ESTADOLINEA, "BOR");
        setDefaultValue(PROPERTY_PROCESARINI, false);
        setDefaultValue(PROPERTY_PROCESARFIN, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Long getLineNo() {
        return (Long) get(PROPERTY_LINENO);
    }

    public void setLineNo(Long lineNo) {
        set(PROPERTY_LINENO, lineNo);
    }

    public neoCaso getNEOCaso() {
        return (neoCaso) get(PROPERTY_NEOCASO);
    }

    public void setNEOCaso(neoCaso nEOCaso) {
        set(PROPERTY_NEOCASO, nEOCaso);
    }

    public BusinessPartner getCBPTecnico() {
        return (BusinessPartner) get(PROPERTY_CBPTECNICO);
    }

    public void setCBPTecnico(BusinessPartner cBPTecnico) {
        set(PROPERTY_CBPTECNICO, cBPTecnico);
    }

    public BigDecimal getTiempoEjecutado() {
        return (BigDecimal) get(PROPERTY_TIEMPOEJECUTADO);
    }

    public void setTiempoEjecutado(BigDecimal tiempoEjecutado) {
        set(PROPERTY_TIEMPOEJECUTADO, tiempoEjecutado);
    }

    public String getComentarios() {
        return (String) get(PROPERTY_COMENTARIOS);
    }

    public void setComentarios(String comentarios) {
        set(PROPERTY_COMENTARIOS, comentarios);
    }

    public BigDecimal getCostos() {
        return (BigDecimal) get(PROPERTY_COSTOS);
    }

    public void setCostos(BigDecimal costos) {
        set(PROPERTY_COSTOS, costos);
    }

    public Boolean isFacturado() {
        return (Boolean) get(PROPERTY_ISFACTURADO);
    }

    public void setFacturado(Boolean isfacturado) {
        set(PROPERTY_ISFACTURADO, isfacturado);
    }

    public String getComentariotec() {
        return (String) get(PROPERTY_COMENTARIOTEC);
    }

    public void setComentariotec(String comentariotec) {
        set(PROPERTY_COMENTARIOTEC, comentariotec);
    }

    public Boolean isEjecutado() {
        return (Boolean) get(PROPERTY_ISEJECUTADO);
    }

    public void setEjecutado(Boolean isejecutado) {
        set(PROPERTY_ISEJECUTADO, isejecutado);
    }

    public Date getFinicio() {
        return (Date) get(PROPERTY_FINICIO);
    }

    public void setFinicio(Date finicio) {
        set(PROPERTY_FINICIO, finicio);
    }

    public Date getFfin() {
        return (Date) get(PROPERTY_FFIN);
    }

    public void setFfin(Date ffin) {
        set(PROPERTY_FFIN, ffin);
    }

    public String getEscalamiento() {
        return (String) get(PROPERTY_ESCALAMIENTO);
    }

    public void setEscalamiento(String escalamiento) {
        set(PROPERTY_ESCALAMIENTO, escalamiento);
    }

    public Boolean isMpreventivo() {
        return (Boolean) get(PROPERTY_MPREVENTIVO);
    }

    public void setMpreventivo(Boolean mpreventivo) {
        set(PROPERTY_MPREVENTIVO, mpreventivo);
    }

    public Boolean isMcorrectivo() {
        return (Boolean) get(PROPERTY_MCORRECTIVO);
    }

    public void setMcorrectivo(Boolean mcorrectivo) {
        set(PROPERTY_MCORRECTIVO, mcorrectivo);
    }

    public Boolean isInstalacion() {
        return (Boolean) get(PROPERTY_INSTALACION);
    }

    public void setInstalacion(Boolean instalacion) {
        set(PROPERTY_INSTALACION, instalacion);
    }

    public Boolean isDesinstalacion() {
        return (Boolean) get(PROPERTY_DESINSTALACION);
    }

    public void setDesinstalacion(Boolean desinstalacion) {
        set(PROPERTY_DESINSTALACION, desinstalacion);
    }

    public Boolean isInspeccion() {
        return (Boolean) get(PROPERTY_INSPECCION);
    }

    public void setInspeccion(Boolean inspeccion) {
        set(PROPERTY_INSPECCION, inspeccion);
    }

    public Boolean isFotografia() {
        return (Boolean) get(PROPERTY_FOTOGRAFIA);
    }

    public void setFotografia(Boolean fotografia) {
        set(PROPERTY_FOTOGRAFIA, fotografia);
    }

    public Boolean isEncuesta() {
        return (Boolean) get(PROPERTY_ENCUESTA);
    }

    public void setEncuesta(Boolean encuesta) {
        set(PROPERTY_ENCUESTA, encuesta);
    }

    public Boolean isDemo() {
        return (Boolean) get(PROPERTY_DEMO);
    }

    public void setDemo(Boolean demo) {
        set(PROPERTY_DEMO, demo);
    }

    public Date getFechaLineacaso() {
        return (Date) get(PROPERTY_FECHALINEACASO);
    }

    public void setFechaLineacaso(Date fechaLineacaso) {
        set(PROPERTY_FECHALINEACASO, fechaLineacaso);
    }

    public String getEstadolinea() {
        return (String) get(PROPERTY_ESTADOLINEA);
    }

    public void setEstadolinea(String estadolinea) {
        set(PROPERTY_ESTADOLINEA, estadolinea);
    }

    public Boolean isProcesarini() {
        return (Boolean) get(PROPERTY_PROCESARINI);
    }

    public void setProcesarini(Boolean procesarini) {
        set(PROPERTY_PROCESARINI, procesarini);
    }

    public Boolean isProcesarfin() {
        return (Boolean) get(PROPERTY_PROCESARFIN);
    }

    public void setProcesarfin(Boolean procesarfin) {
        set(PROPERTY_PROCESARFIN, procesarfin);
    }

}
