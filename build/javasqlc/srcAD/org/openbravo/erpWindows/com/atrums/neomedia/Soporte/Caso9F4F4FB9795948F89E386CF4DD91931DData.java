//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Soporte;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Caso9F4F4FB9795948F89E386CF4DD91931DData implements FieldProvider {
static Logger log4j = Logger.getLogger(Caso9F4F4FB9795948F89E386CF4DD91931DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String docstatus;
  public String docstatusr;
  public String cbpClienteId;
  public String canaltercero;
  public String agenciacanal;
  public String neoZonaId;
  public String nombrecaso;
  public String cbpEjecutivoId;
  public String tipocaso;
  public String tipocasor;
  public String neoAreaEmpresaId;
  public String tiemposumatoria;
  public String isactive;
  public String procesar;
  public String fInicio;
  public String fFinal;
  public String ciudad;
  public String agencia;
  public String prioridad;
  public String prioridadr;
  public String origen;
  public String origenr;
  public String descripcioncaso;
  public String direccioncli;
  public String contantocli;
  public String isenvioemail;
  public String estadocorreo;
  public String adClientId;
  public String neoCasoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("cbp_cliente_id") || fieldName.equals("cbpClienteId"))
      return cbpClienteId;
    else if (fieldName.equalsIgnoreCase("canaltercero"))
      return canaltercero;
    else if (fieldName.equalsIgnoreCase("agenciacanal"))
      return agenciacanal;
    else if (fieldName.equalsIgnoreCase("neo_zona_id") || fieldName.equals("neoZonaId"))
      return neoZonaId;
    else if (fieldName.equalsIgnoreCase("nombrecaso"))
      return nombrecaso;
    else if (fieldName.equalsIgnoreCase("cbp_ejecutivo_id") || fieldName.equals("cbpEjecutivoId"))
      return cbpEjecutivoId;
    else if (fieldName.equalsIgnoreCase("tipocaso"))
      return tipocaso;
    else if (fieldName.equalsIgnoreCase("tipocasor"))
      return tipocasor;
    else if (fieldName.equalsIgnoreCase("neo_area_empresa_id") || fieldName.equals("neoAreaEmpresaId"))
      return neoAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("tiemposumatoria"))
      return tiemposumatoria;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("f_inicio") || fieldName.equals("fInicio"))
      return fInicio;
    else if (fieldName.equalsIgnoreCase("f_final") || fieldName.equals("fFinal"))
      return fFinal;
    else if (fieldName.equalsIgnoreCase("ciudad"))
      return ciudad;
    else if (fieldName.equalsIgnoreCase("agencia"))
      return agencia;
    else if (fieldName.equalsIgnoreCase("prioridad"))
      return prioridad;
    else if (fieldName.equalsIgnoreCase("prioridadr"))
      return prioridadr;
    else if (fieldName.equalsIgnoreCase("origen"))
      return origen;
    else if (fieldName.equalsIgnoreCase("origenr"))
      return origenr;
    else if (fieldName.equalsIgnoreCase("descripcioncaso"))
      return descripcioncaso;
    else if (fieldName.equalsIgnoreCase("direccioncli"))
      return direccioncli;
    else if (fieldName.equalsIgnoreCase("contantocli"))
      return contantocli;
    else if (fieldName.equalsIgnoreCase("isenvioemail"))
      return isenvioemail;
    else if (fieldName.equalsIgnoreCase("estadocorreo"))
      return estadocorreo;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_caso_id") || fieldName.equals("neoCasoId"))
      return neoCasoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Caso9F4F4FB9795948F89E386CF4DD91931DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Caso9F4F4FB9795948F89E386CF4DD91931DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_caso.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_caso.CreatedBy) as CreatedByR, " +
      "        to_char(neo_caso.Updated, ?) as updated, " +
      "        to_char(neo_caso.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_caso.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_caso.UpdatedBy) as UpdatedByR," +
      "        neo_caso.AD_Org_ID, " +
      "(CASE WHEN neo_caso.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "neo_caso.C_Doctype_ID, " +
      "(CASE WHEN neo_caso.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "neo_caso.Documentno, " +
      "neo_caso.Docstatus, " +
      "(CASE WHEN neo_caso.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "neo_caso.CBP_Cliente_ID, " +
      "neo_caso.Canaltercero, " +
      "neo_caso.Agenciacanal, " +
      "neo_caso.NEO_Zona_ID, " +
      "neo_caso.Nombrecaso, " +
      "neo_caso.CBP_Ejecutivo_ID, " +
      "neo_caso.Tipocaso, " +
      "(CASE WHEN neo_caso.Tipocaso IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS TipocasoR, " +
      "neo_caso.NEO_Area_Empresa_ID, " +
      "neo_caso.Tiemposumatoria, " +
      "COALESCE(neo_caso.Isactive, 'N') AS Isactive, " +
      "neo_caso.Procesar, " +
      "TO_CHAR(neo_caso.F_Inicio, ?) AS F_Inicio, " +
      "TO_CHAR(neo_caso.F_Final, ?) AS F_Final, " +
      "neo_caso.Ciudad, " +
      "neo_caso.Agencia, " +
      "neo_caso.Prioridad, " +
      "(CASE WHEN neo_caso.Prioridad IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS PrioridadR, " +
      "neo_caso.Origen, " +
      "(CASE WHEN neo_caso.Origen IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list4.name),'') ) END) AS OrigenR, " +
      "neo_caso.Descripcioncaso, " +
      "neo_caso.Direccioncli, " +
      "neo_caso.Contantocli, " +
      "COALESCE(neo_caso.Isenvioemail, 'N') AS Isenvioemail, " +
      "neo_caso.Estadocorreo, " +
      "neo_caso.AD_Client_ID, " +
      "neo_caso.NEO_Caso_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_caso left join (select AD_Org_ID, Name from AD_Org) table1 on (neo_caso.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (neo_caso.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join ad_ref_list_v list1 on (neo_caso.Docstatus = list1.value and list1.ad_reference_id = 'DC48B7CE25024029AB039B17EFE73376' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (neo_caso.Tipocaso = list2.value and list2.ad_reference_id = '6DEBB599BFDC428F944EF87D09FC6886' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (neo_caso.Prioridad = list3.value and list3.ad_reference_id = '584D5F45D7D14C61A5083FB2AFE01337' and list3.ad_language = ?)  left join ad_ref_list_v list4 on (neo_caso.Origen = list4.value and list4.ad_reference_id = '790DB9877BFC4CF296A4E3AC11B98F12' and list4.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_caso.NEO_Caso_ID = ? " +
      "        AND neo_caso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_caso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Caso9F4F4FB9795948F89E386CF4DD91931DData objectCaso9F4F4FB9795948F89E386CF4DD91931DData = new Caso9F4F4FB9795948F89E386CF4DD91931DData();
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.created = UtilSql.getValue(result, "created");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.updated = UtilSql.getValue(result, "updated");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.documentno = UtilSql.getValue(result, "documentno");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.docstatus = UtilSql.getValue(result, "docstatus");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.cbpClienteId = UtilSql.getValue(result, "cbp_cliente_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.canaltercero = UtilSql.getValue(result, "canaltercero");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.agenciacanal = UtilSql.getValue(result, "agenciacanal");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.neoZonaId = UtilSql.getValue(result, "neo_zona_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.nombrecaso = UtilSql.getValue(result, "nombrecaso");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.cbpEjecutivoId = UtilSql.getValue(result, "cbp_ejecutivo_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.tipocaso = UtilSql.getValue(result, "tipocaso");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.tipocasor = UtilSql.getValue(result, "tipocasor");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.neoAreaEmpresaId = UtilSql.getValue(result, "neo_area_empresa_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.tiemposumatoria = UtilSql.getValue(result, "tiemposumatoria");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.isactive = UtilSql.getValue(result, "isactive");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.procesar = UtilSql.getValue(result, "procesar");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.fInicio = UtilSql.getValue(result, "f_inicio");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.fFinal = UtilSql.getValue(result, "f_final");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.ciudad = UtilSql.getValue(result, "ciudad");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.agencia = UtilSql.getValue(result, "agencia");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.prioridad = UtilSql.getValue(result, "prioridad");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.prioridadr = UtilSql.getValue(result, "prioridadr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.origen = UtilSql.getValue(result, "origen");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.origenr = UtilSql.getValue(result, "origenr");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.descripcioncaso = UtilSql.getValue(result, "descripcioncaso");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.direccioncli = UtilSql.getValue(result, "direccioncli");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.contantocli = UtilSql.getValue(result, "contantocli");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.isenvioemail = UtilSql.getValue(result, "isenvioemail");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.estadocorreo = UtilSql.getValue(result, "estadocorreo");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.neoCasoId = UtilSql.getValue(result, "neo_caso_id");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.language = UtilSql.getValue(result, "language");
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.adUserClient = "";
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.adOrgClient = "";
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.createdby = "";
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.trBgcolor = "";
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.totalCount = "";
        objectCaso9F4F4FB9795948F89E386CF4DD91931DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCaso9F4F4FB9795948F89E386CF4DD91931DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Caso9F4F4FB9795948F89E386CF4DD91931DData objectCaso9F4F4FB9795948F89E386CF4DD91931DData[] = new Caso9F4F4FB9795948F89E386CF4DD91931DData[vector.size()];
    vector.copyInto(objectCaso9F4F4FB9795948F89E386CF4DD91931DData);
    return(objectCaso9F4F4FB9795948F89E386CF4DD91931DData);
  }

/**
Create a registry
 */
  public static Caso9F4F4FB9795948F89E386CF4DD91931DData[] set(String neoCasoId, String cbpEjecutivoId, String cbpClienteId, String prioridad, String canaltercero, String neoAreaEmpresaId, String adOrgId, String fFinal, String adClientId, String procesar, String contantocli, String isenvioemail, String fInicio, String isactive, String tiemposumatoria, String nombrecaso, String tipocaso, String neoZonaId, String updatedby, String updatedbyr, String agenciacanal, String docstatus, String estadocorreo, String createdby, String createdbyr, String direccioncli, String ciudad, String agencia, String origen, String cDoctypeId, String documentno, String descripcioncaso)    throws ServletException {
    Caso9F4F4FB9795948F89E386CF4DD91931DData objectCaso9F4F4FB9795948F89E386CF4DD91931DData[] = new Caso9F4F4FB9795948F89E386CF4DD91931DData[1];
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0] = new Caso9F4F4FB9795948F89E386CF4DD91931DData();
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].created = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].createdbyr = createdbyr;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].updated = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].updatedTimeStamp = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].updatedby = updatedby;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].updatedbyr = updatedbyr;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].adOrgId = adOrgId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].adOrgIdr = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].cDoctypeId = cDoctypeId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].cDoctypeIdr = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].documentno = documentno;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].docstatus = docstatus;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].docstatusr = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].cbpClienteId = cbpClienteId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].canaltercero = canaltercero;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].agenciacanal = agenciacanal;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].neoZonaId = neoZonaId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].nombrecaso = nombrecaso;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].cbpEjecutivoId = cbpEjecutivoId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].tipocaso = tipocaso;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].tipocasor = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].neoAreaEmpresaId = neoAreaEmpresaId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].tiemposumatoria = tiemposumatoria;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].isactive = isactive;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].procesar = procesar;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].fInicio = fInicio;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].fFinal = fFinal;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].ciudad = ciudad;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].agencia = agencia;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].prioridad = prioridad;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].prioridadr = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].origen = origen;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].origenr = "";
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].descripcioncaso = descripcioncaso;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].direccioncli = direccioncli;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].contantocli = contantocli;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].isenvioemail = isenvioemail;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].estadocorreo = estadocorreo;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].adClientId = adClientId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].neoCasoId = neoCasoId;
    objectCaso9F4F4FB9795948F89E386CF4DD91931DData[0].language = "";
    return objectCaso9F4F4FB9795948F89E386CF4DD91931DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefA3608585B5404DB8AE6FA95265AB3E67_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB792866C5DE4443582DF4FA1F2BDACFD_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_caso" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , Docstatus = (?) , CBP_Cliente_ID = (?) , Canaltercero = (?) , Agenciacanal = (?) , NEO_Zona_ID = (?) , Nombrecaso = (?) , CBP_Ejecutivo_ID = (?) , Tipocaso = (?) , NEO_Area_Empresa_ID = (?) , Tiemposumatoria = TO_NUMBER(?) , Isactive = (?) , Procesar = (?) , F_Inicio = TO_TIMESTAMP(?, ?) , F_Final = TO_TIMESTAMP(?, ?) , Ciudad = (?) , Agencia = (?) , Prioridad = (?) , Origen = (?) , Descripcioncaso = (?) , Direccioncli = (?) , Contantocli = (?) , Isenvioemail = (?) , Estadocorreo = (?) , AD_Client_ID = (?) , NEO_Caso_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_caso.NEO_Caso_ID = ? " +
      "        AND neo_caso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_caso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, canaltercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenciacanal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoZonaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombrecaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpEjecutivoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipocaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiemposumatoria);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFinal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ciudad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prioridad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, origen);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcioncaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, direccioncli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, contantocli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isenvioemail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadocorreo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_caso " +
      "        (AD_Org_ID, C_Doctype_ID, Documentno, Docstatus, CBP_Cliente_ID, Canaltercero, Agenciacanal, NEO_Zona_ID, Nombrecaso, CBP_Ejecutivo_ID, Tipocaso, NEO_Area_Empresa_ID, Tiemposumatoria, Isactive, Procesar, F_Inicio, F_Final, Ciudad, Agencia, Prioridad, Origen, Descripcioncaso, Direccioncli, Contantocli, Isenvioemail, Estadocorreo, AD_Client_ID, NEO_Caso_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_TIMESTAMP(?, ?), TO_TIMESTAMP(?, ?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, canaltercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agenciacanal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoZonaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombrecaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpEjecutivoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipocaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiemposumatoria);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFinal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ciudad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, agencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prioridad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, origen);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcioncaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, direccioncli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, contantocli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isenvioemail);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadocorreo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_caso" +
      "        WHERE neo_caso.NEO_Caso_ID = ? " +
      "        AND neo_caso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_caso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_caso" +
      "         WHERE neo_caso.NEO_Caso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_caso" +
      "         WHERE neo_caso.NEO_Caso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
