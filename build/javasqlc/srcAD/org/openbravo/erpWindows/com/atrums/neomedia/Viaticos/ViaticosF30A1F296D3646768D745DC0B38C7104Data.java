//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Viaticos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class ViaticosF30A1F296D3646768D745DC0B38C7104Data implements FieldProvider {
static Logger log4j = Logger.getLogger(ViaticosF30A1F296D3646768D745DC0B38C7104Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String neoCityId;
  public String cbpEjecutivoId;
  public String fechaViaje;
  public String fechaRetorno;
  public String viaticostatus;
  public String viaticostatusr;
  public String canthospedaje;
  public String valorhospedaje;
  public String totlahospedaje;
  public String cantmovilizacion;
  public String valormovilizacion;
  public String totalmovilizacion;
  public String cantmovilizacioninter;
  public String valormovilizacioninter;
  public String totalmovilizacioninter;
  public String cantmovilizaciontaxi;
  public String valormovilizaciontaxi;
  public String totalmovilizaciontaxi;
  public String cantalimentacion;
  public String valoralimentacion;
  public String totalalimentacion;
  public String descripcion;
  public String vtotal;
  public String procesar;
  public String neoCasoviaticoId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("neo_city_id") || fieldName.equals("neoCityId"))
      return neoCityId;
    else if (fieldName.equalsIgnoreCase("cbp_ejecutivo_id") || fieldName.equals("cbpEjecutivoId"))
      return cbpEjecutivoId;
    else if (fieldName.equalsIgnoreCase("fecha_viaje") || fieldName.equals("fechaViaje"))
      return fechaViaje;
    else if (fieldName.equalsIgnoreCase("fecha_retorno") || fieldName.equals("fechaRetorno"))
      return fechaRetorno;
    else if (fieldName.equalsIgnoreCase("viaticostatus"))
      return viaticostatus;
    else if (fieldName.equalsIgnoreCase("viaticostatusr"))
      return viaticostatusr;
    else if (fieldName.equalsIgnoreCase("canthospedaje"))
      return canthospedaje;
    else if (fieldName.equalsIgnoreCase("valorhospedaje"))
      return valorhospedaje;
    else if (fieldName.equalsIgnoreCase("totlahospedaje"))
      return totlahospedaje;
    else if (fieldName.equalsIgnoreCase("cantmovilizacion"))
      return cantmovilizacion;
    else if (fieldName.equalsIgnoreCase("valormovilizacion"))
      return valormovilizacion;
    else if (fieldName.equalsIgnoreCase("totalmovilizacion"))
      return totalmovilizacion;
    else if (fieldName.equalsIgnoreCase("cantmovilizacioninter"))
      return cantmovilizacioninter;
    else if (fieldName.equalsIgnoreCase("valormovilizacioninter"))
      return valormovilizacioninter;
    else if (fieldName.equalsIgnoreCase("totalmovilizacioninter"))
      return totalmovilizacioninter;
    else if (fieldName.equalsIgnoreCase("cantmovilizaciontaxi"))
      return cantmovilizaciontaxi;
    else if (fieldName.equalsIgnoreCase("valormovilizaciontaxi"))
      return valormovilizaciontaxi;
    else if (fieldName.equalsIgnoreCase("totalmovilizaciontaxi"))
      return totalmovilizaciontaxi;
    else if (fieldName.equalsIgnoreCase("cantalimentacion"))
      return cantalimentacion;
    else if (fieldName.equalsIgnoreCase("valoralimentacion"))
      return valoralimentacion;
    else if (fieldName.equalsIgnoreCase("totalalimentacion"))
      return totalalimentacion;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("vtotal"))
      return vtotal;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("neo_casoviatico_id") || fieldName.equals("neoCasoviaticoId"))
      return neoCasoviaticoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ViaticosF30A1F296D3646768D745DC0B38C7104Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ViaticosF30A1F296D3646768D745DC0B38C7104Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_casoviatico.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_casoviatico.CreatedBy) as CreatedByR, " +
      "        to_char(neo_casoviatico.Updated, ?) as updated, " +
      "        to_char(neo_casoviatico.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_casoviatico.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_casoviatico.UpdatedBy) as UpdatedByR," +
      "        neo_casoviatico.AD_Org_ID, " +
      "(CASE WHEN neo_casoviatico.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(neo_casoviatico.Isactive, 'N') AS Isactive, " +
      "neo_casoviatico.NEO_City_ID, " +
      "neo_casoviatico.CBP_Ejecutivo_ID, " +
      "neo_casoviatico.Fecha_Viaje, " +
      "neo_casoviatico.Fecha_Retorno, " +
      "neo_casoviatico.Viaticostatus, " +
      "(CASE WHEN neo_casoviatico.Viaticostatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ViaticostatusR, " +
      "neo_casoviatico.Canthospedaje, " +
      "neo_casoviatico.Valorhospedaje, " +
      "neo_casoviatico.Totlahospedaje, " +
      "neo_casoviatico.Cantmovilizacion, " +
      "neo_casoviatico.Valormovilizacion, " +
      "neo_casoviatico.Totalmovilizacion, " +
      "neo_casoviatico.Cantmovilizacioninter, " +
      "neo_casoviatico.Valormovilizacioninter, " +
      "neo_casoviatico.Totalmovilizacioninter, " +
      "neo_casoviatico.Cantmovilizaciontaxi, " +
      "neo_casoviatico.Valormovilizaciontaxi, " +
      "neo_casoviatico.Totalmovilizaciontaxi, " +
      "neo_casoviatico.Cantalimentacion, " +
      "neo_casoviatico.Valoralimentacion, " +
      "neo_casoviatico.Totalalimentacion, " +
      "neo_casoviatico.Descripcion, " +
      "neo_casoviatico.Vtotal, " +
      "neo_casoviatico.Procesar, " +
      "neo_casoviatico.NEO_Casoviatico_ID, " +
      "neo_casoviatico.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_casoviatico left join (select AD_Org_ID, Name from AD_Org) table1 on (neo_casoviatico.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list1 on (neo_casoviatico.Viaticostatus = list1.value and list1.ad_reference_id = '2689922AF2A94EE7A4F535147884690D' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_casoviatico.NEO_Casoviatico_ID = ? " +
      "        AND neo_casoviatico.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_casoviatico.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ViaticosF30A1F296D3646768D745DC0B38C7104Data objectViaticosF30A1F296D3646768D745DC0B38C7104Data = new ViaticosF30A1F296D3646768D745DC0B38C7104Data();
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.created = UtilSql.getValue(result, "created");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.updated = UtilSql.getValue(result, "updated");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.isactive = UtilSql.getValue(result, "isactive");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.neoCityId = UtilSql.getValue(result, "neo_city_id");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.cbpEjecutivoId = UtilSql.getValue(result, "cbp_ejecutivo_id");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.fechaViaje = UtilSql.getDateValue(result, "fecha_viaje", "dd-MM-yyyy");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.fechaRetorno = UtilSql.getDateValue(result, "fecha_retorno", "dd-MM-yyyy");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.viaticostatus = UtilSql.getValue(result, "viaticostatus");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.viaticostatusr = UtilSql.getValue(result, "viaticostatusr");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.canthospedaje = UtilSql.getValue(result, "canthospedaje");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.valorhospedaje = UtilSql.getValue(result, "valorhospedaje");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totlahospedaje = UtilSql.getValue(result, "totlahospedaje");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.cantmovilizacion = UtilSql.getValue(result, "cantmovilizacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.valormovilizacion = UtilSql.getValue(result, "valormovilizacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totalmovilizacion = UtilSql.getValue(result, "totalmovilizacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.cantmovilizacioninter = UtilSql.getValue(result, "cantmovilizacioninter");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.valormovilizacioninter = UtilSql.getValue(result, "valormovilizacioninter");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totalmovilizacioninter = UtilSql.getValue(result, "totalmovilizacioninter");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.cantmovilizaciontaxi = UtilSql.getValue(result, "cantmovilizaciontaxi");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.valormovilizaciontaxi = UtilSql.getValue(result, "valormovilizaciontaxi");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totalmovilizaciontaxi = UtilSql.getValue(result, "totalmovilizaciontaxi");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.cantalimentacion = UtilSql.getValue(result, "cantalimentacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.valoralimentacion = UtilSql.getValue(result, "valoralimentacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totalalimentacion = UtilSql.getValue(result, "totalalimentacion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.descripcion = UtilSql.getValue(result, "descripcion");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.vtotal = UtilSql.getValue(result, "vtotal");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.procesar = UtilSql.getValue(result, "procesar");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.neoCasoviaticoId = UtilSql.getValue(result, "neo_casoviatico_id");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.language = UtilSql.getValue(result, "language");
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.adUserClient = "";
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.adOrgClient = "";
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.createdby = "";
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.trBgcolor = "";
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.totalCount = "";
        objectViaticosF30A1F296D3646768D745DC0B38C7104Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectViaticosF30A1F296D3646768D745DC0B38C7104Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ViaticosF30A1F296D3646768D745DC0B38C7104Data objectViaticosF30A1F296D3646768D745DC0B38C7104Data[] = new ViaticosF30A1F296D3646768D745DC0B38C7104Data[vector.size()];
    vector.copyInto(objectViaticosF30A1F296D3646768D745DC0B38C7104Data);
    return(objectViaticosF30A1F296D3646768D745DC0B38C7104Data);
  }

/**
Create a registry
 */
  public static ViaticosF30A1F296D3646768D745DC0B38C7104Data[] set(String canthospedaje, String cantmovilizaciontaxi, String totalmovilizaciontaxi, String valorhospedaje, String fechaViaje, String updatedby, String updatedbyr, String descripcion, String cantalimentacion, String valormovilizaciontaxi, String valoralimentacion, String valormovilizacion, String adClientId, String neoCityId, String totalmovilizacion, String totlahospedaje, String cbpEjecutivoId, String viaticostatus, String adOrgId, String isactive, String totalmovilizacioninter, String cantmovilizacioninter, String valormovilizacioninter, String totalalimentacion, String procesar, String createdby, String createdbyr, String neoCasoviaticoId, String cantmovilizacion, String fechaRetorno, String vtotal)    throws ServletException {
    ViaticosF30A1F296D3646768D745DC0B38C7104Data objectViaticosF30A1F296D3646768D745DC0B38C7104Data[] = new ViaticosF30A1F296D3646768D745DC0B38C7104Data[1];
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0] = new ViaticosF30A1F296D3646768D745DC0B38C7104Data();
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].created = "";
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].createdbyr = createdbyr;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].updated = "";
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].updatedTimeStamp = "";
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].updatedby = updatedby;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].updatedbyr = updatedbyr;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].adOrgId = adOrgId;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].adOrgIdr = "";
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].isactive = isactive;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].neoCityId = neoCityId;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].cbpEjecutivoId = cbpEjecutivoId;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].fechaViaje = fechaViaje;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].fechaRetorno = fechaRetorno;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].viaticostatus = viaticostatus;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].viaticostatusr = "";
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].canthospedaje = canthospedaje;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].valorhospedaje = valorhospedaje;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].totlahospedaje = totlahospedaje;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].cantmovilizacion = cantmovilizacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].valormovilizacion = valormovilizacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].totalmovilizacion = totalmovilizacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].cantmovilizacioninter = cantmovilizacioninter;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].valormovilizacioninter = valormovilizacioninter;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].totalmovilizacioninter = totalmovilizacioninter;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].cantmovilizaciontaxi = cantmovilizaciontaxi;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].valormovilizaciontaxi = valormovilizaciontaxi;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].totalmovilizaciontaxi = totalmovilizaciontaxi;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].cantalimentacion = cantalimentacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].valoralimentacion = valoralimentacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].totalalimentacion = totalalimentacion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].descripcion = descripcion;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].vtotal = vtotal;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].procesar = procesar;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].neoCasoviaticoId = neoCasoviaticoId;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].adClientId = adClientId;
    objectViaticosF30A1F296D3646768D745DC0B38C7104Data[0].language = "";
    return objectViaticosF30A1F296D3646768D745DC0B38C7104Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef18D51031A1C54B9DAC6BC0C4F4C7A603_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCA25A9E11B23467499BEB91451C0743F_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_casoviatico" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , NEO_City_ID = (?) , CBP_Ejecutivo_ID = (?) , Fecha_Viaje = TO_DATE(?) , Fecha_Retorno = TO_DATE(?) , Viaticostatus = (?) , Canthospedaje = TO_NUMBER(?) , Valorhospedaje = TO_NUMBER(?) , Totlahospedaje = TO_NUMBER(?) , Cantmovilizacion = TO_NUMBER(?) , Valormovilizacion = TO_NUMBER(?) , Totalmovilizacion = TO_NUMBER(?) , Cantmovilizacioninter = TO_NUMBER(?) , Valormovilizacioninter = TO_NUMBER(?) , Totalmovilizacioninter = TO_NUMBER(?) , Cantmovilizaciontaxi = TO_NUMBER(?) , Valormovilizaciontaxi = TO_NUMBER(?) , Totalmovilizaciontaxi = TO_NUMBER(?) , Cantalimentacion = TO_NUMBER(?) , Valoralimentacion = TO_NUMBER(?) , Totalalimentacion = TO_NUMBER(?) , Descripcion = (?) , Vtotal = TO_NUMBER(?) , Procesar = (?) , NEO_Casoviatico_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_casoviatico.NEO_Casoviatico_ID = ? " +
      "        AND neo_casoviatico.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_casoviatico.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpEjecutivoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaViaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaRetorno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, viaticostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, canthospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorhospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totlahospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantalimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valoralimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalalimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoviaticoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoviaticoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_casoviatico " +
      "        (AD_Org_ID, Isactive, NEO_City_ID, CBP_Ejecutivo_ID, Fecha_Viaje, Fecha_Retorno, Viaticostatus, Canthospedaje, Valorhospedaje, Totlahospedaje, Cantmovilizacion, Valormovilizacion, Totalmovilizacion, Cantmovilizacioninter, Valormovilizacioninter, Totalmovilizacioninter, Cantmovilizaciontaxi, Valormovilizaciontaxi, Totalmovilizaciontaxi, Cantalimentacion, Valoralimentacion, Totalalimentacion, Descripcion, Vtotal, Procesar, NEO_Casoviatico_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpEjecutivoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaViaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaRetorno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, viaticostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, canthospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorhospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totlahospedaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizacioninter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantmovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valormovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalmovilizaciontaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantalimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valoralimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalalimentacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoviaticoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_casoviatico" +
      "        WHERE neo_casoviatico.NEO_Casoviatico_ID = ? " +
      "        AND neo_casoviatico.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_casoviatico.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_casoviatico" +
      "         WHERE neo_casoviatico.NEO_Casoviatico_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_casoviatico" +
      "         WHERE neo_casoviatico.NEO_Casoviatico_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
