//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Cotizaciondeventas;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Header14F6810D5E014E80BC1DC729700C3A8DData implements FieldProvider {
static Logger log4j = Logger.getLogger(Header14F6810D5E014E80BC1DC729700C3A8DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypetargetId;
  public String cDoctypetargetIdr;
  public String cDoctypeId;
  public String documentno;
  public String cReturnReasonId;
  public String dateordered;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String rmPickfromshipment;
  public String emNeoSegComercialId;
  public String mPricelistId;
  public String mPricelistIdr;
  public String validuntil;
  public String rmReceivematerials;
  public String datepromised;
  public String rmCreateinvoice;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String cPaymenttermId;
  public String cPaymenttermIdr;
  public String mWarehouseId;
  public String cRejectReasonId;
  public String cRejectReasonIdr;
  public String invoicerule;
  public String salesrepId;
  public String salesrepIdr;
  public String description;
  public String emNeoDocstatusOrden;
  public String docstatus;
  public String grandtotal;
  public String totallines;
  public String cCurrencyId;
  public String adUserId;
  public String poreference;
  public String billtoId;
  public String deliveryLocationId;
  public String copyfrom;
  public String copyfrompo;
  public String docaction;
  public String docactionBtn;
  public String deliveryviarule;
  public String mShipperId;
  public String deliveryrule;
  public String freightcostrule;
  public String freightamt;
  public String isdiscountprinted;
  public String priorityrule;
  public String chargeamt;
  public String cChargeId;
  public String convertquotation;
  public String emNeoCreaOrdenCot;
  public String calculatePromotions;
  public String cActivityId;
  public String cProjectId;
  public String cProjectIdr;
  public String cCampaignId;
  public String adOrgtrxId;
  public String user1Id;
  public String user2Id;
  public String emNeoCreaProdNuevo;
  public String emNeoNuevosProd;
  public String emNeoProcessCotiza;
  public String emNeoGeneraordentrabajo;
  public String emNeoEspublicidad;
  public String emNeoDuplicaOrden;
  public String emNeoGenordenTrabajo;
  public String generatetemplate;
  public String isselfservice;
  public String dateacct;
  public String dropshipBpartnerId;
  public String isinvoiced;
  public String processed;
  public String issotrx;
  public String dropshipLocationId;
  public String istaxincluded;
  public String adClientId;
  public String dropshipUserId;
  public String cIncotermsId;
  public String isdelivered;
  public String incotermsdescription;
  public String isprinted;
  public String processing;
  public String deliverynotes;
  public String isactive;
  public String paymentrule;
  public String cOrderId;
  public String posted;
  public String dateprinted;
  public String isselected;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_idr") || fieldName.equals("cDoctypetargetIdr"))
      return cDoctypetargetIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("rm_pickfromshipment") || fieldName.equals("rmPickfromshipment"))
      return rmPickfromshipment;
    else if (fieldName.equalsIgnoreCase("em_neo_seg_comercial_id") || fieldName.equals("emNeoSegComercialId"))
      return emNeoSegComercialId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_idr") || fieldName.equals("mPricelistIdr"))
      return mPricelistIdr;
    else if (fieldName.equalsIgnoreCase("validuntil"))
      return validuntil;
    else if (fieldName.equalsIgnoreCase("rm_receivematerials") || fieldName.equals("rmReceivematerials"))
      return rmReceivematerials;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("rm_createinvoice") || fieldName.equals("rmCreateinvoice"))
      return rmCreateinvoice;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_idr") || fieldName.equals("cPaymenttermIdr"))
      return cPaymenttermIdr;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_id") || fieldName.equals("cRejectReasonId"))
      return cRejectReasonId;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_idr") || fieldName.equals("cRejectReasonIdr"))
      return cRejectReasonIdr;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("salesrep_idr") || fieldName.equals("salesrepIdr"))
      return salesrepIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("em_neo_docstatus_orden") || fieldName.equals("emNeoDocstatusOrden"))
      return emNeoDocstatusOrden;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("billto_id") || fieldName.equals("billtoId"))
      return billtoId;
    else if (fieldName.equalsIgnoreCase("delivery_location_id") || fieldName.equals("deliveryLocationId"))
      return deliveryLocationId;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("copyfrompo"))
      return copyfrompo;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("docaction_btn") || fieldName.equals("docactionBtn"))
      return docactionBtn;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("freightcostrule"))
      return freightcostrule;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("priorityrule"))
      return priorityrule;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("convertquotation"))
      return convertquotation;
    else if (fieldName.equalsIgnoreCase("em_neo_crea_orden_cot") || fieldName.equals("emNeoCreaOrdenCot"))
      return emNeoCreaOrdenCot;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("em_neo_crea_prod_nuevo") || fieldName.equals("emNeoCreaProdNuevo"))
      return emNeoCreaProdNuevo;
    else if (fieldName.equalsIgnoreCase("em_neo_nuevos_prod") || fieldName.equals("emNeoNuevosProd"))
      return emNeoNuevosProd;
    else if (fieldName.equalsIgnoreCase("em_neo_process_cotiza") || fieldName.equals("emNeoProcessCotiza"))
      return emNeoProcessCotiza;
    else if (fieldName.equalsIgnoreCase("em_neo_generaordentrabajo") || fieldName.equals("emNeoGeneraordentrabajo"))
      return emNeoGeneraordentrabajo;
    else if (fieldName.equalsIgnoreCase("em_neo_espublicidad") || fieldName.equals("emNeoEspublicidad"))
      return emNeoEspublicidad;
    else if (fieldName.equalsIgnoreCase("em_neo_duplica_orden") || fieldName.equals("emNeoDuplicaOrden"))
      return emNeoDuplicaOrden;
    else if (fieldName.equalsIgnoreCase("em_neo_genorden_trabajo") || fieldName.equals("emNeoGenordenTrabajo"))
      return emNeoGenordenTrabajo;
    else if (fieldName.equalsIgnoreCase("generatetemplate"))
      return generatetemplate;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("dropship_bpartner_id") || fieldName.equals("dropshipBpartnerId"))
      return dropshipBpartnerId;
    else if (fieldName.equalsIgnoreCase("isinvoiced"))
      return isinvoiced;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("dropship_location_id") || fieldName.equals("dropshipLocationId"))
      return dropshipLocationId;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("dropship_user_id") || fieldName.equals("dropshipUserId"))
      return dropshipUserId;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("isdelivered"))
      return isdelivered;
    else if (fieldName.equalsIgnoreCase("incotermsdescription"))
      return incotermsdescription;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("deliverynotes"))
      return deliverynotes;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("isselected"))
      return isselected;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Header14F6810D5E014E80BC1DC729700C3A8DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Header14F6810D5E014E80BC1DC729700C3A8DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.CreatedBy) as CreatedByR, " +
      "        to_char(C_Order.Updated, ?) as updated, " +
      "        to_char(C_Order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.UpdatedBy) as UpdatedByR," +
      "        C_Order.AD_Org_ID, " +
      "(CASE WHEN C_Order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_Order.C_DocTypeTarget_ID, " +
      "(CASE WHEN C_Order.C_DocTypeTarget_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_DocTypeTarget_IDR, " +
      "C_Order.C_DocType_ID, " +
      "C_Order.DocumentNo, " +
      "C_Order.C_Return_Reason_ID, " +
      "C_Order.DateOrdered, " +
      "C_Order.C_BPartner_ID, " +
      "(CASE WHEN C_Order.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name2), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Order.C_BPartner_Location_ID, " +
      "(CASE WHEN C_Order.C_BPartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Address1), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Address2), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Postal), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.City), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL9.Name IS NULL THEN TO_CHAR(table9.Name) ELSE TO_CHAR(tableTRL9.Name) END)), ''))),'') ) END) AS C_BPartner_Location_IDR, " +
      "C_Order.RM_PickFromShipment, " +
      "C_Order.EM_Neo_Seg_Comercial_ID, " +
      "C_Order.M_PriceList_ID, " +
      "(CASE WHEN C_Order.M_PriceList_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Name), ''))),'') ) END) AS M_PriceList_IDR, " +
      "C_Order.validuntil, " +
      "C_Order.RM_ReceiveMaterials, " +
      "C_Order.DatePromised, " +
      "C_Order.RM_CreateInvoice, " +
      "C_Order.FIN_Paymentmethod_ID, " +
      "(CASE WHEN C_Order.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "C_Order.C_PaymentTerm_ID, " +
      "(CASE WHEN C_Order.C_PaymentTerm_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL13.Name IS NULL THEN TO_CHAR(table13.Name) ELSE TO_CHAR(tableTRL13.Name) END)), ''))),'') ) END) AS C_PaymentTerm_IDR, " +
      "C_Order.M_Warehouse_ID, " +
      "C_Order.C_Reject_Reason_ID, " +
      "(CASE WHEN C_Order.C_Reject_Reason_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.Name), ''))),'') ) END) AS C_Reject_Reason_IDR, " +
      "C_Order.InvoiceRule, " +
      "C_Order.SalesRep_ID, " +
      "(CASE WHEN C_Order.SalesRep_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table16.Name), ''))),'') ) END) AS SalesRep_IDR, " +
      "C_Order.Description, " +
      "C_Order.EM_Neo_Docstatus_Orden, " +
      "C_Order.DocStatus, " +
      "C_Order.GrandTotal, " +
      "C_Order.TotalLines, " +
      "C_Order.C_Currency_ID, " +
      "C_Order.AD_User_ID, " +
      "C_Order.POReference, " +
      "C_Order.BillTo_ID, " +
      "C_Order.Delivery_Location_ID, " +
      "C_Order.CopyFrom, " +
      "C_Order.CopyFromPO, " +
      "C_Order.DocAction, " +
      "list1.name as DocAction_BTN, " +
      "C_Order.DeliveryViaRule, " +
      "C_Order.M_Shipper_ID, " +
      "C_Order.DeliveryRule, " +
      "C_Order.FreightCostRule, " +
      "C_Order.FreightAmt, " +
      "COALESCE(C_Order.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "C_Order.PriorityRule, " +
      "C_Order.ChargeAmt, " +
      "C_Order.C_Charge_ID, " +
      "C_Order.Convertquotation, " +
      "C_Order.EM_Neo_Crea_Orden_Cot, " +
      "C_Order.Calculate_Promotions, " +
      "C_Order.C_Activity_ID, " +
      "C_Order.C_Project_ID, " +
      "(CASE WHEN C_Order.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table17.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table17.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_Order.C_Campaign_ID, " +
      "C_Order.AD_OrgTrx_ID, " +
      "C_Order.User1_ID, " +
      "C_Order.User2_ID, " +
      "C_Order.EM_Neo_Crea_Prod_Nuevo, " +
      "COALESCE(C_Order.EM_Neo_Nuevos_Prod, 'N') AS EM_Neo_Nuevos_Prod, " +
      "C_Order.EM_Neo_Process_Cotiza, " +
      "C_Order.EM_Neo_Generaordentrabajo, " +
      "COALESCE(C_Order.EM_Neo_Espublicidad, 'N') AS EM_Neo_Espublicidad, " +
      "C_Order.EM_Neo_Duplica_Orden, " +
      "C_Order.EM_Neo_Genorden_Trabajo, " +
      "C_Order.Generatetemplate, " +
      "COALESCE(C_Order.IsSelfService, 'N') AS IsSelfService, " +
      "C_Order.DateAcct, " +
      "C_Order.DropShip_BPartner_ID, " +
      "COALESCE(C_Order.IsInvoiced, 'N') AS IsInvoiced, " +
      "COALESCE(C_Order.Processed, 'N') AS Processed, " +
      "COALESCE(C_Order.IsSOTrx, 'N') AS IsSOTrx, " +
      "C_Order.DropShip_Location_ID, " +
      "COALESCE(C_Order.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "C_Order.AD_Client_ID, " +
      "C_Order.DropShip_User_ID, " +
      "C_Order.C_Incoterms_ID, " +
      "COALESCE(C_Order.IsDelivered, 'N') AS IsDelivered, " +
      "C_Order.Incotermsdescription, " +
      "COALESCE(C_Order.IsPrinted, 'N') AS IsPrinted, " +
      "C_Order.Processing, " +
      "C_Order.Deliverynotes, " +
      "COALESCE(C_Order.IsActive, 'N') AS IsActive, " +
      "C_Order.PaymentRule, " +
      "C_Order.C_Order_ID, " +
      "C_Order.Posted, " +
      "C_Order.DatePrinted, " +
      "COALESCE(C_Order.IsSelected, 'N') AS IsSelected, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Order left join (select AD_Org_ID, Name from AD_Org) table1 on (C_Order.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (C_Order.C_DocTypeTarget_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table4 on (C_Order.C_BPartner_ID = table4.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name, C_Location_ID from C_BPartner_Location) table5 on (C_Order.C_BPartner_Location_ID = table5.C_BPartner_Location_ID) left join (select C_Location_ID, Address1, Address2, Postal, C_City_ID, City, C_Region_ID, C_Country_ID from C_Location) table6 on (table5.C_Location_ID = table6.C_Location_ID) left join (select C_City_ID, Name from C_City) table7 on (table6.C_City_ID = table7.C_City_ID) left join (select C_Region_ID, Name from C_Region) table8 on (table6.C_Region_ID = table8.C_Region_ID) left join (select C_Country_ID, Name from C_Country) table9 on (table6.C_Country_ID = table9.C_Country_ID) left join (select C_Country_ID,AD_Language, Name from C_Country_TRL) tableTRL9 on (table9.C_Country_ID = tableTRL9.C_Country_ID and tableTRL9.AD_Language = ?)  left join (select M_PriceList_ID, Name from M_PriceList) table11 on (C_Order.M_PriceList_ID = table11.M_PriceList_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table12 on (C_Order.FIN_Paymentmethod_ID = table12.FIN_Paymentmethod_ID) left join (select C_PaymentTerm_ID, Name from C_PaymentTerm) table13 on (C_Order.C_PaymentTerm_ID = table13.C_PaymentTerm_ID) left join (select C_PaymentTerm_ID,AD_Language, Name from C_PaymentTerm_TRL) tableTRL13 on (table13.C_PaymentTerm_ID = tableTRL13.C_PaymentTerm_ID and tableTRL13.AD_Language = ?)  left join (select C_Reject_Reason_ID, Name from C_Reject_Reason) table15 on (C_Order.C_Reject_Reason_ID = table15.C_Reject_Reason_ID) left join (select AD_User_ID, Name from AD_User) table16 on (C_Order.SalesRep_ID =  table16.AD_User_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = 'FF80818130217A35013021A672400035' and list1.ad_language = ?  AND (CASE C_Order.DocAction WHEN '--' THEN 'CL' ELSE TO_CHAR(C_Order.DocAction) END) = list1.value) left join (select C_Project_ID, Value, Name from C_Project) table17 on (C_Order.C_Project_ID = table17.C_Project_ID)" +
      "        WHERE 2=2 " +
      " AND C_Order.IsSOTrx='Y'" +
      "        AND 1=1 " +
      "        AND C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Header14F6810D5E014E80BC1DC729700C3A8DData objectHeader14F6810D5E014E80BC1DC729700C3A8DData = new Header14F6810D5E014E80BC1DC729700C3A8DData();
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.created = UtilSql.getValue(result, "created");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.updated = UtilSql.getValue(result, "updated");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cDoctypetargetIdr = UtilSql.getValue(result, "c_doctypetarget_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.documentno = UtilSql.getValue(result, "documentno");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.rmPickfromshipment = UtilSql.getValue(result, "rm_pickfromshipment");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoSegComercialId = UtilSql.getValue(result, "em_neo_seg_comercial_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.mPricelistIdr = UtilSql.getValue(result, "m_pricelist_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.validuntil = UtilSql.getDateValue(result, "validuntil", "dd-MM-yyyy");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.rmReceivematerials = UtilSql.getValue(result, "rm_receivematerials");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.rmCreateinvoice = UtilSql.getValue(result, "rm_createinvoice");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cPaymenttermIdr = UtilSql.getValue(result, "c_paymentterm_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cRejectReasonId = UtilSql.getValue(result, "c_reject_reason_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cRejectReasonIdr = UtilSql.getValue(result, "c_reject_reason_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.salesrepIdr = UtilSql.getValue(result, "salesrep_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.description = UtilSql.getValue(result, "description");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoDocstatusOrden = UtilSql.getValue(result, "em_neo_docstatus_orden");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.docstatus = UtilSql.getValue(result, "docstatus");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.totallines = UtilSql.getValue(result, "totallines");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.poreference = UtilSql.getValue(result, "poreference");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.billtoId = UtilSql.getValue(result, "billto_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.deliveryLocationId = UtilSql.getValue(result, "delivery_location_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.copyfrompo = UtilSql.getValue(result, "copyfrompo");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.docaction = UtilSql.getValue(result, "docaction");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.docactionBtn = UtilSql.getValue(result, "docaction_btn");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.freightcostrule = UtilSql.getValue(result, "freightcostrule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.freightamt = UtilSql.getValue(result, "freightamt");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.priorityrule = UtilSql.getValue(result, "priorityrule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.convertquotation = UtilSql.getValue(result, "convertquotation");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoCreaOrdenCot = UtilSql.getValue(result, "em_neo_crea_orden_cot");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.user1Id = UtilSql.getValue(result, "user1_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.user2Id = UtilSql.getValue(result, "user2_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoCreaProdNuevo = UtilSql.getValue(result, "em_neo_crea_prod_nuevo");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoNuevosProd = UtilSql.getValue(result, "em_neo_nuevos_prod");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoProcessCotiza = UtilSql.getValue(result, "em_neo_process_cotiza");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoGeneraordentrabajo = UtilSql.getValue(result, "em_neo_generaordentrabajo");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoEspublicidad = UtilSql.getValue(result, "em_neo_espublicidad");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoDuplicaOrden = UtilSql.getValue(result, "em_neo_duplica_orden");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.emNeoGenordenTrabajo = UtilSql.getValue(result, "em_neo_genorden_trabajo");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.generatetemplate = UtilSql.getValue(result, "generatetemplate");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dropshipBpartnerId = UtilSql.getValue(result, "dropship_bpartner_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isinvoiced = UtilSql.getValue(result, "isinvoiced");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.processed = UtilSql.getValue(result, "processed");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.issotrx = UtilSql.getValue(result, "issotrx");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dropshipLocationId = UtilSql.getValue(result, "dropship_location_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dropshipUserId = UtilSql.getValue(result, "dropship_user_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isdelivered = UtilSql.getValue(result, "isdelivered");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.incotermsdescription = UtilSql.getValue(result, "incotermsdescription");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isprinted = UtilSql.getValue(result, "isprinted");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.processing = UtilSql.getValue(result, "processing");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.deliverynotes = UtilSql.getValue(result, "deliverynotes");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isactive = UtilSql.getValue(result, "isactive");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.posted = UtilSql.getValue(result, "posted");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.isselected = UtilSql.getValue(result, "isselected");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.language = UtilSql.getValue(result, "language");
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adUserClient = "";
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.adOrgClient = "";
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.createdby = "";
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.trBgcolor = "";
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.totalCount = "";
        objectHeader14F6810D5E014E80BC1DC729700C3A8DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeader14F6810D5E014E80BC1DC729700C3A8DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Header14F6810D5E014E80BC1DC729700C3A8DData objectHeader14F6810D5E014E80BC1DC729700C3A8DData[] = new Header14F6810D5E014E80BC1DC729700C3A8DData[vector.size()];
    vector.copyInto(objectHeader14F6810D5E014E80BC1DC729700C3A8DData);
    return(objectHeader14F6810D5E014E80BC1DC729700C3A8DData);
  }

/**
Create a registry
 */
  public static Header14F6810D5E014E80BC1DC729700C3A8DData[] set(String convertquotation, String validuntil, String cOrderId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String docstatus, String docaction, String docactionBtn, String cDoctypeId, String cDoctypetargetId, String description, String isdelivered, String isinvoiced, String isprinted, String dateordered, String datepromised, String dateacct, String salesrepId, String cPaymenttermId, String billtoId, String cCurrencyId, String invoicerule, String freightamt, String deliveryviarule, String mShipperId, String priorityrule, String totallines, String grandtotal, String mWarehouseId, String mPricelistId, String processing, String cCampaignId, String emNeoEspublicidad, String cBpartnerId, String cBpartnerIdr, String adUserId, String poreference, String cChargeId, String chargeamt, String emNeoGenordenTrabajo, String processed, String cBpartnerLocationId, String cProjectId, String cProjectIdr, String cActivityId, String issotrx, String dateprinted, String deliveryrule, String freightcostrule, String paymentrule, String isdiscountprinted, String posted, String istaxincluded, String isselected, String emNeoSegComercialId, String emNeoDuplicaOrden, String deliverynotes, String cIncotermsId, String incotermsdescription, String generatetemplate, String deliveryLocationId, String copyfrompo, String finPaymentmethodId, String dropshipUserId, String dropshipBpartnerId, String copyfrom, String dropshipLocationId, String isselfservice, String emNeoCreaOrdenCot, String adOrgtrxId, String user2Id, String user1Id, String calculatePromotions, String emNeoCreaProdNuevo, String rmPickfromshipment, String rmReceivematerials, String rmCreateinvoice, String cReturnReasonId, String emNeoNuevosProd, String emNeoDocstatusOrden, String cRejectReasonId, String emNeoGeneraordentrabajo, String emNeoProcessCotiza)    throws ServletException {
    Header14F6810D5E014E80BC1DC729700C3A8DData objectHeader14F6810D5E014E80BC1DC729700C3A8DData[] = new Header14F6810D5E014E80BC1DC729700C3A8DData[1];
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0] = new Header14F6810D5E014E80BC1DC729700C3A8DData();
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].created = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].createdbyr = createdbyr;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].updated = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].updatedTimeStamp = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].updatedby = updatedby;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].updatedbyr = updatedbyr;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].adOrgId = adOrgId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].adOrgIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cDoctypetargetId = cDoctypetargetId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cDoctypetargetIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cDoctypeId = cDoctypeId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].documentno = documentno;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cReturnReasonId = cReturnReasonId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dateordered = dateordered;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cBpartnerId = cBpartnerId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cBpartnerIdr = cBpartnerIdr;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cBpartnerLocationId = cBpartnerLocationId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cBpartnerLocationIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].rmPickfromshipment = rmPickfromshipment;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoSegComercialId = emNeoSegComercialId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].mPricelistId = mPricelistId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].mPricelistIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].validuntil = validuntil;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].rmReceivematerials = rmReceivematerials;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].datepromised = datepromised;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].rmCreateinvoice = rmCreateinvoice;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].finPaymentmethodId = finPaymentmethodId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].finPaymentmethodIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cPaymenttermId = cPaymenttermId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cPaymenttermIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].mWarehouseId = mWarehouseId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cRejectReasonId = cRejectReasonId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cRejectReasonIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].invoicerule = invoicerule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].salesrepId = salesrepId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].salesrepIdr = "";
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].description = description;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoDocstatusOrden = emNeoDocstatusOrden;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].docstatus = docstatus;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].grandtotal = grandtotal;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].totallines = totallines;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cCurrencyId = cCurrencyId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].adUserId = adUserId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].poreference = poreference;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].billtoId = billtoId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].deliveryLocationId = deliveryLocationId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].copyfrom = copyfrom;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].copyfrompo = copyfrompo;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].docaction = docaction;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].docactionBtn = docactionBtn;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].deliveryviarule = deliveryviarule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].mShipperId = mShipperId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].deliveryrule = deliveryrule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].freightcostrule = freightcostrule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].freightamt = freightamt;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isdiscountprinted = isdiscountprinted;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].priorityrule = priorityrule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].chargeamt = chargeamt;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cChargeId = cChargeId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].convertquotation = convertquotation;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoCreaOrdenCot = emNeoCreaOrdenCot;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].calculatePromotions = calculatePromotions;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cActivityId = cActivityId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cProjectId = cProjectId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cProjectIdr = cProjectIdr;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cCampaignId = cCampaignId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].adOrgtrxId = adOrgtrxId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].user1Id = user1Id;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].user2Id = user2Id;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoCreaProdNuevo = emNeoCreaProdNuevo;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoNuevosProd = emNeoNuevosProd;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoProcessCotiza = emNeoProcessCotiza;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoGeneraordentrabajo = emNeoGeneraordentrabajo;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoEspublicidad = emNeoEspublicidad;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoDuplicaOrden = emNeoDuplicaOrden;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].emNeoGenordenTrabajo = emNeoGenordenTrabajo;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].generatetemplate = generatetemplate;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isselfservice = isselfservice;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dateacct = dateacct;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dropshipBpartnerId = dropshipBpartnerId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isinvoiced = isinvoiced;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].processed = processed;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].issotrx = issotrx;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dropshipLocationId = dropshipLocationId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].istaxincluded = istaxincluded;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].adClientId = adClientId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dropshipUserId = dropshipUserId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cIncotermsId = cIncotermsId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isdelivered = isdelivered;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].incotermsdescription = incotermsdescription;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isprinted = isprinted;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].processing = processing;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].deliverynotes = deliverynotes;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isactive = isactive;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].paymentrule = paymentrule;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].cOrderId = cOrderId;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].posted = posted;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].dateprinted = dateprinted;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].isselected = isselected;
    objectHeader14F6810D5E014E80BC1DC729700C3A8DData[0].language = "";
    return objectHeader14F6810D5E014E80BC1DC729700C3A8DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2166_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2168_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2762_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3402_3(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static int updateDocAction(ConnectionProvider connectionProvider, String docaction, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET docaction = ? " +
      "        WHERE C_Order.C_Order_ID = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET AD_Org_ID = (?) , C_DocTypeTarget_ID = (?) , C_DocType_ID = (?) , DocumentNo = (?) , C_Return_Reason_ID = (?) , DateOrdered = TO_DATE(?) , C_BPartner_ID = (?) , C_BPartner_Location_ID = (?) , RM_PickFromShipment = (?) , EM_Neo_Seg_Comercial_ID = (?) , M_PriceList_ID = (?) , validuntil = TO_DATE(?) , RM_ReceiveMaterials = (?) , DatePromised = TO_DATE(?) , RM_CreateInvoice = (?) , FIN_Paymentmethod_ID = (?) , C_PaymentTerm_ID = (?) , M_Warehouse_ID = (?) , C_Reject_Reason_ID = (?) , InvoiceRule = (?) , SalesRep_ID = (?) , Description = (?) , EM_Neo_Docstatus_Orden = (?) , DocStatus = (?) , GrandTotal = TO_NUMBER(?) , TotalLines = TO_NUMBER(?) , C_Currency_ID = (?) , AD_User_ID = (?) , POReference = (?) , BillTo_ID = (?) , Delivery_Location_ID = (?) , CopyFrom = (?) , CopyFromPO = (?) , DocAction = (?) , DeliveryViaRule = (?) , M_Shipper_ID = (?) , DeliveryRule = (?) , FreightCostRule = (?) , FreightAmt = TO_NUMBER(?) , IsDiscountPrinted = (?) , PriorityRule = (?) , ChargeAmt = TO_NUMBER(?) , C_Charge_ID = (?) , Convertquotation = (?) , EM_Neo_Crea_Orden_Cot = (?) , Calculate_Promotions = (?) , C_Activity_ID = (?) , C_Project_ID = (?) , C_Campaign_ID = (?) , AD_OrgTrx_ID = (?) , User1_ID = (?) , User2_ID = (?) , EM_Neo_Crea_Prod_Nuevo = (?) , EM_Neo_Nuevos_Prod = (?) , EM_Neo_Process_Cotiza = (?) , EM_Neo_Generaordentrabajo = (?) , EM_Neo_Espublicidad = (?) , EM_Neo_Duplica_Orden = (?) , EM_Neo_Genorden_Trabajo = (?) , Generatetemplate = (?) , IsSelfService = (?) , DateAcct = TO_DATE(?) , DropShip_BPartner_ID = (?) , IsInvoiced = (?) , Processed = (?) , IsSOTrx = (?) , DropShip_Location_ID = (?) , IsTaxIncluded = (?) , AD_Client_ID = (?) , DropShip_User_ID = (?) , C_Incoterms_ID = (?) , IsDelivered = (?) , Incotermsdescription = (?) , IsPrinted = (?) , Processing = (?) , Deliverynotes = (?) , IsActive = (?) , PaymentRule = (?) , C_Order_ID = (?) , Posted = (?) , DatePrinted = TO_DATE(?) , IsSelected = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoSegComercialId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoDocstatusOrden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoCreaOrdenCot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoCreaProdNuevo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoNuevosProd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoProcessCotiza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoGeneraordentrabajo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoEspublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoDuplicaOrden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoGenordenTrabajo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Order " +
      "        (AD_Org_ID, C_DocTypeTarget_ID, C_DocType_ID, DocumentNo, C_Return_Reason_ID, DateOrdered, C_BPartner_ID, C_BPartner_Location_ID, RM_PickFromShipment, EM_Neo_Seg_Comercial_ID, M_PriceList_ID, validuntil, RM_ReceiveMaterials, DatePromised, RM_CreateInvoice, FIN_Paymentmethod_ID, C_PaymentTerm_ID, M_Warehouse_ID, C_Reject_Reason_ID, InvoiceRule, SalesRep_ID, Description, EM_Neo_Docstatus_Orden, DocStatus, GrandTotal, TotalLines, C_Currency_ID, AD_User_ID, POReference, BillTo_ID, Delivery_Location_ID, CopyFrom, CopyFromPO, DocAction, DeliveryViaRule, M_Shipper_ID, DeliveryRule, FreightCostRule, FreightAmt, IsDiscountPrinted, PriorityRule, ChargeAmt, C_Charge_ID, Convertquotation, EM_Neo_Crea_Orden_Cot, Calculate_Promotions, C_Activity_ID, C_Project_ID, C_Campaign_ID, AD_OrgTrx_ID, User1_ID, User2_ID, EM_Neo_Crea_Prod_Nuevo, EM_Neo_Nuevos_Prod, EM_Neo_Process_Cotiza, EM_Neo_Generaordentrabajo, EM_Neo_Espublicidad, EM_Neo_Duplica_Orden, EM_Neo_Genorden_Trabajo, Generatetemplate, IsSelfService, DateAcct, DropShip_BPartner_ID, IsInvoiced, Processed, IsSOTrx, DropShip_Location_ID, IsTaxIncluded, AD_Client_ID, DropShip_User_ID, C_Incoterms_ID, IsDelivered, Incotermsdescription, IsPrinted, Processing, Deliverynotes, IsActive, PaymentRule, C_Order_ID, Posted, DatePrinted, IsSelected, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), TO_DATE(?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoSegComercialId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoDocstatusOrden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoCreaOrdenCot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoCreaProdNuevo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoNuevosProd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoProcessCotiza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoGeneraordentrabajo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoEspublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoDuplicaOrden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeoGenordenTrabajo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Order" +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
