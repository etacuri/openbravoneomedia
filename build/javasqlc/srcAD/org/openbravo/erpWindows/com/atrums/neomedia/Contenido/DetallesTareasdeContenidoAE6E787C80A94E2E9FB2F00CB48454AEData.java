//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Contenido;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData implements FieldProvider {
static Logger log4j = Logger.getLogger(DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String line;
  public String adOrgId;
  public String isactive;
  public String fecharegistro;
  public String tipolinea;
  public String tipolinear;
  public String neoMonitoreoId;
  public String descripcionlinea;
  public String finicio;
  public String ffin;
  public String tiempo;
  public String estadolinea;
  public String estadolinear;
  public String procesarini;
  public String procesarfin;
  public String eliminarlinea;
  public String adClientId;
  public String neoMonitoreolineaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("fecharegistro"))
      return fecharegistro;
    else if (fieldName.equalsIgnoreCase("tipolinea"))
      return tipolinea;
    else if (fieldName.equalsIgnoreCase("tipolinear"))
      return tipolinear;
    else if (fieldName.equalsIgnoreCase("neo_monitoreo_id") || fieldName.equals("neoMonitoreoId"))
      return neoMonitoreoId;
    else if (fieldName.equalsIgnoreCase("descripcionlinea"))
      return descripcionlinea;
    else if (fieldName.equalsIgnoreCase("finicio"))
      return finicio;
    else if (fieldName.equalsIgnoreCase("ffin"))
      return ffin;
    else if (fieldName.equalsIgnoreCase("tiempo"))
      return tiempo;
    else if (fieldName.equalsIgnoreCase("estadolinea"))
      return estadolinea;
    else if (fieldName.equalsIgnoreCase("estadolinear"))
      return estadolinear;
    else if (fieldName.equalsIgnoreCase("procesarini"))
      return procesarini;
    else if (fieldName.equalsIgnoreCase("procesarfin"))
      return procesarfin;
    else if (fieldName.equalsIgnoreCase("eliminarlinea"))
      return eliminarlinea;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_monitoreolinea_id") || fieldName.equals("neoMonitoreolineaId"))
      return neoMonitoreolineaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String neoMonitoreoId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, neoMonitoreoId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String neoMonitoreoId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_monitoreolinea.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_monitoreolinea.CreatedBy) as CreatedByR, " +
      "        to_char(neo_monitoreolinea.Updated, ?) as updated, " +
      "        to_char(neo_monitoreolinea.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_monitoreolinea.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_monitoreolinea.UpdatedBy) as UpdatedByR," +
      "        neo_monitoreolinea.Line, " +
      "neo_monitoreolinea.AD_Org_ID, " +
      "COALESCE(neo_monitoreolinea.Isactive, 'N') AS Isactive, " +
      "TO_CHAR(neo_monitoreolinea.Fecharegistro, ?) AS Fecharegistro, " +
      "neo_monitoreolinea.Tipolinea, " +
      "(CASE WHEN neo_monitoreolinea.Tipolinea IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS TipolineaR, " +
      "neo_monitoreolinea.NEO_Monitoreo_ID, " +
      "neo_monitoreolinea.Descripcionlinea, " +
      "TO_CHAR(neo_monitoreolinea.Finicio, ?) AS Finicio, " +
      "TO_CHAR(neo_monitoreolinea.Ffin, ?) AS Ffin, " +
      "neo_monitoreolinea.Tiempo, " +
      "neo_monitoreolinea.Estadolinea, " +
      "(CASE WHEN neo_monitoreolinea.Estadolinea IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS EstadolineaR, " +
      "neo_monitoreolinea.Procesarini, " +
      "neo_monitoreolinea.Procesarfin, " +
      "neo_monitoreolinea.Eliminarlinea, " +
      "neo_monitoreolinea.AD_Client_ID, " +
      "neo_monitoreolinea.NEO_Monitoreolinea_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_monitoreolinea left join ad_ref_list_v list1 on (neo_monitoreolinea.Tipolinea = list1.value and list1.ad_reference_id = '7570B11FD59F451BBAC2A5C7A186FFC5' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (neo_monitoreolinea.Estadolinea = list2.value and list2.ad_reference_id = '5A673B369799440881D5E132BE93CE65' and list2.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((neoMonitoreoId==null || neoMonitoreoId.equals(""))?"":"  AND neo_monitoreolinea.NEO_Monitoreo_ID = ?  ");
    strSql = strSql + 
      "        AND neo_monitoreolinea.NEO_Monitoreolinea_ID = ? " +
      "        AND neo_monitoreolinea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_monitoreolinea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (neoMonitoreoId != null && !(neoMonitoreoId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData();
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.created = UtilSql.getValue(result, "created");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.updated = UtilSql.getValue(result, "updated");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.updatedby = UtilSql.getValue(result, "updatedby");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.line = UtilSql.getValue(result, "line");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.isactive = UtilSql.getValue(result, "isactive");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.fecharegistro = UtilSql.getValue(result, "fecharegistro");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.tipolinea = UtilSql.getValue(result, "tipolinea");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.tipolinear = UtilSql.getValue(result, "tipolinear");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.neoMonitoreoId = UtilSql.getValue(result, "neo_monitoreo_id");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.descripcionlinea = UtilSql.getValue(result, "descripcionlinea");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.finicio = UtilSql.getValue(result, "finicio");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.ffin = UtilSql.getValue(result, "ffin");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.tiempo = UtilSql.getValue(result, "tiempo");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.estadolinea = UtilSql.getValue(result, "estadolinea");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.estadolinear = UtilSql.getValue(result, "estadolinear");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.procesarini = UtilSql.getValue(result, "procesarini");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.procesarfin = UtilSql.getValue(result, "procesarfin");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.eliminarlinea = UtilSql.getValue(result, "eliminarlinea");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.neoMonitoreolineaId = UtilSql.getValue(result, "neo_monitoreolinea_id");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.language = UtilSql.getValue(result, "language");
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.adUserClient = "";
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.adOrgClient = "";
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.createdby = "";
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.trBgcolor = "";
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.totalCount = "";
        objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[vector.size()];
    vector.copyInto(objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData);
    return(objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData);
  }

/**
Create a registry
 */
  public static DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] set(String neoMonitoreoId, String procesarini, String neoMonitoreolineaId, String tiempo, String adOrgId, String estadolinea, String descripcionlinea, String tipolinea, String createdby, String createdbyr, String line, String isactive, String eliminarlinea, String updatedby, String updatedbyr, String finicio, String procesarfin, String adClientId, String ffin, String fecharegistro)    throws ServletException {
    DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[] = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[1];
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0] = new DetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData();
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].created = "";
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].createdbyr = createdbyr;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].updated = "";
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].updatedTimeStamp = "";
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].updatedby = updatedby;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].updatedbyr = updatedbyr;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].line = line;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].adOrgId = adOrgId;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].isactive = isactive;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].fecharegistro = fecharegistro;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].tipolinea = tipolinea;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].tipolinear = "";
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].neoMonitoreoId = neoMonitoreoId;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].descripcionlinea = descripcionlinea;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].finicio = finicio;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].ffin = ffin;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].tiempo = tiempo;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].estadolinea = estadolinea;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].estadolinear = "";
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].procesarini = procesarini;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].procesarfin = procesarfin;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].eliminarlinea = eliminarlinea;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].adClientId = adClientId;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].neoMonitoreolineaId = neoMonitoreolineaId;
    objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData[0].language = "";
    return objectDetallesTareasdeContenidoAE6E787C80A94E2E9FB2F00CB48454AEData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef965FA361A9F644109731C624214AAEA8_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef9EC68EDFEE814881A47AC169C7FB7375(ConnectionProvider connectionProvider, String NEO_Monitoreo_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue FROM NEO_Monitoreolinea WHERE NEO_Monitoreo_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, NEO_Monitoreo_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA657727788954A059FF37DEF7EBD5BAF_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT neo_monitoreolinea.NEO_Monitoreo_ID AS NAME" +
      "        FROM neo_monitoreolinea" +
      "        WHERE neo_monitoreolinea.NEO_Monitoreolinea_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String neoMonitoreoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Monitor), ''))) AS NAME FROM neo_monitoreo left join (select NEO_Monitoreo_ID, Monitor from NEO_Monitoreo) table1 on (neo_monitoreo.NEO_Monitoreo_ID = table1.NEO_Monitoreo_ID) WHERE neo_monitoreo.NEO_Monitoreo_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String neoMonitoreoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Monitor), ''))) AS NAME FROM neo_monitoreo left join (select NEO_Monitoreo_ID, Monitor from NEO_Monitoreo) table1 on (neo_monitoreo.NEO_Monitoreo_ID = table1.NEO_Monitoreo_ID) WHERE neo_monitoreo.NEO_Monitoreo_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_monitoreolinea" +
      "        SET Line = TO_NUMBER(?) , AD_Org_ID = (?) , Isactive = (?) , Fecharegistro = TO_TIMESTAMP(?, ?) , Tipolinea = (?) , NEO_Monitoreo_ID = (?) , Descripcionlinea = (?) , Finicio = TO_TIMESTAMP(?, ?) , Ffin = TO_TIMESTAMP(?, ?) , Tiempo = TO_NUMBER(?) , Estadolinea = (?) , Procesarini = (?) , Procesarfin = (?) , Eliminarlinea = (?) , AD_Client_ID = (?) , NEO_Monitoreolinea_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_monitoreolinea.NEO_Monitoreolinea_ID = ? " +
      "                 AND neo_monitoreolinea.NEO_Monitoreo_ID = ? " +
      "        AND neo_monitoreolinea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_monitoreolinea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecharegistro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcionlinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ffin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiempo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarini);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarfin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, eliminarlinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreolineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreolineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_monitoreolinea " +
      "        (Line, AD_Org_ID, Isactive, Fecharegistro, Tipolinea, NEO_Monitoreo_ID, Descripcionlinea, Finicio, Ffin, Tiempo, Estadolinea, Procesarini, Procesarfin, Eliminarlinea, AD_Client_ID, NEO_Monitoreolinea_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), (?), (?), TO_TIMESTAMP(?, ?), (?), (?), (?), TO_TIMESTAMP(?, ?), TO_TIMESTAMP(?, ?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecharegistro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcionlinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ffin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiempo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarini);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarfin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, eliminarlinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreolineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String neoMonitoreoId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_monitoreolinea" +
      "        WHERE neo_monitoreolinea.NEO_Monitoreolinea_ID = ? " +
      "                 AND neo_monitoreolinea.NEO_Monitoreo_ID = ? " +
      "        AND neo_monitoreolinea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_monitoreolinea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_monitoreolinea" +
      "         WHERE neo_monitoreolinea.NEO_Monitoreolinea_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_monitoreolinea" +
      "         WHERE neo_monitoreolinea.NEO_Monitoreolinea_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
