//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Soporte;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data implements FieldProvider {
static Logger log4j = Logger.getLogger(LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String line;
  public String adOrgId;
  public String fechaLineacaso;
  public String cbpTecnicoId;
  public String cbpTecnicoIdr;
  public String estadolinea;
  public String estadolinear;
  public String tiempoEjecutado;
  public String finicio;
  public String neoCasoId;
  public String ffin;
  public String comentarios;
  public String escalamiento;
  public String escalamientor;
  public String isejecutado;
  public String comentariotec;
  public String mpreventivo;
  public String mcorrectivo;
  public String instalacion;
  public String desinstalacion;
  public String inspeccion;
  public String fotografia;
  public String encuesta;
  public String demo;
  public String procesarini;
  public String procesarfin;
  public String isactive;
  public String neoCasolineId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("fecha_lineacaso") || fieldName.equals("fechaLineacaso"))
      return fechaLineacaso;
    else if (fieldName.equalsIgnoreCase("cbp_tecnico_id") || fieldName.equals("cbpTecnicoId"))
      return cbpTecnicoId;
    else if (fieldName.equalsIgnoreCase("cbp_tecnico_idr") || fieldName.equals("cbpTecnicoIdr"))
      return cbpTecnicoIdr;
    else if (fieldName.equalsIgnoreCase("estadolinea"))
      return estadolinea;
    else if (fieldName.equalsIgnoreCase("estadolinear"))
      return estadolinear;
    else if (fieldName.equalsIgnoreCase("tiempo_ejecutado") || fieldName.equals("tiempoEjecutado"))
      return tiempoEjecutado;
    else if (fieldName.equalsIgnoreCase("finicio"))
      return finicio;
    else if (fieldName.equalsIgnoreCase("neo_caso_id") || fieldName.equals("neoCasoId"))
      return neoCasoId;
    else if (fieldName.equalsIgnoreCase("ffin"))
      return ffin;
    else if (fieldName.equalsIgnoreCase("comentarios"))
      return comentarios;
    else if (fieldName.equalsIgnoreCase("escalamiento"))
      return escalamiento;
    else if (fieldName.equalsIgnoreCase("escalamientor"))
      return escalamientor;
    else if (fieldName.equalsIgnoreCase("isejecutado"))
      return isejecutado;
    else if (fieldName.equalsIgnoreCase("comentariotec"))
      return comentariotec;
    else if (fieldName.equalsIgnoreCase("mpreventivo"))
      return mpreventivo;
    else if (fieldName.equalsIgnoreCase("mcorrectivo"))
      return mcorrectivo;
    else if (fieldName.equalsIgnoreCase("instalacion"))
      return instalacion;
    else if (fieldName.equalsIgnoreCase("desinstalacion"))
      return desinstalacion;
    else if (fieldName.equalsIgnoreCase("inspeccion"))
      return inspeccion;
    else if (fieldName.equalsIgnoreCase("fotografia"))
      return fotografia;
    else if (fieldName.equalsIgnoreCase("encuesta"))
      return encuesta;
    else if (fieldName.equalsIgnoreCase("demo"))
      return demo;
    else if (fieldName.equalsIgnoreCase("procesarini"))
      return procesarini;
    else if (fieldName.equalsIgnoreCase("procesarfin"))
      return procesarfin;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("neo_casoline_id") || fieldName.equals("neoCasolineId"))
      return neoCasolineId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String neoCasoId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, neoCasoId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String neoCasoId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_casoline.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_casoline.CreatedBy) as CreatedByR, " +
      "        to_char(neo_casoline.Updated, ?) as updated, " +
      "        to_char(neo_casoline.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_casoline.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_casoline.UpdatedBy) as UpdatedByR," +
      "        neo_casoline.Line, " +
      "neo_casoline.AD_Org_ID, " +
      "neo_casoline.Fecha_Lineacaso, " +
      "neo_casoline.CBP_Tecnico_ID, " +
      "(CASE WHEN neo_casoline.CBP_Tecnico_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS CBP_Tecnico_IDR, " +
      "neo_casoline.Estadolinea, " +
      "(CASE WHEN neo_casoline.Estadolinea IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EstadolineaR, " +
      "neo_casoline.Tiempo_Ejecutado, " +
      "TO_CHAR(neo_casoline.Finicio, ?) AS Finicio, " +
      "neo_casoline.NEO_Caso_ID, " +
      "TO_CHAR(neo_casoline.Ffin, ?) AS Ffin, " +
      "neo_casoline.Comentarios, " +
      "neo_casoline.Escalamiento, " +
      "(CASE WHEN neo_casoline.Escalamiento IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS EscalamientoR, " +
      "COALESCE(neo_casoline.Isejecutado, 'N') AS Isejecutado, " +
      "neo_casoline.Comentariotec, " +
      "COALESCE(neo_casoline.Mpreventivo, 'N') AS Mpreventivo, " +
      "COALESCE(neo_casoline.Mcorrectivo, 'N') AS Mcorrectivo, " +
      "COALESCE(neo_casoline.Instalacion, 'N') AS Instalacion, " +
      "COALESCE(neo_casoline.Desinstalacion, 'N') AS Desinstalacion, " +
      "COALESCE(neo_casoline.Inspeccion, 'N') AS Inspeccion, " +
      "COALESCE(neo_casoline.Fotografia, 'N') AS Fotografia, " +
      "COALESCE(neo_casoline.Encuesta, 'N') AS Encuesta, " +
      "COALESCE(neo_casoline.Demo, 'N') AS Demo, " +
      "neo_casoline.Procesarini, " +
      "neo_casoline.Procesarfin, " +
      "COALESCE(neo_casoline.Isactive, 'N') AS Isactive, " +
      "neo_casoline.NEO_Casoline_ID, " +
      "neo_casoline.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_casoline left join (select C_BPartner_ID, value, Name from C_BPartner) table1 on (neo_casoline.CBP_Tecnico_ID =  table1.C_BPartner_ID) left join ad_ref_list_v list1 on (neo_casoline.Estadolinea = list1.value and list1.ad_reference_id = '5A673B369799440881D5E132BE93CE65' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (neo_casoline.Escalamiento = list2.value and list2.ad_reference_id = '3F16C355DDBB4B7887E5A6456FC01C17' and list2.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((neoCasoId==null || neoCasoId.equals(""))?"":"  AND neo_casoline.NEO_Caso_ID = ?  ");
    strSql = strSql + 
      "        AND neo_casoline.NEO_Casoline_ID = ? " +
      "        AND neo_casoline.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_casoline.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (neoCasoId != null && !(neoCasoId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data();
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.created = UtilSql.getValue(result, "created");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.updated = UtilSql.getValue(result, "updated");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.line = UtilSql.getValue(result, "line");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.fechaLineacaso = UtilSql.getDateValue(result, "fecha_lineacaso", "dd-MM-yyyy");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.cbpTecnicoId = UtilSql.getValue(result, "cbp_tecnico_id");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.cbpTecnicoIdr = UtilSql.getValue(result, "cbp_tecnico_idr");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.estadolinea = UtilSql.getValue(result, "estadolinea");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.estadolinear = UtilSql.getValue(result, "estadolinear");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.tiempoEjecutado = UtilSql.getValue(result, "tiempo_ejecutado");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.finicio = UtilSql.getValue(result, "finicio");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.neoCasoId = UtilSql.getValue(result, "neo_caso_id");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.ffin = UtilSql.getValue(result, "ffin");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.comentarios = UtilSql.getValue(result, "comentarios");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.escalamiento = UtilSql.getValue(result, "escalamiento");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.escalamientor = UtilSql.getValue(result, "escalamientor");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.isejecutado = UtilSql.getValue(result, "isejecutado");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.comentariotec = UtilSql.getValue(result, "comentariotec");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.mpreventivo = UtilSql.getValue(result, "mpreventivo");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.mcorrectivo = UtilSql.getValue(result, "mcorrectivo");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.instalacion = UtilSql.getValue(result, "instalacion");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.desinstalacion = UtilSql.getValue(result, "desinstalacion");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.inspeccion = UtilSql.getValue(result, "inspeccion");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.fotografia = UtilSql.getValue(result, "fotografia");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.encuesta = UtilSql.getValue(result, "encuesta");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.demo = UtilSql.getValue(result, "demo");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.procesarini = UtilSql.getValue(result, "procesarini");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.procesarfin = UtilSql.getValue(result, "procesarfin");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.isactive = UtilSql.getValue(result, "isactive");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.neoCasolineId = UtilSql.getValue(result, "neo_casoline_id");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.language = UtilSql.getValue(result, "language");
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.adUserClient = "";
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.adOrgClient = "";
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.createdby = "";
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.trBgcolor = "";
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.totalCount = "";
        objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[vector.size()];
    vector.copyInto(objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data);
    return(objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data);
  }

/**
Create a registry
 */
  public static LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] set(String neoCasoId, String comentariotec, String mcorrectivo, String comentarios, String adOrgId, String desinstalacion, String line, String escalamiento, String cbpTecnicoId, String demo, String isejecutado, String fechaLineacaso, String updatedby, String updatedbyr, String encuesta, String mpreventivo, String finicio, String estadolinea, String instalacion, String ffin, String procesarini, String fotografia, String isactive, String procesarfin, String tiempoEjecutado, String neoCasolineId, String inspeccion, String adClientId, String createdby, String createdbyr)    throws ServletException {
    LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[] = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[1];
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0] = new LineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data();
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].created = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].createdbyr = createdbyr;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].updated = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].updatedTimeStamp = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].updatedby = updatedby;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].updatedbyr = updatedbyr;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].line = line;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].adOrgId = adOrgId;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].fechaLineacaso = fechaLineacaso;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].cbpTecnicoId = cbpTecnicoId;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].cbpTecnicoIdr = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].estadolinea = estadolinea;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].estadolinear = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].tiempoEjecutado = tiempoEjecutado;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].finicio = finicio;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].neoCasoId = neoCasoId;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].ffin = ffin;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].comentarios = comentarios;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].escalamiento = escalamiento;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].escalamientor = "";
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].isejecutado = isejecutado;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].comentariotec = comentariotec;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].mpreventivo = mpreventivo;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].mcorrectivo = mcorrectivo;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].instalacion = instalacion;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].desinstalacion = desinstalacion;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].inspeccion = inspeccion;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].fotografia = fotografia;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].encuesta = encuesta;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].demo = demo;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].procesarini = procesarini;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].procesarfin = procesarfin;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].isactive = isactive;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].neoCasolineId = neoCasolineId;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].adClientId = adClientId;
    objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data[0].language = "";
    return objectLineasCaso2DD9FA7F8FF64A3090C7AA7BBA82A099Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef39EA8AB0705F48029552559B19BF58D6(ConnectionProvider connectionProvider, String NEO_CASO_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(LINE),0)+10 AS DefaultValue FROM NEO_CASOLINE WHERE NEO_CASO_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, NEO_CASO_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef66908ECE63BC4995AE4DC4B9092C3D28_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDE7D759E11224AECB3DCF159DC746AFC_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT neo_casoline.NEO_Caso_ID AS NAME" +
      "        FROM neo_casoline" +
      "        WHERE neo_casoline.NEO_Casoline_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String neoCasoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Nombrecaso), ''))) AS NAME FROM neo_caso left join (select NEO_Caso_ID, Nombrecaso from NEO_Caso) table1 on (neo_caso.NEO_Caso_ID = table1.NEO_Caso_ID) WHERE neo_caso.NEO_Caso_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String neoCasoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Nombrecaso), ''))) AS NAME FROM neo_caso left join (select NEO_Caso_ID, Nombrecaso from NEO_Caso) table1 on (neo_caso.NEO_Caso_ID = table1.NEO_Caso_ID) WHERE neo_caso.NEO_Caso_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_casoline" +
      "        SET Line = TO_NUMBER(?) , AD_Org_ID = (?) , Fecha_Lineacaso = TO_DATE(?) , CBP_Tecnico_ID = (?) , Estadolinea = (?) , Tiempo_Ejecutado = TO_NUMBER(?) , Finicio = TO_TIMESTAMP(?, ?) , NEO_Caso_ID = (?) , Ffin = TO_TIMESTAMP(?, ?) , Comentarios = (?) , Escalamiento = (?) , Isejecutado = (?) , Comentariotec = (?) , Mpreventivo = (?) , Mcorrectivo = (?) , Instalacion = (?) , Desinstalacion = (?) , Inspeccion = (?) , Fotografia = (?) , Encuesta = (?) , Demo = (?) , Procesarini = (?) , Procesarfin = (?) , Isactive = (?) , NEO_Casoline_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_casoline.NEO_Casoline_ID = ? " +
      "                 AND neo_casoline.NEO_Caso_ID = ? " +
      "        AND neo_casoline.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_casoline.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaLineacaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpTecnicoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiempoEjecutado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ffin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, comentarios);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, escalamiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isejecutado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, comentariotec);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mpreventivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mcorrectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, instalacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, desinstalacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inspeccion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fotografia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, encuesta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, demo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarini);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarfin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasolineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasolineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_casoline " +
      "        (Line, AD_Org_ID, Fecha_Lineacaso, CBP_Tecnico_ID, Estadolinea, Tiempo_Ejecutado, Finicio, NEO_Caso_ID, Ffin, Comentarios, Escalamiento, Isejecutado, Comentariotec, Mpreventivo, Mcorrectivo, Instalacion, Desinstalacion, Inspeccion, Fotografia, Encuesta, Demo, Procesarini, Procesarfin, Isactive, NEO_Casoline_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), (?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_TIMESTAMP(?, ?), (?), TO_TIMESTAMP(?, ?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaLineacaso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpTecnicoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadolinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tiempoEjecutado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ffin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, comentarios);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, escalamiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isejecutado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, comentariotec);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mpreventivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mcorrectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, instalacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, desinstalacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inspeccion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fotografia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, encuesta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, demo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarini);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarfin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasolineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String neoCasoId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_casoline" +
      "        WHERE neo_casoline.NEO_Casoline_ID = ? " +
      "                 AND neo_casoline.NEO_Caso_ID = ? " +
      "        AND neo_casoline.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_casoline.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_casoline" +
      "         WHERE neo_casoline.NEO_Casoline_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_casoline" +
      "         WHERE neo_casoline.NEO_Casoline_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
