//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.AdministracionSoporteYMantenimiento;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data implements FieldProvider {
static Logger log4j = Logger.getLogger(SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String isactive;
  public String numdocumento;
  public String processed;
  public String fInicio;
  public String fFin;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String mProductId;
  public String mProductIdr;
  public String cantidad;
  public String cantPendFacturar;
  public String cantFacturar;
  public String estadofacturado;
  public String estadofacturador;
  public String generarfactura;
  public String adClientId;
  public String neoAdmSopMantVId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("numdocumento"))
      return numdocumento;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("f_inicio") || fieldName.equals("fInicio"))
      return fInicio;
    else if (fieldName.equalsIgnoreCase("f_fin") || fieldName.equals("fFin"))
      return fFin;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("cantidad"))
      return cantidad;
    else if (fieldName.equalsIgnoreCase("cant_pend_facturar") || fieldName.equals("cantPendFacturar"))
      return cantPendFacturar;
    else if (fieldName.equalsIgnoreCase("cant_facturar") || fieldName.equals("cantFacturar"))
      return cantFacturar;
    else if (fieldName.equalsIgnoreCase("estadofacturado"))
      return estadofacturado;
    else if (fieldName.equalsIgnoreCase("estadofacturador"))
      return estadofacturador;
    else if (fieldName.equalsIgnoreCase("generarfactura"))
      return generarfactura;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_adm_sop_mant_v_id") || fieldName.equals("neoAdmSopMantVId"))
      return neoAdmSopMantVId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_adm_sop_mant_v.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_adm_sop_mant_v.CreatedBy) as CreatedByR, " +
      "        to_char(neo_adm_sop_mant_v.Updated, ?) as updated, " +
      "        to_char(neo_adm_sop_mant_v.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_adm_sop_mant_v.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_adm_sop_mant_v.UpdatedBy) as UpdatedByR," +
      "        neo_adm_sop_mant_v.AD_Org_ID, " +
      "(CASE WHEN neo_adm_sop_mant_v.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "neo_adm_sop_mant_v.C_Doctype_ID, " +
      "(CASE WHEN neo_adm_sop_mant_v.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "COALESCE(neo_adm_sop_mant_v.Isactive, 'N') AS Isactive, " +
      "neo_adm_sop_mant_v.Numdocumento, " +
      "COALESCE(neo_adm_sop_mant_v.Processed, 'N') AS Processed, " +
      "neo_adm_sop_mant_v.F_Inicio, " +
      "neo_adm_sop_mant_v.F_Fin, " +
      "neo_adm_sop_mant_v.C_Bpartner_ID, " +
      "(CASE WHEN neo_adm_sop_mant_v.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "neo_adm_sop_mant_v.M_Product_ID, " +
      "(CASE WHEN neo_adm_sop_mant_v.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "neo_adm_sop_mant_v.Cantidad, " +
      "neo_adm_sop_mant_v.Cant_Pend_Facturar, " +
      "neo_adm_sop_mant_v.Cant_Facturar, " +
      "neo_adm_sop_mant_v.Estadofacturado, " +
      "(CASE WHEN neo_adm_sop_mant_v.Estadofacturado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EstadofacturadoR, " +
      "neo_adm_sop_mant_v.Generarfactura, " +
      "neo_adm_sop_mant_v.AD_Client_ID, " +
      "neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_adm_sop_mant_v left join (select AD_Org_ID, Name from AD_Org) table1 on (neo_adm_sop_mant_v.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (neo_adm_sop_mant_v.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table4 on (neo_adm_sop_mant_v.C_Bpartner_ID = table4.C_BPartner_ID) left join (select M_Product_ID, Name from M_Product) table5 on (neo_adm_sop_mant_v.M_Product_ID = table5.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL5 on (table5.M_Product_ID = tableTRL5.M_Product_ID and tableTRL5.AD_Language = ?)  left join ad_ref_list_v list1 on (neo_adm_sop_mant_v.Estadofacturado = list1.value and list1.ad_reference_id = '939B4F215E004FBA85454E6494ADC245' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID = ? " +
      "        AND neo_adm_sop_mant_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_adm_sop_mant_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data = new SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data();
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.created = UtilSql.getValue(result, "created");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.updated = UtilSql.getValue(result, "updated");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.isactive = UtilSql.getValue(result, "isactive");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.numdocumento = UtilSql.getValue(result, "numdocumento");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.processed = UtilSql.getValue(result, "processed");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.fInicio = UtilSql.getDateValue(result, "f_inicio", "dd-MM-yyyy");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.fFin = UtilSql.getDateValue(result, "f_fin", "dd-MM-yyyy");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cantidad = UtilSql.getValue(result, "cantidad");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cantPendFacturar = UtilSql.getValue(result, "cant_pend_facturar");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.cantFacturar = UtilSql.getValue(result, "cant_facturar");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.estadofacturado = UtilSql.getValue(result, "estadofacturado");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.estadofacturador = UtilSql.getValue(result, "estadofacturador");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.generarfactura = UtilSql.getValue(result, "generarfactura");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.neoAdmSopMantVId = UtilSql.getValue(result, "neo_adm_sop_mant_v_id");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.language = UtilSql.getValue(result, "language");
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.adUserClient = "";
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.adOrgClient = "";
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.createdby = "";
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.trBgcolor = "";
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.totalCount = "";
        objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[] = new SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[vector.size()];
    vector.copyInto(objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data);
    return(objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data);
  }

/**
Create a registry
 */
  public static SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[] set(String processed, String fFin, String cantFacturar, String generarfactura, String cantidad, String createdby, String createdbyr, String numdocumento, String adClientId, String cantPendFacturar, String neoAdmSopMantVId, String cBpartnerId, String cBpartnerIdr, String mProductId, String mProductIdr, String isactive, String fInicio, String estadofacturado, String cDoctypeId, String adOrgId, String updatedby, String updatedbyr)    throws ServletException {
    SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[] = new SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[1];
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0] = new SoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data();
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].created = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].createdbyr = createdbyr;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].updated = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].updatedTimeStamp = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].updatedby = updatedby;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].updatedbyr = updatedbyr;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].adOrgId = adOrgId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].adOrgIdr = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cDoctypeId = cDoctypeId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cDoctypeIdr = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].isactive = isactive;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].numdocumento = numdocumento;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].processed = processed;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].fInicio = fInicio;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].fFin = fFin;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cBpartnerId = cBpartnerId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cBpartnerIdr = cBpartnerIdr;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].mProductId = mProductId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].mProductIdr = mProductIdr;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cantidad = cantidad;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cantPendFacturar = cantPendFacturar;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].cantFacturar = cantFacturar;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].estadofacturado = estadofacturado;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].estadofacturador = "";
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].generarfactura = generarfactura;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].adClientId = adClientId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].neoAdmSopMantVId = neoAdmSopMantVId;
    objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data[0].language = "";
    return objectSoporteyMantenimientoB89406C4315A4D2B956D817D3B18A036Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef8A93885E0162484A85A22839964DFCA3_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB8781E791D0F4C7482FF9B4758CE975C_1(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB95B3C9896A24EEA9F2CDD2D749A82F9_2(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefF26C10C5AB88451DADEAF15806CAF3CD_3(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_adm_sop_mant_v" +
      "        SET AD_Org_ID = (?) , C_Doctype_ID = (?) , Isactive = (?) , Numdocumento = (?) , Processed = (?) , F_Inicio = TO_DATE(?) , F_Fin = TO_DATE(?) , C_Bpartner_ID = (?) , M_Product_ID = (?) , Cantidad = TO_NUMBER(?) , Cant_Pend_Facturar = TO_NUMBER(?) , Cant_Facturar = TO_NUMBER(?) , Estadofacturado = (?) , Generarfactura = (?) , AD_Client_ID = (?) , NEO_Adm_Sop_Mant_V_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID = ? " +
      "        AND neo_adm_sop_mant_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_adm_sop_mant_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numdocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantPendFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadofacturado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarfactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoAdmSopMantVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoAdmSopMantVId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_adm_sop_mant_v " +
      "        (AD_Org_ID, C_Doctype_ID, Isactive, Numdocumento, Processed, F_Inicio, F_Fin, C_Bpartner_ID, M_Product_ID, Cantidad, Cant_Pend_Facturar, Cant_Facturar, Estadofacturado, Generarfactura, AD_Client_ID, NEO_Adm_Sop_Mant_V_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numdocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantPendFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadofacturado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarfactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoAdmSopMantVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_adm_sop_mant_v" +
      "        WHERE neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID = ? " +
      "        AND neo_adm_sop_mant_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_adm_sop_mant_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_adm_sop_mant_v" +
      "         WHERE neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_adm_sop_mant_v" +
      "         WHERE neo_adm_sop_mant_v.NEO_Adm_Sop_Mant_V_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
