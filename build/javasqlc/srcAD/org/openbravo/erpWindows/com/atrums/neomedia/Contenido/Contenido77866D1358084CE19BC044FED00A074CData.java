//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.Contenido;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Contenido77866D1358084CE19BC044FED00A074CData implements FieldProvider {
static Logger log4j = Logger.getLogger(Contenido77866D1358084CE19BC044FED00A074CData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String monitor;
  public String docstatus;
  public String docstatusr;
  public String reproductores;
  public String fechaInicio;
  public String fechaFinal;
  public String cbpClienteId;
  public String contactopubli;
  public String descripcion;
  public String procesar;
  public String ordentrabajoId;
  public String oportunidadId;
  public String cbpSupervisorId;
  public String fechaFactura;
  public String generarFactura;
  public String adClientId;
  public String neoMonitoreoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("monitor"))
      return monitor;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("reproductores"))
      return reproductores;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_final") || fieldName.equals("fechaFinal"))
      return fechaFinal;
    else if (fieldName.equalsIgnoreCase("cbp_cliente_id") || fieldName.equals("cbpClienteId"))
      return cbpClienteId;
    else if (fieldName.equalsIgnoreCase("contactopubli"))
      return contactopubli;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("ordentrabajo_id") || fieldName.equals("ordentrabajoId"))
      return ordentrabajoId;
    else if (fieldName.equalsIgnoreCase("oportunidad_id") || fieldName.equals("oportunidadId"))
      return oportunidadId;
    else if (fieldName.equalsIgnoreCase("cbp_supervisor_id") || fieldName.equals("cbpSupervisorId"))
      return cbpSupervisorId;
    else if (fieldName.equalsIgnoreCase("fecha_factura") || fieldName.equals("fechaFactura"))
      return fechaFactura;
    else if (fieldName.equalsIgnoreCase("generar_factura") || fieldName.equals("generarFactura"))
      return generarFactura;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_monitoreo_id") || fieldName.equals("neoMonitoreoId"))
      return neoMonitoreoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Contenido77866D1358084CE19BC044FED00A074CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Contenido77866D1358084CE19BC044FED00A074CData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_monitoreo.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_monitoreo.CreatedBy) as CreatedByR, " +
      "        to_char(neo_monitoreo.Updated, ?) as updated, " +
      "        to_char(neo_monitoreo.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_monitoreo.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_monitoreo.UpdatedBy) as UpdatedByR," +
      "        neo_monitoreo.AD_Org_ID, " +
      "(CASE WHEN neo_monitoreo.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(neo_monitoreo.Isactive, 'N') AS Isactive, " +
      "neo_monitoreo.C_Doctype_ID, " +
      "(CASE WHEN neo_monitoreo.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "neo_monitoreo.Documentno, " +
      "neo_monitoreo.Monitor, " +
      "neo_monitoreo.Docstatus, " +
      "(CASE WHEN neo_monitoreo.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "neo_monitoreo.Reproductores, " +
      "TO_CHAR(neo_monitoreo.Fecha_Inicio, ?) AS Fecha_Inicio, " +
      "TO_CHAR(neo_monitoreo.Fecha_Final, ?) AS Fecha_Final, " +
      "neo_monitoreo.CBP_Cliente_ID, " +
      "neo_monitoreo.Contactopubli, " +
      "neo_monitoreo.Descripcion, " +
      "neo_monitoreo.Procesar, " +
      "neo_monitoreo.Ordentrabajo_ID, " +
      "neo_monitoreo.Oportunidad_ID, " +
      "neo_monitoreo.CBP_Supervisor_ID, " +
      "neo_monitoreo.Fecha_Factura, " +
      "neo_monitoreo.Generar_Factura, " +
      "neo_monitoreo.AD_Client_ID, " +
      "neo_monitoreo.NEO_Monitoreo_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_monitoreo left join (select AD_Org_ID, Name from AD_Org) table1 on (neo_monitoreo.AD_Org_ID = table1.AD_Org_ID) left join (select C_Doctype_ID, Name from C_Doctype) table2 on (neo_monitoreo.C_Doctype_ID = table2.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join ad_ref_list_v list1 on (neo_monitoreo.Docstatus = list1.value and list1.ad_reference_id = '12E6E4D183B44D4EA5C27C50F90502A1' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_monitoreo.NEO_Monitoreo_ID = ? " +
      "        AND neo_monitoreo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_monitoreo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Contenido77866D1358084CE19BC044FED00A074CData objectContenido77866D1358084CE19BC044FED00A074CData = new Contenido77866D1358084CE19BC044FED00A074CData();
        objectContenido77866D1358084CE19BC044FED00A074CData.created = UtilSql.getValue(result, "created");
        objectContenido77866D1358084CE19BC044FED00A074CData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectContenido77866D1358084CE19BC044FED00A074CData.updated = UtilSql.getValue(result, "updated");
        objectContenido77866D1358084CE19BC044FED00A074CData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectContenido77866D1358084CE19BC044FED00A074CData.updatedby = UtilSql.getValue(result, "updatedby");
        objectContenido77866D1358084CE19BC044FED00A074CData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectContenido77866D1358084CE19BC044FED00A074CData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectContenido77866D1358084CE19BC044FED00A074CData.isactive = UtilSql.getValue(result, "isactive");
        objectContenido77866D1358084CE19BC044FED00A074CData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectContenido77866D1358084CE19BC044FED00A074CData.documentno = UtilSql.getValue(result, "documentno");
        objectContenido77866D1358084CE19BC044FED00A074CData.monitor = UtilSql.getValue(result, "monitor");
        objectContenido77866D1358084CE19BC044FED00A074CData.docstatus = UtilSql.getValue(result, "docstatus");
        objectContenido77866D1358084CE19BC044FED00A074CData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectContenido77866D1358084CE19BC044FED00A074CData.reproductores = UtilSql.getValue(result, "reproductores");
        objectContenido77866D1358084CE19BC044FED00A074CData.fechaInicio = UtilSql.getValue(result, "fecha_inicio");
        objectContenido77866D1358084CE19BC044FED00A074CData.fechaFinal = UtilSql.getValue(result, "fecha_final");
        objectContenido77866D1358084CE19BC044FED00A074CData.cbpClienteId = UtilSql.getValue(result, "cbp_cliente_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.contactopubli = UtilSql.getValue(result, "contactopubli");
        objectContenido77866D1358084CE19BC044FED00A074CData.descripcion = UtilSql.getValue(result, "descripcion");
        objectContenido77866D1358084CE19BC044FED00A074CData.procesar = UtilSql.getValue(result, "procesar");
        objectContenido77866D1358084CE19BC044FED00A074CData.ordentrabajoId = UtilSql.getValue(result, "ordentrabajo_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.oportunidadId = UtilSql.getValue(result, "oportunidad_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.cbpSupervisorId = UtilSql.getValue(result, "cbp_supervisor_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.fechaFactura = UtilSql.getDateValue(result, "fecha_factura", "dd-MM-yyyy");
        objectContenido77866D1358084CE19BC044FED00A074CData.generarFactura = UtilSql.getValue(result, "generar_factura");
        objectContenido77866D1358084CE19BC044FED00A074CData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.neoMonitoreoId = UtilSql.getValue(result, "neo_monitoreo_id");
        objectContenido77866D1358084CE19BC044FED00A074CData.language = UtilSql.getValue(result, "language");
        objectContenido77866D1358084CE19BC044FED00A074CData.adUserClient = "";
        objectContenido77866D1358084CE19BC044FED00A074CData.adOrgClient = "";
        objectContenido77866D1358084CE19BC044FED00A074CData.createdby = "";
        objectContenido77866D1358084CE19BC044FED00A074CData.trBgcolor = "";
        objectContenido77866D1358084CE19BC044FED00A074CData.totalCount = "";
        objectContenido77866D1358084CE19BC044FED00A074CData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectContenido77866D1358084CE19BC044FED00A074CData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Contenido77866D1358084CE19BC044FED00A074CData objectContenido77866D1358084CE19BC044FED00A074CData[] = new Contenido77866D1358084CE19BC044FED00A074CData[vector.size()];
    vector.copyInto(objectContenido77866D1358084CE19BC044FED00A074CData);
    return(objectContenido77866D1358084CE19BC044FED00A074CData);
  }

/**
Create a registry
 */
  public static Contenido77866D1358084CE19BC044FED00A074CData[] set(String isactive, String adClientId, String fechaInicio, String cbpSupervisorId, String cbpClienteId, String neoMonitoreoId, String docstatus, String oportunidadId, String monitor, String fechaFinal, String contactopubli, String createdby, String createdbyr, String descripcion, String reproductores, String procesar, String generarFactura, String ordentrabajoId, String created, String adOrgId, String updatedby, String updatedbyr, String fechaFactura, String documentno, String cDoctypeId)    throws ServletException {
    Contenido77866D1358084CE19BC044FED00A074CData objectContenido77866D1358084CE19BC044FED00A074CData[] = new Contenido77866D1358084CE19BC044FED00A074CData[1];
    objectContenido77866D1358084CE19BC044FED00A074CData[0] = new Contenido77866D1358084CE19BC044FED00A074CData();
    objectContenido77866D1358084CE19BC044FED00A074CData[0].created = created;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].createdbyr = createdbyr;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].updated = "";
    objectContenido77866D1358084CE19BC044FED00A074CData[0].updatedTimeStamp = "";
    objectContenido77866D1358084CE19BC044FED00A074CData[0].updatedby = updatedby;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].updatedbyr = updatedbyr;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].adOrgId = adOrgId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].adOrgIdr = "";
    objectContenido77866D1358084CE19BC044FED00A074CData[0].isactive = isactive;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].cDoctypeId = cDoctypeId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].cDoctypeIdr = "";
    objectContenido77866D1358084CE19BC044FED00A074CData[0].documentno = documentno;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].monitor = monitor;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].docstatus = docstatus;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].docstatusr = "";
    objectContenido77866D1358084CE19BC044FED00A074CData[0].reproductores = reproductores;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].fechaInicio = fechaInicio;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].fechaFinal = fechaFinal;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].cbpClienteId = cbpClienteId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].contactopubli = contactopubli;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].descripcion = descripcion;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].procesar = procesar;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].ordentrabajoId = ordentrabajoId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].oportunidadId = oportunidadId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].cbpSupervisorId = cbpSupervisorId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].fechaFactura = fechaFactura;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].generarFactura = generarFactura;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].adClientId = adClientId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].neoMonitoreoId = neoMonitoreoId;
    objectContenido77866D1358084CE19BC044FED00A074CData[0].language = "";
    return objectContenido77866D1358084CE19BC044FED00A074CData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef7FC447BAFDC04EF88454EBEBAB0F07E9_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefD7D6CF55C59C498AA304DC800F272018_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_monitoreo" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , C_Doctype_ID = (?) , Documentno = (?) , Monitor = (?) , Docstatus = (?) , Reproductores = TO_NUMBER(?) , Fecha_Inicio = TO_TIMESTAMP(?, ?) , Fecha_Final = TO_TIMESTAMP(?, ?) , CBP_Cliente_ID = (?) , Contactopubli = (?) , Descripcion = (?) , Procesar = (?) , Ordentrabajo_ID = (?) , Oportunidad_ID = (?) , CBP_Supervisor_ID = (?) , Fecha_Factura = TO_DATE(?) , Generar_Factura = (?) , AD_Client_ID = (?) , NEO_Monitoreo_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_monitoreo.NEO_Monitoreo_ID = ? " +
      "        AND neo_monitoreo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_monitoreo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, monitor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reproductores);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFinal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, contactopubli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ordentrabajoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, oportunidadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpSupervisorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_monitoreo " +
      "        (Created, AD_Org_ID, Isactive, C_Doctype_ID, Documentno, Monitor, Docstatus, Reproductores, Fecha_Inicio, Fecha_Final, CBP_Cliente_ID, Contactopubli, Descripcion, Procesar, Ordentrabajo_ID, Oportunidad_ID, CBP_Supervisor_ID, Fecha_Factura, Generar_Factura, AD_Client_ID, NEO_Monitoreo_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (, (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_TIMESTAMP(?, ?), TO_TIMESTAMP(?, ?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, monitor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, reproductores);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFinal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpClienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, contactopubli);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ordentrabajoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, oportunidadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpSupervisorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarFactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoMonitoreoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_monitoreo" +
      "        WHERE neo_monitoreo.NEO_Monitoreo_ID = ? " +
      "        AND neo_monitoreo.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_monitoreo.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_monitoreo" +
      "         WHERE neo_monitoreo.NEO_Monitoreo_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_monitoreo" +
      "         WHERE neo_monitoreo.NEO_Monitoreo_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
