//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.TransferenciaBancos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Transferencia18A6BEFC864B40058D465172BA530492Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Transferencia18A6BEFC864B40058D465172BA530492Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String fechaTransferencia;
  public String isactive;
  public String amount;
  public String finFinancialAccountId;
  public String finFinancialAccountIdr;
  public String finFinancialAccounttoId;
  public String finFinancialAccounttoIdr;
  public String cGlitemId;
  public String cGlitemIdr;
  public String description;
  public String finPaymentId;
  public String finPaymentIdr;
  public String finPaymenttoId;
  public String finPaymenttoIdr;
  public String transDocstatus;
  public String transProcesar;
  public String transProcesarBtn;
  public String adClientId;
  public String neoTransferenciaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("fecha_transferencia") || fieldName.equals("fechaTransferencia"))
      return fechaTransferencia;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("amount"))
      return amount;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_idr") || fieldName.equals("finFinancialAccountIdr"))
      return finFinancialAccountIdr;
    else if (fieldName.equalsIgnoreCase("fin_financial_accountto_id") || fieldName.equals("finFinancialAccounttoId"))
      return finFinancialAccounttoId;
    else if (fieldName.equalsIgnoreCase("fin_financial_accountto_idr") || fieldName.equals("finFinancialAccounttoIdr"))
      return finFinancialAccounttoIdr;
    else if (fieldName.equalsIgnoreCase("c_glitem_id") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("c_glitem_idr") || fieldName.equals("cGlitemIdr"))
      return cGlitemIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("fin_payment_id") || fieldName.equals("finPaymentId"))
      return finPaymentId;
    else if (fieldName.equalsIgnoreCase("fin_payment_idr") || fieldName.equals("finPaymentIdr"))
      return finPaymentIdr;
    else if (fieldName.equalsIgnoreCase("fin_paymentto_id") || fieldName.equals("finPaymenttoId"))
      return finPaymenttoId;
    else if (fieldName.equalsIgnoreCase("fin_paymentto_idr") || fieldName.equals("finPaymenttoIdr"))
      return finPaymenttoIdr;
    else if (fieldName.equalsIgnoreCase("trans_docstatus") || fieldName.equals("transDocstatus"))
      return transDocstatus;
    else if (fieldName.equalsIgnoreCase("trans_procesar") || fieldName.equals("transProcesar"))
      return transProcesar;
    else if (fieldName.equalsIgnoreCase("trans_procesar_btn") || fieldName.equals("transProcesarBtn"))
      return transProcesarBtn;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_transferencia_id") || fieldName.equals("neoTransferenciaId"))
      return neoTransferenciaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Transferencia18A6BEFC864B40058D465172BA530492Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Transferencia18A6BEFC864B40058D465172BA530492Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_transferencia.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_transferencia.CreatedBy) as CreatedByR, " +
      "        to_char(neo_transferencia.Updated, ?) as updated, " +
      "        to_char(neo_transferencia.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_transferencia.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_transferencia.UpdatedBy) as UpdatedByR," +
      "        neo_transferencia.AD_Org_ID, " +
      "(CASE WHEN neo_transferencia.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "neo_transferencia.Fecha_Transferencia, " +
      "COALESCE(neo_transferencia.Isactive, 'N') AS Isactive, " +
      "neo_transferencia.Amount, " +
      "neo_transferencia.FIN_Financial_Account_ID, " +
      "(CASE WHEN neo_transferencia.FIN_Financial_Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS FIN_Financial_Account_IDR, " +
      "neo_transferencia.FIN_Financial_Accountto_ID, " +
      "(CASE WHEN neo_transferencia.FIN_Financial_Accountto_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS FIN_Financial_Accountto_IDR, " +
      "neo_transferencia.C_Glitem_ID, " +
      "(CASE WHEN neo_transferencia.C_Glitem_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_Glitem_IDR, " +
      "neo_transferencia.Description, " +
      "neo_transferencia.FIN_Payment_ID, " +
      "(CASE WHEN neo_transferencia.FIN_Payment_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.DocumentNo), ''))),'') ) END) AS FIN_Payment_IDR, " +
      "neo_transferencia.FIN_Paymentto_ID, " +
      "(CASE WHEN neo_transferencia.FIN_Paymentto_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.DocumentNo), ''))),'') ) END) AS FIN_Paymentto_IDR, " +
      "neo_transferencia.Trans_Docstatus, " +
      "neo_transferencia.Trans_Procesar, " +
      "list1.name as Trans_Procesar_BTN, " +
      "neo_transferencia.AD_Client_ID, " +
      "neo_transferencia.NEO_Transferencia_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_transferencia left join (select AD_Org_ID, Name from AD_Org) table1 on (neo_transferencia.AD_Org_ID = table1.AD_Org_ID) left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table2 on (neo_transferencia.FIN_Financial_Account_ID = table2.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (table2.C_Currency_ID = table3.C_Currency_ID) left join (select Fin_Financial_Account_ID, Name from FIN_Financial_Account) table4 on (neo_transferencia.FIN_Financial_Accountto_ID =  table4.Fin_Financial_Account_ID) left join (select C_Glitem_ID, Name from C_Glitem) table5 on (neo_transferencia.C_Glitem_ID = table5.C_Glitem_ID) left join (select Fin_Payment_ID, DocumentNo from FIN_Payment) table6 on (neo_transferencia.FIN_Payment_ID =  table6.Fin_Payment_ID) left join (select Fin_Payment_ID, DocumentNo from FIN_Payment) table7 on (neo_transferencia.FIN_Paymentto_ID =  table7.Fin_Payment_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = 'D5FFEBF35DE24B14A1594F06761FF9EA' and list1.ad_language = ?  AND neo_transferencia.Trans_Procesar = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_transferencia.NEO_Transferencia_ID = ? " +
      "        AND neo_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Transferencia18A6BEFC864B40058D465172BA530492Data objectTransferencia18A6BEFC864B40058D465172BA530492Data = new Transferencia18A6BEFC864B40058D465172BA530492Data();
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.created = UtilSql.getValue(result, "created");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.updated = UtilSql.getValue(result, "updated");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.fechaTransferencia = UtilSql.getDateValue(result, "fecha_transferencia", "dd-MM-yyyy");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.isactive = UtilSql.getValue(result, "isactive");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.amount = UtilSql.getValue(result, "amount");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finFinancialAccountIdr = UtilSql.getValue(result, "fin_financial_account_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finFinancialAccounttoId = UtilSql.getValue(result, "fin_financial_accountto_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finFinancialAccounttoIdr = UtilSql.getValue(result, "fin_financial_accountto_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.cGlitemId = UtilSql.getValue(result, "c_glitem_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.cGlitemIdr = UtilSql.getValue(result, "c_glitem_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.description = UtilSql.getValue(result, "description");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finPaymentId = UtilSql.getValue(result, "fin_payment_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finPaymentIdr = UtilSql.getValue(result, "fin_payment_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finPaymenttoId = UtilSql.getValue(result, "fin_paymentto_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.finPaymenttoIdr = UtilSql.getValue(result, "fin_paymentto_idr");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.transDocstatus = UtilSql.getValue(result, "trans_docstatus");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.transProcesar = UtilSql.getValue(result, "trans_procesar");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.transProcesarBtn = UtilSql.getValue(result, "trans_procesar_btn");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.neoTransferenciaId = UtilSql.getValue(result, "neo_transferencia_id");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.language = UtilSql.getValue(result, "language");
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.adUserClient = "";
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.adOrgClient = "";
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.createdby = "";
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.trBgcolor = "";
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.totalCount = "";
        objectTransferencia18A6BEFC864B40058D465172BA530492Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectTransferencia18A6BEFC864B40058D465172BA530492Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Transferencia18A6BEFC864B40058D465172BA530492Data objectTransferencia18A6BEFC864B40058D465172BA530492Data[] = new Transferencia18A6BEFC864B40058D465172BA530492Data[vector.size()];
    vector.copyInto(objectTransferencia18A6BEFC864B40058D465172BA530492Data);
    return(objectTransferencia18A6BEFC864B40058D465172BA530492Data);
  }

/**
Create a registry
 */
  public static Transferencia18A6BEFC864B40058D465172BA530492Data[] set(String description, String createdby, String createdbyr, String fechaTransferencia, String transDocstatus, String cGlitemId, String neoTransferenciaId, String amount, String adOrgId, String updatedby, String updatedbyr, String finFinancialAccountId, String transProcesar, String transProcesarBtn, String finPaymentId, String finFinancialAccounttoId, String adClientId, String finPaymenttoId, String isactive)    throws ServletException {
    Transferencia18A6BEFC864B40058D465172BA530492Data objectTransferencia18A6BEFC864B40058D465172BA530492Data[] = new Transferencia18A6BEFC864B40058D465172BA530492Data[1];
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0] = new Transferencia18A6BEFC864B40058D465172BA530492Data();
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].created = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].createdbyr = createdbyr;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].updated = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].updatedTimeStamp = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].updatedby = updatedby;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].updatedbyr = updatedbyr;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].adOrgId = adOrgId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].adOrgIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].fechaTransferencia = fechaTransferencia;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].isactive = isactive;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].amount = amount;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finFinancialAccountId = finFinancialAccountId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finFinancialAccountIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finFinancialAccounttoId = finFinancialAccounttoId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finFinancialAccounttoIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].cGlitemId = cGlitemId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].cGlitemIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].description = description;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finPaymentId = finPaymentId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finPaymentIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finPaymenttoId = finPaymenttoId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].finPaymenttoIdr = "";
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].transDocstatus = transDocstatus;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].transProcesar = transProcesar;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].transProcesarBtn = transProcesarBtn;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].adClientId = adClientId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].neoTransferenciaId = neoTransferenciaId;
    objectTransferencia18A6BEFC864B40058D465172BA530492Data[0].language = "";
    return objectTransferencia18A6BEFC864B40058D465172BA530492Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef182A76D203F64D468FD1314261830E8B_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef7C99B9474609408295935291421779F7_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_transferencia" +
      "        SET AD_Org_ID = (?) , Fecha_Transferencia = TO_DATE(?) , Isactive = (?) , Amount = TO_NUMBER(?) , FIN_Financial_Account_ID = (?) , FIN_Financial_Accountto_ID = (?) , C_Glitem_ID = (?) , Description = (?) , FIN_Payment_ID = (?) , FIN_Paymentto_ID = (?) , Trans_Docstatus = (?) , Trans_Procesar = (?) , AD_Client_ID = (?) , NEO_Transferencia_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_transferencia.NEO_Transferencia_ID = ? " +
      "        AND neo_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccounttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymenttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoTransferenciaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_transferencia " +
      "        (AD_Org_ID, Fecha_Transferencia, Isactive, Amount, FIN_Financial_Account_ID, FIN_Financial_Accountto_ID, C_Glitem_ID, Description, FIN_Payment_ID, FIN_Paymentto_ID, Trans_Docstatus, Trans_Procesar, AD_Client_ID, NEO_Transferencia_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_DATE(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaTransferencia);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccounttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymenttoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, transProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoTransferenciaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_transferencia" +
      "        WHERE neo_transferencia.NEO_Transferencia_ID = ? " +
      "        AND neo_transferencia.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_transferencia.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_transferencia" +
      "         WHERE neo_transferencia.NEO_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_transferencia" +
      "         WHERE neo_transferencia.NEO_Transferencia_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
