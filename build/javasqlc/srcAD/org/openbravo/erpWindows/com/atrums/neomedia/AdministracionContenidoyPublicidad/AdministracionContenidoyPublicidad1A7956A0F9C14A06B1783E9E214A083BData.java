//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.AdministracionContenidoyPublicidad;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData implements FieldProvider {
static Logger log4j = Logger.getLogger(AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String isactive;
  public String fInicio;
  public String facturar;
  public String processed;
  public String fFin;
  public String mProductId;
  public String mProductIdr;
  public String adOrgId;
  public String cantidad;
  public String cantidaddias;
  public String cantPendFacturar;
  public String subtotpublicidad;
  public String cantFacturar;
  public String estadofacturado;
  public String estadofacturador;
  public String responsableId;
  public String asignado;
  public String asignador;
  public String numcotiza;
  public String numdocumento;
  public String fechadeorden;
  public String fFacturar;
  public String generarfactura;
  public String gencascontenido;
  public String estadomonitoreo;
  public String estadomonitoreor;
  public String generacontenido;
  public String subtotal;
  public String neoOrdenPrimerVId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("f_inicio") || fieldName.equals("fInicio"))
      return fInicio;
    else if (fieldName.equalsIgnoreCase("facturar"))
      return facturar;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("f_fin") || fieldName.equals("fFin"))
      return fFin;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("cantidad"))
      return cantidad;
    else if (fieldName.equalsIgnoreCase("cantidaddias"))
      return cantidaddias;
    else if (fieldName.equalsIgnoreCase("cant_pend_facturar") || fieldName.equals("cantPendFacturar"))
      return cantPendFacturar;
    else if (fieldName.equalsIgnoreCase("subtotpublicidad"))
      return subtotpublicidad;
    else if (fieldName.equalsIgnoreCase("cant_facturar") || fieldName.equals("cantFacturar"))
      return cantFacturar;
    else if (fieldName.equalsIgnoreCase("estadofacturado"))
      return estadofacturado;
    else if (fieldName.equalsIgnoreCase("estadofacturador"))
      return estadofacturador;
    else if (fieldName.equalsIgnoreCase("responsable_id") || fieldName.equals("responsableId"))
      return responsableId;
    else if (fieldName.equalsIgnoreCase("asignado"))
      return asignado;
    else if (fieldName.equalsIgnoreCase("asignador"))
      return asignador;
    else if (fieldName.equalsIgnoreCase("numcotiza"))
      return numcotiza;
    else if (fieldName.equalsIgnoreCase("numdocumento"))
      return numdocumento;
    else if (fieldName.equalsIgnoreCase("fechadeorden"))
      return fechadeorden;
    else if (fieldName.equalsIgnoreCase("f_facturar") || fieldName.equals("fFacturar"))
      return fFacturar;
    else if (fieldName.equalsIgnoreCase("generarfactura"))
      return generarfactura;
    else if (fieldName.equalsIgnoreCase("gencascontenido"))
      return gencascontenido;
    else if (fieldName.equalsIgnoreCase("estadomonitoreo"))
      return estadomonitoreo;
    else if (fieldName.equalsIgnoreCase("estadomonitoreor"))
      return estadomonitoreor;
    else if (fieldName.equalsIgnoreCase("generacontenido"))
      return generacontenido;
    else if (fieldName.equalsIgnoreCase("subtotal"))
      return subtotal;
    else if (fieldName.equalsIgnoreCase("neo_orden_primer_v_id") || fieldName.equals("neoOrdenPrimerVId"))
      return neoOrdenPrimerVId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_orden_primer_v.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_orden_primer_v.CreatedBy) as CreatedByR, " +
      "        to_char(neo_orden_primer_v.Updated, ?) as updated, " +
      "        to_char(neo_orden_primer_v.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_orden_primer_v.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_orden_primer_v.UpdatedBy) as UpdatedByR," +
      "        neo_orden_primer_v.C_Doctype_ID, " +
      "(CASE WHEN neo_orden_primer_v.C_Doctype_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "neo_orden_primer_v.C_Bpartner_ID, " +
      "(CASE WHEN neo_orden_primer_v.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "COALESCE(neo_orden_primer_v.Isactive, 'N') AS Isactive, " +
      "neo_orden_primer_v.F_Inicio, " +
      "neo_orden_primer_v.Facturar, " +
      "COALESCE(neo_orden_primer_v.Processed, 'N') AS Processed, " +
      "neo_orden_primer_v.F_Fin, " +
      "neo_orden_primer_v.M_Product_ID, " +
      "(CASE WHEN neo_orden_primer_v.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "neo_orden_primer_v.AD_Org_ID, " +
      "neo_orden_primer_v.Cantidad, " +
      "neo_orden_primer_v.Cantidaddias, " +
      "neo_orden_primer_v.Cant_Pend_Facturar, " +
      "neo_orden_primer_v.Subtotpublicidad, " +
      "neo_orden_primer_v.Cant_Facturar, " +
      "neo_orden_primer_v.Estadofacturado, " +
      "(CASE WHEN neo_orden_primer_v.Estadofacturado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EstadofacturadoR, " +
      "neo_orden_primer_v.Responsable_ID, " +
      "neo_orden_primer_v.Asignado, " +
      "(CASE WHEN neo_orden_primer_v.Asignado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS AsignadoR, " +
      "neo_orden_primer_v.Numcotiza, " +
      "neo_orden_primer_v.Numdocumento, " +
      "neo_orden_primer_v.Fechadeorden, " +
      "neo_orden_primer_v.F_Facturar, " +
      "neo_orden_primer_v.Generarfactura, " +
      "neo_orden_primer_v.Gencascontenido, " +
      "neo_orden_primer_v.Estadomonitoreo, " +
      "(CASE WHEN neo_orden_primer_v.Estadomonitoreo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS EstadomonitoreoR, " +
      "neo_orden_primer_v.Generacontenido, " +
      "neo_orden_primer_v.Subtotal, " +
      "neo_orden_primer_v.NEO_Orden_Primer_V_ID, " +
      "neo_orden_primer_v.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_orden_primer_v left join (select C_Doctype_ID, Name from C_Doctype) table1 on (neo_orden_primer_v.C_Doctype_ID = table1.C_Doctype_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table3 on (neo_orden_primer_v.C_Bpartner_ID = table3.C_BPartner_ID) left join (select M_Product_ID, Name from M_Product) table4 on (neo_orden_primer_v.M_Product_ID = table4.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL4 on (table4.M_Product_ID = tableTRL4.M_Product_ID and tableTRL4.AD_Language = ?)  left join ad_ref_list_v list1 on (neo_orden_primer_v.Estadofacturado = list1.value and list1.ad_reference_id = '939B4F215E004FBA85454E6494ADC245' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (neo_orden_primer_v.Asignado = list2.value and list2.ad_reference_id = 'B62567D1B8534AFE81AC9A7EE0A8D9CC' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (neo_orden_primer_v.Estadomonitoreo = list3.value and list3.ad_reference_id = '12E6E4D183B44D4EA5C27C50F90502A1' and list3.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_orden_primer_v.NEO_Orden_Primer_V_ID = ? " +
      "        AND neo_orden_primer_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_orden_primer_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData = new AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData();
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.created = UtilSql.getValue(result, "created");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.updated = UtilSql.getValue(result, "updated");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.updatedby = UtilSql.getValue(result, "updatedby");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.isactive = UtilSql.getValue(result, "isactive");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.fInicio = UtilSql.getDateValue(result, "f_inicio", "dd-MM-yyyy");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.facturar = UtilSql.getValue(result, "facturar");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.processed = UtilSql.getValue(result, "processed");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.fFin = UtilSql.getDateValue(result, "f_fin", "dd-MM-yyyy");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cantidad = UtilSql.getValue(result, "cantidad");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cantidaddias = UtilSql.getValue(result, "cantidaddias");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cantPendFacturar = UtilSql.getValue(result, "cant_pend_facturar");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.subtotpublicidad = UtilSql.getValue(result, "subtotpublicidad");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.cantFacturar = UtilSql.getValue(result, "cant_facturar");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.estadofacturado = UtilSql.getValue(result, "estadofacturado");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.estadofacturador = UtilSql.getValue(result, "estadofacturador");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.responsableId = UtilSql.getValue(result, "responsable_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.asignado = UtilSql.getValue(result, "asignado");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.asignador = UtilSql.getValue(result, "asignador");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.numcotiza = UtilSql.getValue(result, "numcotiza");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.numdocumento = UtilSql.getValue(result, "numdocumento");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.fechadeorden = UtilSql.getDateValue(result, "fechadeorden", "dd-MM-yyyy");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.fFacturar = UtilSql.getDateValue(result, "f_facturar", "dd-MM-yyyy");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.generarfactura = UtilSql.getValue(result, "generarfactura");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.gencascontenido = UtilSql.getValue(result, "gencascontenido");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.estadomonitoreo = UtilSql.getValue(result, "estadomonitoreo");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.estadomonitoreor = UtilSql.getValue(result, "estadomonitoreor");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.generacontenido = UtilSql.getValue(result, "generacontenido");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.subtotal = UtilSql.getValue(result, "subtotal");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.neoOrdenPrimerVId = UtilSql.getValue(result, "neo_orden_primer_v_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.language = UtilSql.getValue(result, "language");
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.adUserClient = "";
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.adOrgClient = "";
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.createdby = "";
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.trBgcolor = "";
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.totalCount = "";
        objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[] = new AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[vector.size()];
    vector.copyInto(objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData);
    return(objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData);
  }

/**
Create a registry
 */
  public static AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[] set(String cantPendFacturar, String processed, String generarfactura, String subtotpublicidad, String asignado, String cantidad, String cantidaddias, String fInicio, String generacontenido, String adOrgId, String facturar, String estadomonitoreo, String updatedby, String updatedbyr, String fFin, String responsableId, String gencascontenido, String neoOrdenPrimerVId, String mProductId, String mProductIdr, String numdocumento, String fFacturar, String isactive, String createdby, String createdbyr, String adClientId, String cDoctypeId, String cantFacturar, String cBpartnerId, String cBpartnerIdr, String fechadeorden, String numcotiza, String estadofacturado, String subtotal)    throws ServletException {
    AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[] = new AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[1];
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0] = new AdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData();
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].created = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].createdbyr = createdbyr;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].updated = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].updatedTimeStamp = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].updatedby = updatedby;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].updatedbyr = updatedbyr;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cDoctypeId = cDoctypeId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cDoctypeIdr = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cBpartnerId = cBpartnerId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cBpartnerIdr = cBpartnerIdr;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].isactive = isactive;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].fInicio = fInicio;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].facturar = facturar;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].processed = processed;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].fFin = fFin;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].mProductId = mProductId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].mProductIdr = mProductIdr;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].adOrgId = adOrgId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cantidad = cantidad;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cantidaddias = cantidaddias;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cantPendFacturar = cantPendFacturar;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].subtotpublicidad = subtotpublicidad;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].cantFacturar = cantFacturar;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].estadofacturado = estadofacturado;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].estadofacturador = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].responsableId = responsableId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].asignado = asignado;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].asignador = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].numcotiza = numcotiza;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].numdocumento = numdocumento;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].fechadeorden = fechadeorden;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].fFacturar = fFacturar;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].generarfactura = generarfactura;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].gencascontenido = gencascontenido;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].estadomonitoreo = estadomonitoreo;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].estadomonitoreor = "";
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].generacontenido = generacontenido;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].subtotal = subtotal;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].neoOrdenPrimerVId = neoOrdenPrimerVId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].adClientId = adClientId;
    objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData[0].language = "";
    return objectAdministracionContenidoyPublicidad1A7956A0F9C14A06B1783E9E214A083BData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef816793C475A64840840072DB8A46267C_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA2522CF426E54F1FACB9C9B0A3F67462_1(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefD1611E80CCF9407BAC20A4BAE3C4597A_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDE2C0E26F8CD4863AD749D95A35B1735_3(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_orden_primer_v" +
      "        SET C_Doctype_ID = (?) , C_Bpartner_ID = (?) , Isactive = (?) , Facturar = (?) , F_Inicio = TO_DATE(?) , F_Fin = TO_DATE(?) , Processed = (?) , M_Product_ID = (?) , AD_Org_ID = (?) , Cantidad = TO_NUMBER(?) , Cantidaddias = TO_NUMBER(?) , Cant_Pend_Facturar = TO_NUMBER(?) , Subtotpublicidad = TO_NUMBER(?) , Cant_Facturar = TO_NUMBER(?) , Estadofacturado = (?) , Responsable_ID = (?) , Asignado = (?) , Numcotiza = (?) , Numdocumento = (?) , Fechadeorden = TO_DATE(?) , F_Facturar = TO_DATE(?) , Generarfactura = (?) , Gencascontenido = (?) , Estadomonitoreo = (?) , Generacontenido = (?) , Subtotal = TO_NUMBER(?) , NEO_Orden_Primer_V_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_orden_primer_v.NEO_Orden_Primer_V_ID = ? " +
      "        AND neo_orden_primer_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_orden_primer_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, facturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidaddias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantPendFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotpublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadofacturado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, responsableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, asignado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numcotiza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numdocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechadeorden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarfactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, gencascontenido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadomonitoreo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generacontenido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrdenPrimerVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrdenPrimerVId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_orden_primer_v " +
      "        (C_Doctype_ID, C_Bpartner_ID, Isactive, F_Inicio, Facturar, Processed, F_Fin, M_Product_ID, AD_Org_ID, Cantidad, Cantidaddias, Cant_Pend_Facturar, Subtotpublicidad, Cant_Facturar, Estadofacturado, Responsable_ID, Asignado, Numcotiza, Numdocumento, Fechadeorden, F_Facturar, Generarfactura, Gencascontenido, Estadomonitoreo, Generacontenido, Subtotal, NEO_Orden_Primer_V_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, facturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidaddias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantPendFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotpublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadofacturado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, responsableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, asignado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numcotiza);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numdocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechadeorden);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fFacturar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarfactura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, gencascontenido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estadomonitoreo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generacontenido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrdenPrimerVId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_orden_primer_v" +
      "        WHERE neo_orden_primer_v.NEO_Orden_Primer_V_ID = ? " +
      "        AND neo_orden_primer_v.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_orden_primer_v.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_orden_primer_v" +
      "         WHERE neo_orden_primer_v.NEO_Orden_Primer_V_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_orden_primer_v" +
      "         WHERE neo_orden_primer_v.NEO_Orden_Primer_V_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
