//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.neomedia.SeguimientoComercial;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData implements FieldProvider {
static Logger log4j = Logger.getLogger(SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String clienteId;
  public String isactive;
  public String fecha;
  public String nombre;
  public String descripcion;
  public String responsableId;
  public String cbpLocationId;
  public String isoneshot;
  public String isrecurrente;
  public String isventapublicidad;
  public String isventahwsw;
  public String procesar;
  public String procesaseg;
  public String docstatus;
  public String docstatusr;
  public String adClientId;
  public String neoSegComercialId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("cliente_id") || fieldName.equals("clienteId"))
      return clienteId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("fecha"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("nombre"))
      return nombre;
    else if (fieldName.equalsIgnoreCase("descripcion"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("responsable_id") || fieldName.equals("responsableId"))
      return responsableId;
    else if (fieldName.equalsIgnoreCase("cbp_location_id") || fieldName.equals("cbpLocationId"))
      return cbpLocationId;
    else if (fieldName.equalsIgnoreCase("isoneshot"))
      return isoneshot;
    else if (fieldName.equalsIgnoreCase("isrecurrente"))
      return isrecurrente;
    else if (fieldName.equalsIgnoreCase("isventapublicidad"))
      return isventapublicidad;
    else if (fieldName.equalsIgnoreCase("isventahwsw"))
      return isventahwsw;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("procesaseg"))
      return procesaseg;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("neo_seg_comercial_id") || fieldName.equals("neoSegComercialId"))
      return neoSegComercialId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(neo_seg_comercial.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_seg_comercial.CreatedBy) as CreatedByR, " +
      "        to_char(neo_seg_comercial.Updated, ?) as updated, " +
      "        to_char(neo_seg_comercial.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        neo_seg_comercial.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = neo_seg_comercial.UpdatedBy) as UpdatedByR," +
      "        neo_seg_comercial.AD_Org_ID, " +
      "neo_seg_comercial.Cliente_ID, " +
      "COALESCE(neo_seg_comercial.Isactive, 'N') AS Isactive, " +
      "neo_seg_comercial.Fecha, " +
      "neo_seg_comercial.Nombre, " +
      "neo_seg_comercial.Descripcion, " +
      "neo_seg_comercial.Responsable_ID, " +
      "neo_seg_comercial.CBP_Location_ID, " +
      "COALESCE(neo_seg_comercial.Isoneshot, 'N') AS Isoneshot, " +
      "COALESCE(neo_seg_comercial.Isrecurrente, 'N') AS Isrecurrente, " +
      "COALESCE(neo_seg_comercial.Isventapublicidad, 'N') AS Isventapublicidad, " +
      "COALESCE(neo_seg_comercial.Isventahwsw, 'N') AS Isventahwsw, " +
      "COALESCE(neo_seg_comercial.Procesar, 'N') AS Procesar, " +
      "neo_seg_comercial.Procesaseg, " +
      "neo_seg_comercial.Docstatus, " +
      "(CASE WHEN neo_seg_comercial.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "neo_seg_comercial.AD_Client_ID, " +
      "neo_seg_comercial.NEO_Seg_Comercial_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM neo_seg_comercial left join ad_ref_list_v list1 on (neo_seg_comercial.Docstatus = list1.value and list1.ad_reference_id = '12E3A388BB764FD8A2C34273AE0C1628' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND neo_seg_comercial.NEO_Seg_Comercial_ID = ? " +
      "        AND neo_seg_comercial.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND neo_seg_comercial.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData = new SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData();
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.created = UtilSql.getValue(result, "created");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.updated = UtilSql.getValue(result, "updated");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.clienteId = UtilSql.getValue(result, "cliente_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.isactive = UtilSql.getValue(result, "isactive");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.fecha = UtilSql.getDateValue(result, "fecha", "dd-MM-yyyy");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.nombre = UtilSql.getValue(result, "nombre");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.descripcion = UtilSql.getValue(result, "descripcion");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.responsableId = UtilSql.getValue(result, "responsable_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.cbpLocationId = UtilSql.getValue(result, "cbp_location_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.isoneshot = UtilSql.getValue(result, "isoneshot");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.isrecurrente = UtilSql.getValue(result, "isrecurrente");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.isventapublicidad = UtilSql.getValue(result, "isventapublicidad");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.isventahwsw = UtilSql.getValue(result, "isventahwsw");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.procesar = UtilSql.getValue(result, "procesar");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.procesaseg = UtilSql.getValue(result, "procesaseg");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.docstatus = UtilSql.getValue(result, "docstatus");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.neoSegComercialId = UtilSql.getValue(result, "neo_seg_comercial_id");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.language = UtilSql.getValue(result, "language");
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.adUserClient = "";
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.adOrgClient = "";
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.createdby = "";
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.trBgcolor = "";
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.totalCount = "";
        objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[] = new SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[vector.size()];
    vector.copyInto(objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData);
    return(objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData);
  }

/**
Create a registry
 */
  public static SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[] set(String isrecurrente, String procesar, String fecha, String isventapublicidad, String cbpLocationId, String updatedby, String updatedbyr, String adClientId, String adOrgId, String isactive, String responsableId, String procesaseg, String docstatus, String descripcion, String neoSegComercialId, String isoneshot, String isventahwsw, String createdby, String createdbyr, String nombre, String clienteId)    throws ServletException {
    SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[] = new SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[1];
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0] = new SeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData();
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].created = "";
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].createdbyr = createdbyr;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].updated = "";
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].updatedTimeStamp = "";
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].updatedby = updatedby;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].updatedbyr = updatedbyr;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].adOrgId = adOrgId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].clienteId = clienteId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].isactive = isactive;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].fecha = fecha;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].nombre = nombre;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].descripcion = descripcion;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].responsableId = responsableId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].cbpLocationId = cbpLocationId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].isoneshot = isoneshot;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].isrecurrente = isrecurrente;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].isventapublicidad = isventapublicidad;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].isventahwsw = isventahwsw;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].procesar = procesar;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].procesaseg = procesaseg;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].docstatus = docstatus;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].docstatusr = "";
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].adClientId = adClientId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].neoSegComercialId = neoSegComercialId;
    objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData[0].language = "";
    return objectSeguimientoComercialD12978D2370E4D06AB2FA53B77913B3DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef511172D1ECDC4916BCFD2A9C43FEF5D1_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEB504B56B6C04EC5A2A6D08E543E3C1E_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE neo_seg_comercial" +
      "        SET AD_Org_ID = (?) , Cliente_ID = (?) , Isactive = (?) , Fecha = TO_DATE(?) , Nombre = (?) , Descripcion = (?) , Responsable_ID = (?) , CBP_Location_ID = (?) , Isoneshot = (?) , Isrecurrente = (?) , Isventapublicidad = (?) , Isventahwsw = (?) , Procesar = (?) , Procesaseg = (?) , Docstatus = (?) , AD_Client_ID = (?) , NEO_Seg_Comercial_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE neo_seg_comercial.NEO_Seg_Comercial_ID = ? " +
      "        AND neo_seg_comercial.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_seg_comercial.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, responsableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isoneshot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isrecurrente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isventapublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isventahwsw);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesaseg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoSegComercialId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoSegComercialId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO neo_seg_comercial " +
      "        (AD_Org_ID, Cliente_ID, Isactive, Fecha, Nombre, Descripcion, Responsable_ID, CBP_Location_ID, Isoneshot, Isrecurrente, Isventapublicidad, Isventahwsw, Procesar, Procesaseg, Docstatus, AD_Client_ID, NEO_Seg_Comercial_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clienteId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, responsableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isoneshot);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isrecurrente);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isventapublicidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isventahwsw);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesaseg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoSegComercialId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM neo_seg_comercial" +
      "        WHERE neo_seg_comercial.NEO_Seg_Comercial_ID = ? " +
      "        AND neo_seg_comercial.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND neo_seg_comercial.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM neo_seg_comercial" +
      "         WHERE neo_seg_comercial.NEO_Seg_Comercial_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM neo_seg_comercial" +
      "         WHERE neo_seg_comercial.NEO_Seg_Comercial_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
