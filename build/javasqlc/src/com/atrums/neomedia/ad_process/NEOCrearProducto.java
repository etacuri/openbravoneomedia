//Sqlc generated V1.O00-1
package com.atrums.neomedia.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NEOCrearProducto implements FieldProvider {
static Logger log4j = Logger.getLogger(NEOCrearProducto.class);
  private String InitRecordNumber="0";
  public String productoid;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("productoid"))
      return productoid;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NEOCrearProducto[] crearProducto(ConnectionProvider connectionProvider, String neoNombreProducto, String neoCategoryProducto, String netoTipoProducto, String neoTaxProducto)    throws ServletException {
    return crearProducto(connectionProvider, neoNombreProducto, neoCategoryProducto, netoTipoProducto, neoTaxProducto, 0, 0);
  }

  public static NEOCrearProducto[] crearProducto(ConnectionProvider connectionProvider, String neoNombreProducto, String neoCategoryProducto, String netoTipoProducto, String neoTaxProducto, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_crear_prod(?,?,?,?) as productoid";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoNombreProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCategoryProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, netoTipoProducto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoTaxProducto);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCrearProducto objectNEOCrearProducto = new NEOCrearProducto();
        objectNEOCrearProducto.productoid = UtilSql.getValue(result, "productoid");
        objectNEOCrearProducto.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCrearProducto);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCrearProducto objectNEOCrearProducto[] = new NEOCrearProducto[vector.size()];
    vector.copyInto(objectNEOCrearProducto);
    return(objectNEOCrearProducto);
  }

  public static NEOCrearProducto[] actualizarLinea(ConnectionProvider connectionProvider, String neoProductoId, String neoLineaId)    throws ServletException {
    return actualizarLinea(connectionProvider, neoProductoId, neoLineaId, 0, 0);
  }

  public static NEOCrearProducto[] actualizarLinea(ConnectionProvider connectionProvider, String neoProductoId, String neoLineaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_act_linea(?,?) as productoid";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoProductoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoLineaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCrearProducto objectNEOCrearProducto = new NEOCrearProducto();
        objectNEOCrearProducto.productoid = UtilSql.getValue(result, "productoid");
        objectNEOCrearProducto.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCrearProducto);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCrearProducto objectNEOCrearProducto[] = new NEOCrearProducto[vector.size()];
    vector.copyInto(objectNEOCrearProducto);
    return(objectNEOCrearProducto);
  }

  public static NEOCrearProducto[] updateFlagOrder(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    return updateFlagOrder(connectionProvider, cOrderId, 0, 0);
  }

  public static NEOCrearProducto[] updateFlagOrder(ConnectionProvider connectionProvider, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_act_flagorder(?) as productoid";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCrearProducto objectNEOCrearProducto = new NEOCrearProducto();
        objectNEOCrearProducto.productoid = UtilSql.getValue(result, "productoid");
        objectNEOCrearProducto.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCrearProducto);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCrearProducto objectNEOCrearProducto[] = new NEOCrearProducto[vector.size()];
    vector.copyInto(objectNEOCrearProducto);
    return(objectNEOCrearProducto);
  }
}
