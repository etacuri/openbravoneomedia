//Sqlc generated V1.O00-1
package com.atrums.neomedia.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NEOGenerarFactura implements FieldProvider {
static Logger log4j = Logger.getLogger(NEOGenerarFactura.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String invoiceId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("invoice_id") || fieldName.equals("invoiceId"))
      return invoiceId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NEOGenerarFactura[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, cOrderId, 0, 0);
  }

  public static NEOGenerarFactura[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3," +
      "			 d.dummy as invoice_id" +
      "	  from dual d " +
      "	  where d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOGenerarFactura.dato2 = UtilSql.getValue(result, "dato2");
        objectNEOGenerarFactura.dato3 = UtilSql.getValue(result, "dato3");
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] crearcabecera(ConnectionProvider connectionProvider, String neoOrderId)    throws ServletException {
    return crearcabecera(connectionProvider, neoOrderId, 0, 0);
  }

  public static NEOGenerarFactura[] crearcabecera(ConnectionProvider connectionProvider, String neoOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_genera_cab_facventa(?) as invoice_id";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] crearLineas(ConnectionProvider connectionProvider, String neoOrderLineId, String neoInvoiceId, String numLinea)    throws ServletException {
    return crearLineas(connectionProvider, neoOrderLineId, neoInvoiceId, numLinea, 0, 0);
  }

  public static NEOGenerarFactura[] crearLineas(ConnectionProvider connectionProvider, String neoOrderLineId, String neoInvoiceId, String numLinea, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_genera_linea_facventa(?,?,?) as invoice_id";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numLinea);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] validaLinea(ConnectionProvider connectionProvider, String neoOrderLineId)    throws ServletException {
    return validaLinea(connectionProvider, neoOrderLineId, 0, 0);
  }

  public static NEOGenerarFactura[] validaLinea(ConnectionProvider connectionProvider, String neoOrderLineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select em_neo_facturado as invoice_id" +
      "    from c_orderline" +
      "    where c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneNumFact(ConnectionProvider connectionProvider, String neoInvoiceId)    throws ServletException {
    return obtieneNumFact(connectionProvider, neoInvoiceId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneNumFact(ConnectionProvider connectionProvider, String neoInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select documentno as invoice_id" +
      "    from c_invoice" +
      "    where c_invoice_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneEsPublicidad(ConnectionProvider connectionProvider, String neoOrderLineId)    throws ServletException {
    return obtieneEsPublicidad(connectionProvider, neoOrderLineId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneEsPublicidad(ConnectionProvider connectionProvider, String neoOrderLineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select null as invoice_id, " +
      "    i.em_neo_espublicidad as dato1" +
      "	from c_orderline l" +
      " 	inner join c_order i on (l.c_order_id=i.c_order_id)" +
      "	where l.c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneCantPublicidad(ConnectionProvider connectionProvider, String neoOrderLineId)    throws ServletException {
    return obtieneCantPublicidad(connectionProvider, neoOrderLineId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneCantPublicidad(ConnectionProvider connectionProvider, String neoOrderLineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select null as invoice_id," +
      "    	l.em_neo_cant_facturado as dato1," +
      "    	l.em_neo_cant_pendientefac as dato2" +
      " 	from c_orderline l" +
      " 	where l.c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOGenerarFactura.dato2 = UtilSql.getValue(result, "dato2");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneEsFacturaParte(ConnectionProvider connectionProvider, String neoOrderLineId)    throws ServletException {
    return obtieneEsFacturaParte(connectionProvider, neoOrderLineId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneEsFacturaParte(ConnectionProvider connectionProvider, String neoOrderLineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select null as invoice_id, " +
      "    	i.em_neo_facturaparte as dato1" +
      "	from c_orderline l" +
      " 		inner join c_order i on (l.c_order_id=i.c_order_id)" +
      "		where l.c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneIdInvoice(ConnectionProvider connectionProvider, String neoParntnerId)    throws ServletException {
    return obtieneIdInvoice(connectionProvider, neoParntnerId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneIdInvoice(ConnectionProvider connectionProvider, String neoParntnerId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select coalesce(c.c_invoice_id,'0')  as invoice_id" +
      "	from c_invoice c" +
      "	 inner join c_doctype d on (c.c_doctypetarget_id=d.c_doctype_id)" +
      "	where c.docstatus='TEMP'" +
      "	 and c.processed='N'" +
      "	 and c.issotrx='Y'" +
      "	 and UPPER(d.name) like '%TEMPORAL%'" +
      "	 and c.c_bpartner_id=?" +
      "	 order by c.created asc limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoParntnerId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.invoiceId = UtilSql.getValue(result, "invoice_id");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }

  public static NEOGenerarFactura[] obtieneIdTercero(ConnectionProvider connectionProvider, String neoOrderLineId)    throws ServletException {
    return obtieneIdTercero(connectionProvider, neoOrderLineId, 0, 0);
  }

  public static NEOGenerarFactura[] obtieneIdTercero(ConnectionProvider connectionProvider, String neoOrderLineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select i.c_bpartner_id as dato1 " +
      "	from c_orderline l" +
      "  	inner join c_order i on (l.c_order_id=i.c_order_id)" +
      "  	where l.c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoOrderLineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOGenerarFactura objectNEOGenerarFactura = new NEOGenerarFactura();
        objectNEOGenerarFactura.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOGenerarFactura.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOGenerarFactura);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOGenerarFactura objectNEOGenerarFactura[] = new NEOGenerarFactura[vector.size()];
    vector.copyInto(objectNEOGenerarFactura);
    return(objectNEOGenerarFactura);
  }
}
