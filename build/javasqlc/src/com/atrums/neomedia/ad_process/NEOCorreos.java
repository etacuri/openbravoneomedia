//Sqlc generated V1.O00-1
package com.atrums.neomedia.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NEOCorreos implements FieldProvider {
static Logger log4j = Logger.getLogger(NEOCorreos.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String datocorreo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("datocorreo"))
      return datocorreo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NEOCorreos[] obtenerCorreos(ConnectionProvider connectionProvider)    throws ServletException {
    return obtenerCorreos(connectionProvider, 0, 0);
  }

  public static NEOCorreos[] obtenerCorreos(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select tipo_documento as dato1," +
      "        correos as dato2," +
      "        null as dato3," +
      "        null as dato4," +
      "        null as datocorreo" +
      " 	from neo_conf_correo" +
      " 	where correos is not null";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.dato2 = UtilSql.getValue(result, "dato2");
        objectNEOCorreos.dato3 = UtilSql.getValue(result, "dato3");
        objectNEOCorreos.dato4 = UtilSql.getValue(result, "dato4");
        objectNEOCorreos.datocorreo = UtilSql.getValue(result, "datocorreo");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static NEOCorreos[] obtenerCreados(ConnectionProvider connectionProvider, String cDoctypeId, String cOrderId)    throws ServletException {
    return obtenerCreados(connectionProvider, cDoctypeId, cOrderId, 0, 0);
  }

  public static NEOCorreos[] obtenerCreados(ConnectionProvider connectionProvider, String cDoctypeId, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "     select documentno as dato1 " +
      " 	from c_order " +
      " 	where c_doctype_id=? " +
      " 	and quotation_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static NEOCorreos[] obtenerNombreDocumento(ConnectionProvider connectionProvider, String cDoctypeId)    throws ServletException {
    return obtenerNombreDocumento(connectionProvider, cDoctypeId, 0, 0);
  }

  public static NEOCorreos[] obtenerNombreDocumento(ConnectionProvider connectionProvider, String cDoctypeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "     select upper(name) as dato1" +
      " 	from c_doctype " +
      " 	where c_doctype_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static NEOCorreos[] obtenerCorreoTercero(ConnectionProvider connectionProvider, String cBparnertId)    throws ServletException {
    return obtenerCorreoTercero(connectionProvider, cBparnertId, 0, 0);
  }

  public static NEOCorreos[] obtenerCorreoTercero(ConnectionProvider connectionProvider, String cBparnertId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select coalesce(email,'') as dato1" +
      "    from ad_user" +
      "    where c_bpartner_id=?" +
      "    and email is not null" +
      "    limit 1 ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBparnertId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static NEOCorreos[] obtenerIdAsignado(ConnectionProvider connectionProvider, String cOrderlineId)    throws ServletException {
    return obtenerIdAsignado(connectionProvider, cOrderlineId, 0, 0);
  }

  public static NEOCorreos[] obtenerIdAsignado(ConnectionProvider connectionProvider, String cOrderlineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select coalesce(em_neo_responsable_id,'') as dato1 " +
      "	from c_orderline " +
      "	where c_orderline_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static NEOCorreos[] obtenerIdSoportes(ConnectionProvider connectionProvider)    throws ServletException {
    return obtenerIdSoportes(connectionProvider, 0, 0);
  }

  public static NEOCorreos[] obtenerIdSoportes(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select neo_caso_id as dato1" +
      "	from neo_caso" +
      "	where docstatus='CR'" +
      "	and isenvioemail='Y'" +
      "	and estadocorreo='ES'";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static int actualizaSoporte(ConnectionProvider connectionProvider, String neoCasoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    update neo_caso" +
      "	set estadocorreo='EN'" +
      "	where neo_caso_id=?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCasoId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static NEOCorreos[] obtenerVacacionesAP(ConnectionProvider connectionProvider)    throws ServletException {
    return obtenerVacacionesAP(connectionProvider, 0, 0);
  }

  public static NEOCorreos[] obtenerVacacionesAP(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select r.no_vacacion_id as dato1, r.name as dato2, " +
      "           r.fecha_ini as dato3, r.fecha_fin as dato4," +
      "           (select email from ad_user where c_bpartner_id=r.c_bpartner_id and em_atecfe_check_email='Y' order by created desc limit 1) as datocorreo" +
      "    from (select v.no_vacacion_id, p.name," +
      "	     to_char(v.em_ne_fecha_inicio, 'DD-MM-YYYY') as fecha_ini, to_char(v.em_ne_fecha_fin, 'DD-MM-YYYY') as fecha_fin," +
      "	     c.c_bpartner_id" +
      "		 from no_contrato_empleado c" +
      "		 inner join no_vacacion v on (c.no_contrato_empleado_id=v.no_contrato_empleado_id)" +
      "		 inner join c_bpartner p on (c.c_bpartner_id=p.c_bpartner_id)" +
      "		 where v.em_ne_docstatus='AP'" +
      "		 and v.em_ne_procesar = 'PR'" +
      "		 and v.em_neo_estadoemail='ES') as r";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOCorreos objectNEOCorreos = new NEOCorreos();
        objectNEOCorreos.dato1 = UtilSql.getValue(result, "dato1");
        objectNEOCorreos.dato2 = UtilSql.getValue(result, "dato2");
        objectNEOCorreos.dato3 = UtilSql.getValue(result, "dato3");
        objectNEOCorreos.dato4 = UtilSql.getValue(result, "dato4");
        objectNEOCorreos.datocorreo = UtilSql.getValue(result, "datocorreo");
        objectNEOCorreos.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOCorreos);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOCorreos objectNEOCorreos[] = new NEOCorreos[vector.size()];
    vector.copyInto(objectNEOCorreos);
    return(objectNEOCorreos);
  }

  public static int actualizaVacacion(ConnectionProvider connectionProvider, String noVacacionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  update no_vacacion" +
      "             set em_neo_estadoemail='EN'" +
      "             where no_vacacion_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
