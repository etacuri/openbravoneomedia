//Sqlc generated V1.O00-1
package com.atrums.neomedia.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NEOUserData implements FieldProvider {
static Logger log4j = Logger.getLogger(NEOUserData.class);
  private String InitRecordNumber="0";
  public String name;
  public String email;
  public String telefono;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("email"))
      return email;
    else if (fieldName.equalsIgnoreCase("telefono"))
      return telefono;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NEOUserData[] select(ConnectionProvider connectionProvider, String neoContactoId)    throws ServletException {
    return select(connectionProvider, neoContactoId, 0, 0);
  }

  public static NEOUserData[] select(ConnectionProvider connectionProvider, String neoContactoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 select u.name as name," +
      "			    u.email as email," +
      "			    u.phone as telefono" +
      "		 from ad_user u " +
      "		 where u.ad_user_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoContactoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOUserData objectNEOUserData = new NEOUserData();
        objectNEOUserData.name = UtilSql.getValue(result, "name");
        objectNEOUserData.email = UtilSql.getValue(result, "email");
        objectNEOUserData.telefono = UtilSql.getValue(result, "telefono");
        objectNEOUserData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOUserData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOUserData objectNEOUserData[] = new NEOUserData[vector.size()];
    vector.copyInto(objectNEOUserData);
    return(objectNEOUserData);
  }

  public static NEOUserData[] selectPublicidad(ConnectionProvider connectionProvider, String neoCotizaId)    throws ServletException {
    return selectPublicidad(connectionProvider, neoCotizaId, 0, 0);
  }

  public static NEOUserData[] selectPublicidad(ConnectionProvider connectionProvider, String neoCotizaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select seg.isventapublicidad as name" +
      "    	from neo_seg_comercial seg" +
      "    where seg.neo_seg_comercial_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neoCotizaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NEOUserData objectNEOUserData = new NEOUserData();
        objectNEOUserData.name = UtilSql.getValue(result, "name");
        objectNEOUserData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNEOUserData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NEOUserData objectNEOUserData[] = new NEOUserData[vector.size()];
    vector.copyInto(objectNEOUserData);
    return(objectNEOUserData);
  }
}
